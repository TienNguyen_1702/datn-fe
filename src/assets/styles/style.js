export const styleBoxModal = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4
}

export const styleBoxModalBig = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 800,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4
}

export const STYLE_HEADER_TABLE = {
  fontSize: { xs: 10, lg: 13 },
  fontWeight: 700,
  color: (theme) => (theme.palette.mode === 'dark' ? '#fff' : '#6b7280'),
  '&:hover': {
    cursor: 'pointer'
  }
}

const ITEM_HEIGHT = 48
const ITEM_PADDING_TOP = 8
export const MenuPropsSelectMulti = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250
    }
  }
}
