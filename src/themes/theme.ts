import { experimental_extendTheme as extendTheme } from '@mui/material/styles'
import { CSSProperties } from 'react'

declare module '@mui/material/styles' {
  interface Theme {
    dimension: {
      header_height: CSSProperties['height']
      sidebar_width: CSSProperties['width']
    }
  }
  interface ThemeOptions {
    dimension?: {
      header_height: CSSProperties['height']
      sidebar_width: CSSProperties['width']
    }
  }
  interface TypographyVariants {
    _default: React.CSSProperties
    _title: React.CSSProperties
    _emphasize: React.CSSProperties
    _subtitle: React.CSSProperties
  }
  interface TypographyVariantsOptions {
    _default?: React.CSSProperties
    _title?: React.CSSProperties
    _emphasize?: React.CSSProperties
    _subtitle?: React.CSSProperties
  }
  interface Palette {
    _white_121212: Palette['primary']
    _f9fafb_121212: Palette['primary']
    _white_212b36: Palette['primary']
  }

  interface PaletteOptions {
    _white_121212?: PaletteOptions['primary']
    _f9fafb_121212?: PaletteOptions['primary']
    _white_212b36?: PaletteOptions['primary']
  }
}

declare module '@mui/material/Typography' {
  interface TypographyPropsVariantOverrides {
    _default: true
    _title: true
    _emphasize: true
    _subtitle: true
  }
}

declare module '@mui/material/Button' {
  interface ButtonPropsColorOverrides {
    _white_121212: true
    _f9fafb_121212: true
    _white_212b36: true
  }
}

// Create a theme instance.
const theme = extendTheme({
  dimension: {
    header_height: 64,
    sidebar_width: 256
  },
  typography: {
    fontFamily: 'Public Sans',

    _subtitle: {
      fontSize: 16,
      fontWeight: 600
    },
    _emphasize: {
      fontSize: 13,
      fontWeight: 700,
      paddingLeft: 5,
      paddingRight: 5
    }
  },
  spacing: 4,
  colorSchemes: {
    light: {
      palette: {
        primary: {
          main: '#212B36'
        },
        secondary: {
          main: '#10B981',
          contrastText: '#fff'
        },
        _white_121212: {
          main: '#fff',
          contrastText: '#000'
        },
        _f9fafb_121212: {
          main: '#F9FAFB'
        },
        _white_212b36: {
          main: '#fff'
        }
      }
    },
    dark: {
      palette: {
        secondary: {
          main: '#00A76F',
          contrastText: '#fff'
        },
        _white_121212: {
          main: '#121212',
          contrastText: '#fff'
        },
        _f9fafb_121212: {
          main: '#121212'
        },
        _white_212b36: {
          main: '#212b36'
        }
      }
    }
  },

  components: {
    MuiCssBaseline: {
      styleOverrides: {
        body: {
          '*::-webkit-scrollbar': {
            width: '5px'
          },
          '*::-webkit-scrollbar-thumb': {
            backgroundColor: '#9e9e9e',
            borderRadius: '8px'
          },
          '*::-webkit-scrollbar-thumb:hover': {
            backgroundColor: '#9e9e9e'
          }
        }
      }
    },
    MuiButton: {
      styleOverrides: {
        root: {
          textTransform: 'none'
        }
      }
    },
    MuiTab: {
      styleOverrides: {
        root: {
          textTransform: 'none'
        }
      }
    },
    MuiInputLabel: {
      styleOverrides: {
        root: {
          fontSize: '0.8rem',
          lineHeight: '18px'
        }
      }
    },
    MuiOutlinedInput: {
      styleOverrides: {
        root: {
          fontSize: '0.8rem',
          borderRadius: '10px',
          height: 48,
          '& fieldset': {
            borderWidth: '1px !important'
          },
          '&:hover fieldset': {
            borderWidth: '0.1rem !important'
          },
          '&.Mui-focused fieldset': {
            borderWidth: '0.1rem !important'
          }
        }
      }
    },
    MuiTypography: {
      defaultProps: {
        //đặt variant mặc định cho Typography
        variant: '_default'
      }
    }
  }

  // ...other properties
})

theme.typography._title = {
  fontSize: 18,
  fontWeight: 600,
  [theme.breakpoints.down('md')]: {
    fontSize: 15
  }
}

theme.typography._default = {
  fontSize: 13,
  lineHeight: 1.5,
  [theme.breakpoints.down('sm')]: {
    fontSize: 11
  },
  [theme.breakpoints.between('sm', 'md')]: {
    fontSize: 12
  }
}

export default theme
