import { BrowserRouter, Route, Routes } from 'react-router-dom'
import { ReactNode } from 'react'
import appRoutes from '@/routes/appRoutes'
import routeOthers from '@/routes/otherRoutes'
import noAuthRoutes from '@/routes/noAuthRoutes'
import AuthRouter from '@/routes/AuthRouter'
import MainLayout from '@/components/MainLayout'
import PageWrapper from '@/components/PageWrapper'
import NotFoundPage from '@/pages/errors/404'
type RouteType = {
  element: ReactNode
  state: string
  index?: boolean
  path?: string
  child?: RouteType[]
  sidebarProps?: {
    displayText: string
    icon?: ReactNode
  }
}

const generateRoute = (routes: RouteType[]): ReactNode => {
  return routes.map((route, index) =>
    route.index ? (
      <Route
        index
        path={route.path}
        element={<PageWrapper state={route.state}>{route.element}</PageWrapper>}
        key={index}
      />
    ) : (
      <Route
        path={route.path}
        element={
          <PageWrapper state={route.child ? undefined : route.state}>
            {route.element}
          </PageWrapper>
        }
        key={index}
      >
        {route.child && generateRoute(route.child)}
      </Route>
    )
  )
}

function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route
            path="/"
            element={
              <AuthRouter>
                <MainLayout />
              </AuthRouter>
            }
          >
            {generateRoute(appRoutes)}
          </Route>

          {routeOthers.map((route, index) => {
            return (
              <Route
                key={index}
                path={route.path}
                element={
                  <AuthRouter>
                    <route.component />
                  </AuthRouter>
                }
              />
            )
          })}

          {noAuthRoutes.map((route, index) => {
            return (
              <Route
                key={index}
                path={route.path}
                element={<route.component />}
              />
            )
          })}
          <Route path="*" element={<NotFoundPage />} />
        </Routes>
      </BrowserRouter>
    </>
  )
}

export default App
