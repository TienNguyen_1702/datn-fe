import { Modal, Stack } from '@mui/material'

const styleModal = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 10
}

export default function ModalMUI({
  children,
  open,
  onClose,
  minWidth
}: {
  children: React.ReactNode
  open: boolean
  onClose: React.ReactEventHandler
  minWidth?: number | string
}) {
  return (
    <Modal open={open} onClose={onClose}>
      <Stack sx={[styleModal, { minWidth: minWidth || 720 }]}>{children}</Stack>
    </Modal>
  )
}
