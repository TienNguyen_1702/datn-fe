import { Experimental_CssVarsProvider as CssVarsProvider } from '@mui/material/styles'
import theme from '@/themes/theme'

export default function ThemeMUI({ children }: { children: React.ReactNode }) {
  return <CssVarsProvider theme={theme}>{children}</CssVarsProvider>
}
