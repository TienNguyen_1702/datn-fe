import { Drawer, Box } from '@mui/material'
import appRoutes from '@/routes/appRoutes'
import { ListItemButton, ListItemIcon } from '@mui/material'
import { useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import { RootState } from '@/redux/store'
import { FiberManualRecord } from '@mui/icons-material'
import { Collapse } from '@mui/material'
import { useEffect, useState } from 'react'
import KeyboardArrowRight from '@mui/icons-material/KeyboardArrowRight'
import KeyboardArrowDown from '@mui/icons-material/KeyboardArrowDown'
import { ReactNode } from 'react'
import { useTranslation } from 'react-i18next'

type RouteType = {
  element: ReactNode
  state: string
  index?: boolean
  path?: string
  child?: RouteType[]
  sidebarProps?: {
    keyTextI18Next?: string
    displayText: string
    icon?: ReactNode
  }
}

type Props = {
  item: RouteType
}

const SidebarItem = ({ item }: Props) => {
  const { appState } = useSelector((state: RootState) => state.appState)

  const { t } = useTranslation('translation', {
    keyPrefix: 'component.Sidebar'
  })

  return item.sidebarProps && item.path ? (
    <ListItemButton
      component={Link}
      to={item.path}
      sx={{
        marginX: '16px',
        paddingY: item.sidebarProps.icon ? '12px' : '8px',
        paddingX: '12px',
        borderRadius: '8px',
        '& .MuiListItemIcon-root': {
          minWidth: 0
        },
        '&: hover': {
          backgroundColor: (theme) =>
            theme.palette.mode === 'dark' ? '#24292E' : '#F6FAFD'
        },
        color: appState === item.state ? '#10B981' : '',
        backgroundColor: (theme) =>
          appState === item.state
            ? theme.palette.mode === 'dark'
              ? '#14272A'
              : 'F2F2F2'
            : 'unset'
      }}
      className="flex gap-[10px]"
    >
      {item.sidebarProps.icon ? (
        <ListItemIcon
          sx={{
            color: appState === item.state ? '#10B981' : ''
          }}
        >
          {item.sidebarProps.icon}
        </ListItemIcon>
      ) : (
        <ListItemIcon
          sx={{
            color: appState === item.state ? '#10B981' : '',
            paddingLeft: '30px'
          }}
        >
          <FiberManualRecord sx={{ fontSize: 10 }} />
        </ListItemIcon>
      )}
      {t(`${item.sidebarProps.keyTextI18Next}`)}
    </ListItemButton>
  ) : null
}

const SidebarItemCollapse = ({ item }: Props) => {
  const [open, setOpen] = useState(false)

  const { appState } = useSelector((state: RootState) => state.appState)
  const { t } = useTranslation('translation', {
    keyPrefix: 'component.Sidebar'
  })

  useEffect(() => {
    if (appState.includes(item.state)) {
      setOpen(true)
    }
  }, [appState, item])

  return item.sidebarProps ? (
    <>
      <ListItemButton
        onClick={() => setOpen(!open)}
        sx={{
          display: 'flex',
          justifyContent: 'space-between',
          '&: hover': {
            backgroundColor: (theme) =>
              theme.palette.mode === 'dark' ? '#24292E' : '#F6FAFD'
          },
          marginX: '16px',
          paddingY: '12px',
          paddingX: '12px',
          borderRadius: '8px'
        }}
      >
        <Box className="flex gap-[10px]">
          <Box>{item.sidebarProps.icon}</Box>
          <Box>{t(`${item.sidebarProps.keyTextI18Next}`)}</Box>
        </Box>
        {open ? (
          <KeyboardArrowDown fontSize="small" />
        ) : (
          <KeyboardArrowRight fontSize="small" />
        )}
      </ListItemButton>
      <Collapse in={open} timeout="auto">
        <Box>
          {item.child?.map((route, index) =>
            route.sidebarProps ? (
              route.child ? (
                <SidebarItemCollapse item={route} key={index} />
              ) : (
                <SidebarItem item={route} key={index} />
              )
            ) : null
          )}
        </Box>
      </Collapse>
    </>
  ) : null
}

const Sidebar = ({ clickMenu }) => {
  return (
    <Box
      component="nav"
      sx={{
        width: { xs: 0, lg: clickMenu === true ? 0 : 256 },
        borderRight: {
          xs: 'none',
          lg: clickMenu === true ? 'none' : 'dashed 1px'
        },
        borderColor: (theme) =>
          theme.palette.mode === 'dark' ? '#e2e8f0' : '#718096'
      }}
    >
      <Box
        sx={{
          fontFamily: 'Cookie-Regular',
          display: { xs: 'none', lg: 'block' }
        }}
        className=" text-[35px] font-[500] fixed pl-[28px]"
      >
        Fashion360
      </Box>

      <Drawer
        variant="permanent"
        sx={{
          fontSize: 14,
          fontWeight: 500,
          '& .MuiDrawer-paper': {
            zIndex: 100,
            width: { xs: 0, lg: clickMenu === true ? 0 : 256 },
            marginTop: '64px',
            paddingBottom: '64px',
            boxSizing: 'border-box',
            borderRight: {
              xs: 'none',
              lg: clickMenu === true ? 'none' : 'dashed 1px'
            },
            borderColor: (theme) =>
              theme.palette.mode === 'dark' ? '#e2e8f0' : '#718096',
            backgroundColor: (theme) => theme.palette._white_121212.main,
            '&::-webkit-scrollbar': {
              width: '5px'
            }
          }
        }}
      >
        <Box className="text-gray-500 flex flex-col gap-[5px]">
          {appRoutes.map((route, index) =>
            route.sidebarProps ? (
              route.child ? (
                <SidebarItemCollapse item={route} key={index} />
              ) : (
                <SidebarItem item={route} key={index} />
              )
            ) : null
          )}
        </Box>
      </Drawer>
    </Box>
  )
}

export default Sidebar
