import React from 'react'
import Box from '@mui/material/Box'

interface AuthenticationComponentProps {
  Bg: string | undefined
  children: React.ReactNode
}

export default function AuthenticationComponent({
  Bg,
  children
}: AuthenticationComponentProps) {
  return (
    <Box className="flex items-center justify-center min-h-[100vh] py-[30px] bg-gray-50">
      <Box className="flex bg-white rounded-lg shadow-xl">
        <img src={Bg} alt="" className="w-[50%]  rounded-t-lg" />
        <Box className="w-[50%]  p-[40px] h-full">{children}</Box>
      </Box>
    </Box>
  )
}
