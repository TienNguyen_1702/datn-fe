import * as React from 'react'
import { MenuItem, Divider, Avatar, Stack, Typography } from '@mui/material'
import StyledMenu from './StyledMenu'
import {
  ArrowDropDown,
  Settings,
  Person,
  LogoutOutlined
} from '@mui/icons-material'
import { useNavigate } from 'react-router-dom'
import { GetStaffDetailAPI } from '@/api/StaffAPI'
import { RootState } from '@/redux/store'
import { useSelector } from 'react-redux'

export default function CustomizedMenus() {
  const navigate = useNavigate()
  const [anchorEl, setAnchorEl] = React.useState(null)
  const open = Boolean(anchorEl)
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget)
  }
  const handleClose = () => {
    setAnchorEl(null)
  }
  const adminId = useSelector((state: RootState) => state.admin.adminId)
  const { data } = GetStaffDetailAPI(adminId)

  const handleLogout = () => {
    localStorage.clear()
    navigate('/login')
  }

  return (
    <Stack direction="row" paddingRight={2}>
      <Stack
        direction="row"
        alignItems="center"
        onClick={handleClick}
        spacing={1}
        color="_white"
        className="hover:cursor-pointer"
      >
        <Avatar
          sx={{
            width: { xs: 20, sm: 25, lg: 30 },
            height: { xs: 20, sm: 25, lg: 30 },
            background: (theme) => theme.palette.secondary.main
          }}
        ></Avatar>
        <Typography
          sx={{ display: { xs: 'none', md: 'block' }, overflow: 'hidden' }}
        >
          {data?.name}
        </Typography>
        <ArrowDropDown />
      </Stack>
      <StyledMenu
        disableScrollLock={true}
        id="demo-customized-menu"
        MenuListProps={{
          'aria-labelledby': 'demo-customized-button'
        }}
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
      >
        <MenuItem
          sx={{
            paddingBottom: 0,
            '&:hover': {
              backgroundColor: (theme) => theme.palette._white_121212.main
            }
          }}
        >
          <Typography sx={{ display: { xs: 'block', md: 'none' } }}>
            {data?.name}
          </Typography>
        </MenuItem>

        <MenuItem
          sx={{
            paddingTop: 0,
            '&:hover': {
              backgroundColor: (theme) => theme.palette._white_121212.main
            }
          }}
        >
          <Typography>{data?.email}</Typography>
        </MenuItem>

        <Divider sx={{ my: 0.5 }} />

        <MenuItem onClick={handleClose} disableRipple>
          <Person />
          <Typography>My Account</Typography>
        </MenuItem>

        <MenuItem onClick={handleClose} disableRipple>
          <Settings />
          <Typography>Settings</Typography>
        </MenuItem>

        <MenuItem disableRipple onClick={handleLogout}>
          <LogoutOutlined />
          <Typography>Log out</Typography>
        </MenuItem>
      </StyledMenu>
    </Stack>
  )
}
