import { DarkMode, LightMode } from '@mui/icons-material'
import { Box } from '@mui/material'
import { useColorScheme } from '@mui/material/styles'

export default function ModeSelect() {
  const { mode, setMode } = useColorScheme()
  return (
    <Box className="hover:cursor-pointer">
      {mode === 'dark' ? (
        <LightMode
          onClick={() => setMode('light')}
          sx={{
            color: (theme) => theme.palette.secondary.main
          }}
        />
      ) : (
        <DarkMode
          onClick={() => setMode('dark')}
          sx={{
            color: (theme) => theme.palette.secondary.main
          }}
        />
      )}
    </Box>
  )
}
