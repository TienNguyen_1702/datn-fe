import { UploadIcon } from '@/assets/icons'
import { Box, Stack, Typography } from '@mui/material'
import { useTranslation } from 'react-i18next'

export default function UploadImage({ selectedImage, setSelectedImage }) {
  const { t } = useTranslation('translation', {
    keyPrefix: 'component.UploadImage'
  })
  const handleImageChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const files = e.target.files
    if (files) {
      setSelectedImage(files[0])
    }
    //Xử lý lỗi khi chọn ảnh cũ nó không hiển thị
    //https://stackoverflow.com/questions/19643265/second-use-of-input-file-doesnt-trigger-onchange-anymore
    e.target.value = ''
  }

  return (
    <Stack
      spacing={5}
      alignItems="center"
      p={5}
      flex={1}
      sx={{ background: (theme) => theme.palette._white_121212.main }}
      className=" rounded-[15px] shadow-md"
    >
      <label htmlFor="upload-photo">
        <Box className="flex items-center justify-center border-[1px] bg-white border-gray-200 border-solid rounded-full w-[144px] h-[144px] hover:cursor-pointer">
          <input
            type="file"
            id="upload-photo"
            style={{ display: 'none' }}
            onChange={handleImageChange}
          />
          <Box className="flex items-center justify-center border-[1px] border-gray-200 border-solid bg-gray-200 rounded-full w-[130px] h-[130px] hover:bg-gray-100">
            <Stack spacing={2} alignItems="center" justifyContent="center">
              {selectedImage ? (
                <img
                  src={URL.createObjectURL(selectedImage)}
                  alt="Selected"
                  width={130}
                  height={130}
                  className="rounded-full max-w-[130px] max-h-[130px]"
                />
              ) : (
                <>
                  <UploadIcon color="#919EAB" />
                  <Typography className="text-[#919EAB]">
                    {t('Upload Photo')}
                  </Typography>
                </>
              )}
            </Stack>
          </Box>
        </Box>
      </label>
      <Box className="text-center text-[#919EAB] max-w-[70%]">
        <Typography>
          {t('Allowed *.jpeg, *.jpg, *.png, *.gif max size of 3 Mb')}
        </Typography>
      </Box>
    </Stack>
  )
}
