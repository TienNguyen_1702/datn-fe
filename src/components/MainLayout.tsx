import { Outlet } from 'react-router-dom'
import { Box, Toolbar, Typography } from '@mui/material'
import Sidebar from '@/components/Sidebar'
import Header from './Header'
import PageWrapper from './PageWrapper'
import KeyboardArrowLeft from '@mui/icons-material/KeyboardArrowLeft'
import Button from '@mui/material/Button'
import { useNavigate } from 'react-router-dom'
import { useState } from 'react'
import { useTranslation } from 'react-i18next'

interface MainLayoutProps {
  title?: string
  content?: React.ReactNode
  back?: boolean
}

const MainLayout = ({ title, back, content }: MainLayoutProps) => {
  const { t } = useTranslation('translation', {
    keyPrefix: 'component.MainLayout'
  })
  const navigate = useNavigate()

  const [clickMenu, setClickMenu] = useState(false)

  return (
    <Box sx={{ display: 'flex' }}>
      <Header clickMenu={clickMenu} setClickMenu={setClickMenu} />
      <Sidebar clickMenu={clickMenu} />

      <Box
        component="main"
        sx={{
          flexGrow: 1,
          paddingY: 6,
          paddingX: { xs: 3, sm: 6 },
          width: (theme) => `calc(100% - ${theme.dimension.sidebar_width}px)`,
          minHeight: '100vh',
          marginTop: (theme) =>
            content ? `${theme.dimension.header_height}px` : 0,
          background: (theme) => theme.palette._f9fafb_121212.main
        }}
      >
        {content && (
          <>
            {back && (
              <Box sx={{ display: { xs: 'none', md: 'block' }, paddingY: 5 }}>
                <Button
                  startIcon={<KeyboardArrowLeft />}
                  onClick={() => navigate(-1)}
                  variant="outlined"
                  size="small"
                >
                  {t('Back')}
                </Button>
              </Box>
            )}
            {title && (
              <Box className="mb-[20px]">
                <Typography variant="_title">{title}</Typography>
              </Box>
            )}
            <PageWrapper>{content}</PageWrapper>
          </>
        )}

        <Toolbar />
        <Outlet />
      </Box>
    </Box>
  )
}

export default MainLayout
