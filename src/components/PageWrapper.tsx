import { useDispatch } from 'react-redux'
import { setAppState } from '@/redux/appStateSlice'
import { useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import { decode } from 'jwt-js-decode'
import { setAdminId } from '@/redux/adminIdSlice'
import { toast } from 'react-toastify'

type Props = {
  state?: string
  children
}

const PageWrapper = (props: Props) => {
  const dispatch = useDispatch()
  const navigate = useNavigate()

  useEffect(() => {
    const loadDataJwt = () => {
      const jwt = localStorage.getItem('jwt')

      if (jwt) {
        try {
          const data = decode(jwt)
          if (data) {
            dispatch(setAdminId(data.payload.id))
          }
        } catch (err) {
          toast.error('Phiên đăng nhập đã hết hạn')
          navigate('/login')
        }
      } else {
        toast.error('Phiên đăng nhập đã hết hạn')
        navigate('/login')
      }
    }

    loadDataJwt()
  }, [navigate, dispatch])

  useEffect(() => {
    if (props.state) {
      dispatch(setAppState(props.state))
    }
  }, [dispatch, props])

  return <>{props.children}</>
}

export default PageWrapper
