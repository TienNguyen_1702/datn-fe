import axios from 'axios'
import { getJwtToken, URL_BE } from './secure'
import { useQuery } from '@tanstack/react-query'

interface StaffType {
  id: number
  name: string | null
  email: string
  password: string
  phone_number: string
  image: string | null
  password_change_at: string
  last_login_at: string
  registry_at: string
  role: string
}

export const GetAllStaffAPI = () => {
  return useQuery<StaffType[]>({
    queryKey: ['GetAllStaff'],
    queryFn: async () => {
      const res = await axios({
        method: 'get',
        url: `${URL_BE}/staff`,
        headers: {
          Authorization: `Bearer ${getJwtToken()}`
        }
      })
      return res.data.data
    }
  })
}

interface StaffDetailType {
  id: number
  name: string | null
  email: string
  password: string
  phone_number: string
  image: string | undefined
  password_change_at: string
  last_login_at: string
  registry_at: string
  role: string
}

export const GetStaffDetailAPI = (staff_id) => {
  return useQuery<StaffDetailType>({
    queryKey: ['GetStaffDetail', staff_id],
    queryFn: async () => {
      const res = await axios({
        method: 'get',
        url: `${URL_BE}/staff/detail`,
        params: { staff_id: staff_id },
        headers: {
          Authorization: `Bearer ${getJwtToken()}`
        }
      })
      if (res.status === 200) {
        return res.data.data
      }
    }
  })
}

export const CreateStaffAPI = async (data) => {
  return await axios({
    method: 'post',
    data: data,
    url: `${URL_BE}/staff/create`,
    headers: {
      Authorization: `Bearer ${getJwtToken()}`
    }
  })
}

export const LoginStaffAPI = async (data) => {
  return await axios({
    method: 'post',
    data: data,
    url: `${URL_BE}/staff/login`
  })
}
