import axios from 'axios'
import { getJwtToken, URL_BE } from './secure'
import { useQuery } from '@tanstack/react-query'

export const GetAllVariantAPI = () => {
  return useQuery({
    queryKey: ['GetAllVariant'],
    queryFn: async () => {
      const res = await axios({
        method: 'get',
        url: `${URL_BE}/product/variant`,
        headers: {
          Authorization: `Bearer ${getJwtToken()}`
        }
      })
      return res.data.data
    }
  })
}

interface ColorType {
  id: number
  name: string
  color_code: string
}

export const GetAllColorAPI = () => {
  return useQuery<ColorType[]>({
    queryKey: ['GetAllColor'],
    queryFn: async () => {
      const res = await axios({
        method: 'get',
        url: `${URL_BE}/product/color`,
        headers: {
          Authorization: `Bearer ${getJwtToken()}`
        }
      })
      return res.data.data
    }
  })
}

export const CreateVariantAPI = async (data) => {
  return await axios({
    method: 'post',
    data: data,
    url: `${URL_BE}/product/variant`,
    headers: {
      Authorization: `Bearer ${getJwtToken()}`
    }
  })
}

export const UpdateVariantAPI = async (data) => {
  return await axios({
    method: 'patch',
    data: data,
    url: `${URL_BE}/product/variant`,
    headers: {
      Authorization: `Bearer ${getJwtToken()}`
    }
  })
}

export const DeleteVariantAPI = async (variant_id) => {
  return await axios({
    method: 'delete',
    params: { id: variant_id },
    url: `${URL_BE}/product/variant`,
    headers: {
      Authorization: `Bearer ${getJwtToken()}`
    }
  })
}
