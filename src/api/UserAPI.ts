import axios from 'axios'
import { getJwtToken, URL_BE } from './secure'
import { useQuery } from '@tanstack/react-query'

interface UserType {
  id: number
  name: string | null
  email: string
  password: string
  phone_number: string
  image: string | null
  password_change_at: string
  last_login_at: string
  registry_at: string
}

interface Recipient {
  id: number
  name: string
  address: string
  phone_number: string
  user_id: number
  default_recipient: boolean
}

interface UserDetailType {
  id: number
  name: string | null
  email: string
  password: string
  phone_number: string
  image: string | undefined
  password_change_at: string
  last_login_at: string
  registry_at: string
  recipients: Recipient[]
}

export const GetAllUserAPI = () => {
  return useQuery<UserType[]>({
    queryKey: ['GetAllUserAPI'],
    queryFn: async () => {
      const res = await axios({
        method: 'get',
        url: `${URL_BE}/user`,
        headers: {
          Authorization: `Bearer ${getJwtToken()}`
        }
      })
      return res.data.data
    }
  })
}

export const GetUserDetailAPI = (user_id) => {
  return useQuery<UserDetailType>({
    queryKey: ['userDetail', user_id],
    queryFn: async () => {
      const res = await axios({
        method: 'get',
        url: `${URL_BE}/user/detail`,
        params: { user_id: user_id },
        headers: {
          Authorization: `Bearer ${getJwtToken()}`
        }
      })
      return res.data.data
    }
  })
}
