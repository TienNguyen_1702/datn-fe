import axios from 'axios'
import { getJwtToken, URL_BE } from './secure'
import { useQuery } from '@tanstack/react-query'

export interface VoucherType {
  id: number
  name?: string
  created_at?: string
  started_at?: string
  expired_at?: string
  remaining_quantity?: number
  max_price?: number
  min_price_apply?: number
  percent_discount?: number
}

export const GetAllVoucherAPI = () => {
  return useQuery<VoucherType[]>({
    queryKey: ['allVouchers'],
    queryFn: async () => {
      const res = await axios({
        method: 'get',
        url: `${URL_BE}/voucher`,
        headers: {
          Authorization: `Bearer ${getJwtToken()}`
        }
      })
      return res.data.data
    }
  })
}

//DetailVoucher
export const GetRangeVoucherAPI = async (voucher_id) => {
  return await axios({
    method: 'get',
    url: `${URL_BE}/voucher/range-all`,
    params: { voucher_id: voucher_id },
    headers: {
      Authorization: `Bearer ${getJwtToken()}`
    }
  })
}

export const CreateVoucherAPI = async (data) => {
  return await axios({
    method: 'post',
    data: data,
    url: `${URL_BE}/voucher`,
    headers: {
      Authorization: `Bearer ${getJwtToken()}`
    }
  })
}

export const UpdateInfoVoucherAPI = async (data) => {
  return await axios({
    method: 'patch',
    data: data,
    url: `${URL_BE}/voucher/update-info`,
    headers: {
      Authorization: `Bearer ${getJwtToken()}`
    }
  })
}

export const UpdateRangeVoucherAPI = async (data) => {
  return await axios({
    method: 'patch',
    data: data,
    url: `${URL_BE}/voucher/update-range`,
    headers: {
      Authorization: `Bearer ${getJwtToken()}`
    }
  })
}

export const UpdateUserOwnVoucherAPI = async (data) => {
  return await axios({
    method: 'patch',
    data: data,
    url: `${URL_BE}/voucher/user`,
    headers: {
      Authorization: `Bearer ${getJwtToken()}`
    }
  })
}

export const DeleteVoucherAPI = async (voucher_id) => {
  return await axios({
    method: 'delete',
    params: { voucher_id: voucher_id },
    url: `${URL_BE}/voucher`,
    headers: {
      Authorization: `Bearer ${getJwtToken()}`
    }
  })
}
