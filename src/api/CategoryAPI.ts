import axios from 'axios'
import { getJwtToken, URL_BE } from './secure'
import { useQuery } from '@tanstack/react-query'

export interface AddTitleCategoriesLeftType {
  id: number
  name: string
  image: string | null
  title: string
}

// API 1: ----------------------------------------------------------------
interface GetAllCategoryAPIParamsType {
  parent_category_id?: number
}
export interface GetAllCategoryAPIType {
  id: number
  name: string
  image: string | null
  child_categories: GetAllCategoryAPIType[] | []
}
export const GetAllCategoryAPI = (params?: GetAllCategoryAPIParamsType) => {
  return useQuery<GetAllCategoryAPIType[]>({
    queryKey: ['GetAllCategoryAPI'],
    queryFn: async () => {
      const res = await axios({
        method: 'get',
        url: `${URL_BE}/category`,
        params: params,
        headers: {
          Authorization: `Bearer ${getJwtToken()}`
        }
      })
      if (res.status === 200) {
        return res.data.data
      }
    }
  })
}

// API 2: -------------------------------------------------------------------------
interface GetAllCategoryLeftAPIType {
  id: number
  name: string
  image: string | null
  parent_category_id: number | null
}
export const GetAllCategoryLeftAPI = () => {
  return useQuery<GetAllCategoryLeftAPIType[]>({
    queryKey: ['GetAllCategoryLeft'],
    queryFn: async () => {
      const res = await axios({
        method: 'get',
        url: `${URL_BE}/category/leaf`,
        headers: {
          Authorization: `Bearer ${getJwtToken()}`
        }
      })
      if (res.status === 200) {
        return res.data.data
      }
    }
  })
}

// API 3:
//react hook form: create Category không chứa image
export interface CreateCategoryAPIBodyType {
  name: string
  parent_category_id?: number
}

export const CreateCategoryAPI = async (data: FormData) => {
  return await axios({
    method: 'post',
    data: data,
    url: `${URL_BE}/category`,
    headers: {
      Authorization: `Bearer ${getJwtToken()}`
    }
  })
}

export const UpdateCategoryAPI = async (data: FormData) => {
  return await axios({
    method: 'patch',
    data: data,
    url: `${URL_BE}/category`,
    headers: {
      Authorization: `Bearer ${getJwtToken()}`
    }
  })
}

export const DeleteCategoryAPI = async (category_id: number) => {
  return await axios({
    method: 'delete',
    url: `${URL_BE}/category?category_id=${category_id}`,
    headers: {
      Authorization: `Bearer ${getJwtToken()}`
    }
  })
}
