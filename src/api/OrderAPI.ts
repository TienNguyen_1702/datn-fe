import { useQuery } from '@tanstack/react-query'
import { getJwtToken, URL_BE } from './secure'
import axios from 'axios'

// API 1: -----------------------------------------------------
interface GetAllOrderAPIParams {
  state?: string
  user_id?: number
  page?: number
  limit?: number
}
export const GetAllOrderAPI = (params?: GetAllOrderAPIParams) => {
  if (params?.limit) {
    params.limit = 100
  }
  return useQuery({
    queryKey: [
      'GetAllOrderAPI',
      params?.limit,
      params?.page,
      params?.state,
      params?.user_id
    ],
    queryFn: async () => {
      const res = await axios({
        method: 'get',
        url: `${URL_BE}/order/admin`,
        params: params,
        headers: {
          Authorization: `Bearer ${getJwtToken()}`
        }
      })
      if (res.status === 200) {
        return res.data.data
      }
    }
  })
}

// API 2: --------------------------------------------------
interface Variant {
  color: {
    id: number
    name: string
  }
  product: {
    id: number
    name: string
  }
  id: number
  size: string
}

interface RecipientInformation {
  address: string
  id: number
  phone_number: string
  name: string
}

interface OrderDetail {
  id: number
  price: number
  quantity: number
  variant: Variant
  image: string | null // Assuming image can be null
}

interface Voucher {
  id: number
  name: string
}

interface ShippingMethod {
  id: number
  name: string
  price: number
}

interface PaymentMethod {
  id: number
  name: string
}

interface Order {
  id: number
  state: string
  description: string | null
  webhook_data: unknown // Change this to the appropriate type if you have specific data structure
  amount: number
  created_at: string
  confirm_at: string | null
  shipp_at: string | null
  done_at: string | null
  transaction_at: string | null
  recipient_information_id: number
  shipping_method_id: number
  shipping_method: ShippingMethod
  payment_method: PaymentMethod
  payment_method_id: number
  voucher_id: number
  recipient_information: RecipientInformation
  voucher: Voucher
  order_detail: OrderDetail[]
}

export const GetOrderDetailAPI = (order_id: number) => {
  return useQuery<Order>({
    queryKey: ['GetOrderDetailAPI', order_id],
    queryFn: async () => {
      const res = await axios({
        method: 'get',
        url: `${URL_BE}/order/detail`,
        params: { order_id },
        headers: {
          Authorization: `Bearer ${getJwtToken()}`
        }
      })
      return res.data.data
    }
  })
}

// API 3: ---------------------------------------------------
export const UpdateStateOrderAPI = async ({ order_id, new_state }) => {
  return await axios({
    method: 'patch',
    url: `${URL_BE}/order/admin/state`,
    params: { order_id, state: new_state },
    headers: {
      Authorization: `Bearer ${getJwtToken()}`
    }
  })
}
