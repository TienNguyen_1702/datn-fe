import { createSlice, PayloadAction } from '@reduxjs/toolkit'

interface AdminState {
  adminId: string | null
}

const initialState: AdminState = {
  adminId: null
}

const adminSlice = createSlice({
  name: 'admin',
  initialState,
  reducers: {
    setAdminId(state, action: PayloadAction<string>) {
      state.adminId = action.payload
    }
  }
})

export const { setAdminId } = adminSlice.actions
export default adminSlice.reducer
