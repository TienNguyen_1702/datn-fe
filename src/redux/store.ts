// store.ts
import { configureStore } from '@reduxjs/toolkit'
import counterReducer from './couterSlice'
import appStateSlice from '@/redux/appStateSlice'
import adminIdSlice from './adminIdSlice'

export const store = configureStore({
  reducer: {
    counter: counterReducer,
    appState: appStateSlice,
    admin: adminIdSlice
  }
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
