export function formatNumberK(number) {
  return (number / 1000).toFixed(1) + 'k'
}

export function formatDateTime(dateTimeString) {
  if (dateTimeString) {
    const dateTime = new Date(dateTimeString)

    // Format ngày giờ
    const formattedTime = dateTime.toLocaleTimeString([], {
      hour: '2-digit',
      minute: '2-digit'
    })

    // Format ngày tháng
    const formattedDate = dateTime.toLocaleDateString([], {
      day: '2-digit',
      month: '2-digit',
      year: 'numeric'
    })

    // Extract day, month, and year
    const [day, month, year] = formattedDate.split('/')

    // Kết hợp cả hai với định dạng "dd/mm/yyyy"
    const formattedDateTime = `${formattedTime} ${month}/${day}/${year}`

    return formattedDateTime
  }
  return null
}
