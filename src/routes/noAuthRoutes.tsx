import LoginPage from '@/pages/login/page'
import ForgotPage from '@/pages/forgot/page'

const noAuthRoutes = [
  {
    path: '/forgot',
    component: ForgotPage
  },
  {
    path: '/login',
    component: LoginPage
  }
]

export default noAuthRoutes
