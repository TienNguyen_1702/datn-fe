import { decode } from 'jwt-js-decode'
import { toast } from 'react-toastify'
import { useDispatch } from 'react-redux'
import { setAdminId } from '@/redux/adminIdSlice'
import { useNavigate } from 'react-router-dom'
import { useEffect, useState } from 'react'

const AuthRouter = ({ children }) => {
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const [isDisplayChildren, setIsDisplayChildren] = useState<boolean>(false)

  useEffect(() => {
    const loadDataJwt = () => {
      const jwt = localStorage.getItem('jwt')

      if (jwt) {
        try {
          const data = decode(jwt)
          if (data) {
            dispatch(setAdminId(data.payload.id))
            setIsDisplayChildren(true)
          }
        } catch (err) {
          toast.error('Phiên đăng nhập đã hết hạn')
          setIsDisplayChildren(false)

          navigate('/login')
        }
      } else {
        toast.error('Phiên đăng nhập đã hết hạn')
        setIsDisplayChildren(false)

        navigate('/login')
      }
    }

    loadDataJwt()
  }, [navigate, dispatch])
  return isDisplayChildren ? children : <></>
}

export default AuthRouter
