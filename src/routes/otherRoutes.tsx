import SettingPage from '@/pages/settings/page'
import ProductDetail from '@/pages/catalogs/products/[id]/page'
import CustomerDetail from '@/pages/customers/[id]/page'
import VoucherDetailPage from '@/pages/catalogs/coupons/[id]/detailCoupon'
import OrderDetailPage from '@/pages/orders/[id]/page'
import StaffDetailPage from '@/pages/staffs/[id]/detailStaff'
import updateProduct from '@/pages/catalogs/products/updateProduct/updateProduct'
import CreateProductPage from '@/pages/catalogs/products/createProduct/createProduct'
import CreateVoucherPage from '@/pages/catalogs/coupons/create/createCoupon'

const routeOthers = [
  // {
  //   path: '/register',
  //   component: RegisterPage
  // },
  {
    path: '/customers/:id',
    component: CustomerDetail
  },
  {
    path: '/staffs/:id',
    component: StaffDetailPage
  },

  {
    path: '/catalogs/products/:id',
    component: ProductDetail
  },
  {
    path: '/catalogs/vouchers/:id',
    component: VoucherDetailPage
  },
  {
    path: '/orders/:id',
    component: OrderDetailPage
  },

  {
    path: '/settings',
    component: SettingPage
  },
  { path: '/createProduct', component: CreateProductPage },
  { path: '/updateProduct/:id', component: updateProduct },
  { path: '/createVoucher', component: CreateVoucherPage }
]

export default routeOthers
