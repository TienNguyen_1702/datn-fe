import { useState } from 'react'
import { Close, Add } from '@mui/icons-material'
import {
  Box,
  Button,
  TextField,
  Select,
  MenuItem,
  FormControl,
  InputLabel,
  Typography,
  Stack,
  Collapse
} from '@mui/material'
import { useForm, Controller } from 'react-hook-form'
import { toast } from 'react-toastify'
import { toastErrorMessage } from '@/utils/errorMessage'
import { useMutation } from '@tanstack/react-query'
import LoadingButton from '@mui/lab/LoadingButton'
import UploadImage from '@/components/Global/UploadImage'
import { CreateCategoryAPI, CreateCategoryAPIBodyType } from '@/api/CategoryAPI'
import { useTranslation } from 'react-i18next'

export default function AddCategory({ categories, refetch }) {
  //react-hook-form
  const {
    control,
    handleSubmit,
    reset,
    formState: { errors }
  } = useForm<CreateCategoryAPIBodyType>()

  const { t } = useTranslation('translation', {
    keyPrefix: 'page.Category'
  })
  const { t: v } = useTranslation('translation', {
    keyPrefix: 'validate'
  })

  //Đóng mở Modal Add Category
  const [isOpenModalAddCategory, setIsOpenModalAddCategory] =
    useState<boolean>(false)
  const handleOpenModalAddCategory = () => {
    setIsOpenModalAddCategory(true)
  }
  const handleCloseModalAddCategory = () => {
    setIsOpenModalAddCategory(false)
  }

  //Ảnh của category
  const [selectedImage, setSelectedImage] = useState<File | null>()

  //react-query: createCategory
  const { mutate, isPending } = useMutation({
    mutationFn: CreateCategoryAPI,
    onSuccess: () => {
      toast.success('Thêm Category thành công')
      setSelectedImage(null)
      reset()
      refetch()
      setIsOpenModalAddCategory(false)
    },
    onError: (err) => {
      toastErrorMessage(err)
    }
  })
  const handleCreateCategory = async (data: CreateCategoryAPIBodyType) => {
    const formData = new FormData()
    if (selectedImage) {
      formData.append('image', selectedImage)
    }
    formData.append('name', data.name)
    if (data.parent_category_id && data.parent_category_id !== 0) {
      formData.append('parent_category_id', String(data.parent_category_id))
    }
    mutate(formData)
  }

  const ModalAddCategory = () => {
    return (
      <>
        {isOpenModalAddCategory && (
          <Stack
            sx={{
              background: (theme) => theme.palette._white_212b36.main,
              minHeight: '100vh'
            }}
          >
            {/* Start Header Modal */}
            <Stack
              direction="row"
              alignItems="center"
              justifyContent="space-between"
              p={{ xs: 2, lg: 5 }}
              sx={{ background: (theme) => theme.palette._white_212b36.main }}
            >
              <Stack spacing={2}>
                <Typography
                  sx={{ display: { xs: 'none', md: 'block' } }}
                  variant="_subtitle"
                >
                  {t('Add Category')}
                </Typography>
                <Typography sx={{ display: { xs: 'none', md: 'block' } }}>
                  {t('Add your category and necessary information from here')}
                </Typography>
              </Stack>
              <Box
                sx={{ background: (theme) => theme.palette._white_212b36.main }}
                onClick={handleCloseModalAddCategory}
                className="flex  rounded-full text-[#E53E3E] p-[5px] hover:cursor-pointer"
              >
                <Close />
              </Box>
            </Stack>
            {/* End Header Modal */}

            <Stack
              direction={{ sx: 'column', lg: 'row' }}
              gap={3}
              p={{ xs: 2, lg: 5 }}
            >
              <UploadImage
                selectedImage={selectedImage}
                setSelectedImage={setSelectedImage}
              />
              <Stack
                spacing={4}
                p={5}
                flex={1}
                sx={{ background: (theme) => theme.palette._white_121212.main }}
                className=" rounded-[15px] shadow-md relative"
              >
                <Controller
                  defaultValue={''}
                  name="name"
                  control={control}
                  rules={{ required: v('This field is required') }}
                  render={({ field }) => (
                    <TextField
                      label={t('Category Name')}
                      error={!!errors.name}
                      helperText={errors.name?.message?.toString()}
                      {...field}
                    />
                  )}
                />

                <Controller
                  defaultValue={0}
                  name="parent_category_id"
                  control={control}
                  render={({ field }) => (
                    <FormControl>
                      <InputLabel id="demo-simple-select-label">
                        {t('Category Parent')}
                      </InputLabel>
                      <Select
                        id="demo-simple-select"
                        labelId="demo-simple-select-label"
                        label={t('Category Parent')}
                        {...field}
                      >
                        <MenuItem value="" disabled>
                          <Typography>{t('Select a category')}</Typography>
                        </MenuItem>
                        <MenuItem value={0}>
                          <Typography>
                            {t('There is no parent category')}
                          </Typography>
                        </MenuItem>
                        {categories?.map((category) => (
                          <MenuItem
                            key={category.id}
                            value={category.id}
                            className="flex items-center gap-[10px]"
                          >
                            <Stack
                              direction="row"
                              spacing={2}
                              alignItems="center"
                            >
                              <img
                                width="30"
                                height="30"
                                src={category.image}
                              />
                              <Typography>{category.name}</Typography>
                            </Stack>
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                  )}
                />

                <Box className="flex justify-end">
                  <LoadingButton
                    variant="contained"
                    onClick={handleSubmit(handleCreateCategory)}
                    loading={isPending}
                  >
                    {t('Create Category')}
                  </LoadingButton>
                </Box>
              </Stack>
            </Stack>
          </Stack>
        )}
      </>
    )
  }

  return (
    <>
      {/* Button nhấn để hiện Modal */}
      <Box sx={{ display: isOpenModalAddCategory ? 'none' : 'block' }}>
        <Button
          variant="contained"
          startIcon={<Add />}
          onClick={handleOpenModalAddCategory}
          color="secondary"
        >
          {t('Add Category')}
        </Button>
      </Box>
      <Stack sx={{ display: { xs: 'none', md: 'block' } }}>
        {/* Tạo màn hình bóng đen khi Open modal */}
        {isOpenModalAddCategory && (
          <Box
            className="fixed top-0 left-0 w-full h-full bg-black opacity-50 z-[300]"
            onClick={handleCloseModalAddCategory}
          ></Box>
        )}

        {/* Modal */}
        <Box
          className={` fixed top-0 right-0 z-[300] h-screen bg-[#fff] transition-all duration-300 ease-in-out`}
          sx={{ width: isOpenModalAddCategory ? 650 : 0 }}
        >
          <ModalAddCategory />
        </Box>
      </Stack>
      <Stack sx={{ display: { xs: 'block', md: 'none' } }}>
        <Collapse in={isOpenModalAddCategory}>
          <ModalAddCategory />
        </Collapse>
      </Stack>
    </>
  )
}
