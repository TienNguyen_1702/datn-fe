import React from 'react'
import { Box, Stack, Typography } from '@mui/material'
import AddCategory from './AddCategory'
import CustomizedTreeView from './Tree'
import { convertCategory } from '@/utils/convertData'
import { GetAllCategoryAPI } from '@/api/CategoryAPI'
import { useTranslation } from 'react-i18next'

const CategoryLayout = () => {
  const { t } = useTranslation('translation', {
    keyPrefix: 'page.Category'
  })
  const {
    data: categories,
    isLoading,
    isError,
    error,
    refetch
  } = GetAllCategoryAPI()

  return (
    <Stack spacing={5}>
      <Typography variant="_title">{t('Category Page')}</Typography>
      <Box className=" rounded-lg" p={{ xs: 0, sm: 1, md: 5, lg: 10 }}>
        <AddCategory
          categories={convertCategory(categories)}
          refetch={refetch}
        />
      </Box>

      <Box
        sx={{
          width: '100%',
          backgroundColor: (theme) => theme.palette._white_212b36.main,
          borderRadius: '6px'
        }}
      >
        <CustomizedTreeView
          categories={categories}
          isError={isError}
          errorLabel={error?.message}
          isLoading={isLoading}
          refetch={refetch}
        />
      </Box>
    </Stack>
  )
}

export default function CategoryPage() {
  return <CategoryLayout />
}
