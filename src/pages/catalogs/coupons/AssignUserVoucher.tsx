import { MenuPropsSelectMulti } from '@/assets/styles/style'
import { Visibility } from '@mui/icons-material'
import {
  Box,
  Checkbox,
  FormControl,
  FormHelperText,
  InputLabel,
  MenuItem,
  OutlinedInput,
  Select,
  SelectChangeEvent,
  Tooltip,
  Typography
} from '@mui/material'
import { useTranslation } from 'react-i18next'

export default function AssignUserVoucher({
  usersSelect,
  setUsersSelect,
  users
}) {
  const { t } = useTranslation('translation', { keyPrefix: 'page.Coupons' })

  const handleChangeUsersSelect = (
    event: SelectChangeEvent<typeof usersSelect>
  ) => {
    const {
      target: { value }
    } = event
    setUsersSelect(
      // On autofill we get a stringified value.
      typeof value === 'string' ? value.split(',') : value
    )
  }

  return (
    <FormControl sx={{ width: '100%' }}>
      <InputLabel id="demo-multiple-checkbox-label">
        {t('Select Users')}
      </InputLabel>
      <Select
        multiple
        value={usersSelect}
        onChange={handleChangeUsersSelect}
        input={<OutlinedInput label={t('Select Users')} />}
        renderValue={(selected) => selected.join(', ')}
        MenuProps={MenuPropsSelectMulti}
      >
        {users?.map((user) => (
          <MenuItem key={user.id} value={user.title}>
            <Checkbox
              size="small"
              checked={usersSelect.indexOf(user.title) > -1}
            />
            <Box className="flex items-center gap-[10px]">
              <Tooltip title={user.title}>
                <Visibility sx={{ fontSize: 14 }} />
              </Tooltip>
              <Box className="flex gap-[10px] items-center">
                <img width="30" height="30" src={user.image} alt="" />
                {user.name ? (
                  <Typography>{user.name} </Typography>
                ) : (
                  <Typography variant="caption">{t('No name')}</Typography>
                )}
                <Typography>
                  ({user.phone_number} - {user.email})
                </Typography>
              </Box>
            </Box>
          </MenuItem>
        ))}
      </Select>
      <FormHelperText>
        * {t('Leave blank if Voucher applies to all users')}
      </FormHelperText>
    </FormControl>
  )
}
