import React, { useState } from 'react'
import {
  DeleteOutlineOutlined,
  DriveFileRenameOutlineOutlined
} from '@mui/icons-material'
import TableMUI from '@/components/TableMUI'
import { toast } from 'react-toastify'
import { STYLE_HEADER_TABLE } from '@/assets/styles/style'
import { Button, Stack, TextField, Box, Typography } from '@mui/material'
import { toastErrorMessage } from '@/utils/errorMessage'
import { Controller, useForm } from 'react-hook-form'
import { DatePicker } from '@mui/x-date-pickers'
import dayjs from 'dayjs'
import { useNavigate } from 'react-router-dom'
import { formatDateTime } from '@/utils/format'
import ModalMUI from '@/components/ModalMUI'
import { useMutation } from '@tanstack/react-query'
import { LoadingButton } from '@mui/lab'
import {
  DeleteVoucherAPI,
  UpdateInfoVoucherAPI,
  VoucherType
} from '@/api/VoucherAPI'
import { useTranslation } from 'react-i18next'

export default function VoucherTable({
  vouchers,
  refetch,
  isLoading,
  isError,
  errorLabel
}) {
  const navigate = useNavigate()
  const {
    control,
    reset,
    watch,
    handleSubmit,
    formState: { errors }
  } = useForm()
  const maxPrice = watch('max_price')

  const { t } = useTranslation('translation', { keyPrefix: 'page.Coupons' })
  const { t: v } = useTranslation('translation', { keyPrefix: 'validate' })

  const columns = [
    {
      field: 'id',
      maxWidth: 30,
      sortable: false,
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>ID</Box>
    },
    {
      field: 'name',
      minWidth: 200,
      sortable: false,
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>{t('Voucher Name')}</Box>
    },
    {
      field: 'type',
      sortable: false,
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>{t('Voucher Type')}</Box>
    },
    {
      field: 'created_at',
      minWidth: 150,
      sortable: false,
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>{t('Create At')}</Box>,
      renderCell: (rowData) => (
        <Box>{formatDateTime(rowData.row.created_at)}</Box>
      )
    },
    {
      field: 'started_at',
      sortable: false,
      minWidth: 150,
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>{t('Start Date')}</Box>,
      renderCell: (rowData) => (
        <Box>{formatDateTime(rowData.row.started_at)}</Box>
      )
    },
    {
      field: 'expired_at',
      sortable: false,
      minWidth: 150,
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>{t('Expire Date')}</Box>,
      renderCell: (rowData) => (
        <Box>{formatDateTime(rowData.row.expired_at)}</Box>
      )
    },
    {
      field: 'remaining_quantity',
      sortable: false,
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>{t('Quantity')}</Box>
    },
    {
      field: 'max_price',
      sortable: false,
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>{t('Max Price')}</Box>
    },
    {
      field: 'min_price_apply',
      minWidth: 180,
      sortable: false,
      renderHeader: () => (
        <Box sx={STYLE_HEADER_TABLE}>{t('Min Price Apply')}</Box>
      )
    },
    {
      field: 'percent_discount',
      minWidth: 180,
      sortable: false,
      renderHeader: () => (
        <Box sx={STYLE_HEADER_TABLE}>{t('Percent Discount')}</Box>
      )
    },
    {
      field: 'actions',
      minWidth: 300,
      sortable: false,
      renderCell: (params) => (
        <Box sx={{ display: 'flex', gap: 1 }}>
          <Button
            variant="outlined"
            size="small"
            onClick={() => navigate(`/catalogs/vouchers/${params.row.id}`)}
          >
            {t('See details')}
          </Button>
          <DriveFileRenameOutlineOutlined
            style={{ cursor: 'pointer' }}
            onClick={() => {
              handleOpenModalUpdateVoucher()
              setSelectedRows(params.row)
            }}
          />
          <DeleteOutlineOutlined
            style={{ cursor: 'pointer' }}
            onClick={() => {
              handleOpenModalDeleteVoucher()
              setSelectedRows(params.row)
            }}
          />
        </Box>
      ),
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}></Box>
    }
  ]

  const [selectedRows, setSelectedRows] = useState<VoucherType>()

  const [openModalUpdateVoucher, setOpenModalUpdateVoucher] = useState(false)
  const handleOpenModalUpdateVoucher = () => {
    setOpenModalUpdateVoucher(true)
  }
  const handleCloseModalUpdateVoucher = () => {
    setOpenModalUpdateVoucher(false)
    reset()
  }

  const [openModalDeleteVoucher, setOpenModalDeleteVoucher] = useState(false)
  const handleOpenModalDeleteVoucher = () => {
    setOpenModalDeleteVoucher(true)
  }
  const handleCloseModalDeleteVoucher = () => {
    setOpenModalDeleteVoucher(false)
  }

  //react-query: updateVoucherInfo
  const {
    mutate: mutateUpdateVoucherInfo,
    isPending: isPendingUpdateVoucherInfo
  } = useMutation({
    mutationFn: UpdateInfoVoucherAPI,
    onSuccess: () => {
      toast.success('Cập nhật thông tin voucher thành công')
      refetch()
      handleCloseModalUpdateVoucher()
    },
    onError: (err) => {
      toastErrorMessage(err)
    }
  })
  const handleUpdateVoucher = async (data) => {
    data.voucher_id = Number(selectedRows?.id)
    data.remaining_quantity = Number(data.remaining_quantity)
    data.min_price_apply = Number(data.min_price_apply)
    data.max_price = Number(data.max_price)
    data.percent_discount = Number(data.percent_discount)
    data.started_at = data.started_at.unix()
    data.expired_at = data.expired_at.unix()
    mutateUpdateVoucherInfo(data)
  }

  //react-query: deleteVoucher
  const { mutate: mutateDeleteVoucher, isPending: isPendingDeleteVoucher } =
    useMutation({
      mutationFn: DeleteVoucherAPI,
      onSuccess: () => {
        toast.success('Xóa Voucher thành công')
        refetch()
        handleCloseModalDeleteVoucher()
      },
      onError: (err) => {
        toastErrorMessage(err)
      }
    })
  const handleDeleteVoucher = async () => {
    mutateDeleteVoucher(selectedRows?.id)
  }

  return (
    <>
      <TableMUI
        columns={columns}
        rows={vouchers}
        isLoading={isLoading}
        loadingLabel={t('Loading list of Coupons')}
        isError={isError}
        errorLabel={errorLabel}
      />
      <ModalMUI
        open={openModalUpdateVoucher}
        onClose={handleCloseModalUpdateVoucher}
      >
        <Stack spacing={5}>
          <Stack direction="row" spacing={4}>
            <Controller
              name="name"
              control={control}
              rules={{ required: v('This field is required') }}
              defaultValue={selectedRows?.name}
              render={({ field }) => (
                <TextField
                  sx={{ flex: 1 }}
                  label={t('Voucher Name')}
                  error={!!errors.name}
                  helperText={errors.name?.message?.toString()}
                  {...field}
                />
              )}
            />
          </Stack>

          <Stack direction="row" spacing={4}>
            <Controller
              defaultValue={dayjs(selectedRows?.started_at)}
              name="started_at"
              control={control}
              rules={{ required: v('This field is required') }}
              render={({ field }) => (
                <DatePicker
                  label={t('Start Date')}
                  {...field}
                  sx={{ flex: 1 }}
                />
              )}
            />

            <Controller
              name="expired_at"
              defaultValue={dayjs(selectedRows?.expired_at)}
              control={control}
              rules={{ required: v('This field is required') }}
              render={({ field }) => (
                <DatePicker
                  label={t('Expire Date')}
                  {...field}
                  sx={{ flex: 1 }}
                />
              )}
            />
          </Stack>
          <Stack direction="row" spacing={4}>
            <Controller
              defaultValue={selectedRows?.remaining_quantity}
              name="remaining_quantity"
              control={control}
              rules={{
                required: v('This field is required'),
                min: { value: 1, message: v('Minimum value is 1') }
              }}
              render={({ field }) => (
                <TextField
                  sx={{ flex: 1 }}
                  label={t('Quantity')}
                  error={!!errors.remaining_quantity}
                  helperText={errors.remaining_quantity?.message?.toString()}
                  {...field}
                />
              )}
            />
            <Controller
              name="max_price"
              defaultValue={selectedRows?.max_price}
              control={control}
              rules={{
                required: v('This field is required'),
                min: { value: 0, message: v('Invalid value') }
              }}
              render={({ field }) => (
                <TextField
                  sx={{ flex: 1 }}
                  label={t('Max Price')}
                  error={!!errors.max_price}
                  helperText={errors.max_price?.message?.toString()}
                  {...field}
                />
              )}
            />
          </Stack>
          <Stack direction="row" spacing={4}>
            <Controller
              defaultValue={selectedRows?.min_price_apply}
              name="min_price_apply"
              control={control}
              rules={{
                required: v('This field is required'),
                min: {
                  value: 0,
                  message: v('Invalid value')
                },
                validate: (value) =>
                  parseFloat(value) > parseFloat(maxPrice) ||
                  v('Min price apply must be greater than max price')
              }}
              render={({ field }) => (
                <TextField
                  sx={{ flex: 1 }}
                  label={t('Min Price Apply')}
                  error={!!errors.min_price_apply}
                  helperText={errors.min_price_apply?.message?.toString()}
                  {...field}
                />
              )}
            />
            <Controller
              defaultValue={selectedRows?.percent_discount}
              name="percent_discount"
              control={control}
              rules={{
                required: v('This field is required'),
                min: { value: 1, message: v('The value ranges from 1 to 100') },
                max: {
                  value: 100,
                  message: v('The value ranges from 1 to 100')
                }
              }}
              render={({ field }) => (
                <TextField
                  sx={{ flex: 1 }}
                  label={t('Percent Discount')}
                  error={!!errors.percent_discount}
                  helperText={errors.percent_discount?.message?.toString()}
                  {...field}
                />
              )}
            />
          </Stack>

          <Box className="flex justify-end gap-[15px]">
            <Button
              variant="contained"
              onClick={handleCloseModalUpdateVoucher}
              color="error"
            >
              {t('Cancel')}
            </Button>
            <LoadingButton
              variant="contained"
              onClick={handleSubmit(handleUpdateVoucher)}
              loading={isPendingUpdateVoucherInfo}
            >
              {t('Update Voucher')}
            </LoadingButton>
          </Box>
        </Stack>
      </ModalMUI>

      <ModalMUI
        open={openModalDeleteVoucher}
        onClose={handleCloseModalDeleteVoucher}
        minWidth={500}
      >
        <Stack spacing={2} alignItems="center">
          <Typography>
            {t('')}
            <Typography variant="_emphasize">{selectedRows?.name}</Typography>
          </Typography>
          <Stack direction="row" spacing={4}>
            <Button
              variant="contained"
              color="error"
              onClick={handleCloseModalDeleteVoucher}
            >
              {t('Cancel')}
            </Button>
            <LoadingButton
              variant="contained"
              onClick={handleDeleteVoucher}
              loading={isPendingDeleteVoucher}
            >
              {t('Confirm')}
            </LoadingButton>
          </Stack>
        </Stack>
      </ModalMUI>
    </>
  )
}
