import MainLayout from '@/components/MainLayout'
import { convertArrayIds } from '@/utils/convertData'
import { toastErrorMessage } from '@/utils/errorMessage'
import { Box, Button, Stack, Tab, Typography } from '@mui/material'
import { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import { useParams } from 'react-router-dom'
import { toast } from 'react-toastify'
import { ChooseTypeVoucher } from '../ChooseTypeVoucher'
import { formatDateTime } from '@/utils/format'
import { useMutation } from '@tanstack/react-query'
import { LoadingButton, TabContext, TabList, TabPanel } from '@mui/lab'
import ModalMUI from '@/components/ModalMUI'
import {
  GetRangeVoucherAPI,
  UpdateRangeVoucherAPI,
  UpdateUserOwnVoucherAPI
} from '@/api/VoucherAPI'
import {
  AddTitleCategoriesLeftType,
  GetAllCategoryLeftAPI
} from '@/api/CategoryAPI'
import { AddTitleProductsType, GetAllProductAPI } from '@/api/ProductAPI'
import { GetAllVariantAPI } from '@/api/VariantColorAPI'
import { GetAllUserAPI } from '@/api/UserAPI'
import AssignUserVoucher from '../AssignUserVoucher'

const styleTab = {
  minHeight: 500,
  padding: 5,
  background: (theme) => theme.palette._white_212b36.main,
  boxShadow:
    'rgba(145, 158, 171, 0.2) 0px 0px 2px 0px, rgba(145, 158, 171, 0.12) 0px 12px 24px -4px'
}

const VoucherDetail = () => {
  const { handleSubmit } = useForm()
  const { id } = useParams()

  const [openModalUpdateRangeVoucher, setOpenModalUpdateRangeVoucher] =
    useState(false)
  const handleOpenModalUpdateRangeVoucher = () => {
    setOpenModalUpdateRangeVoucher(true)
    loadData()
  }
  const handleCloseModalUpdateRangeVoucher = () => {
    setOpenModalUpdateRangeVoucher(false)
  }

  const [openModalAssignVoucherToUsers, setOpenModalAssignVoucherToUsers] =
    useState(false)
  const handleOpenModalAssignVoucherToUsers = () => {
    setOpenModalAssignVoucherToUsers(true)
    loadData()
  }
  const handleCloseModalAssignVoucherToUsers = () => {
    setOpenModalAssignVoucherToUsers(false)
  }

  const [valuesSelected, setValuesSelected] = useState<string[]>([])
  const [typeVoucher, setTypeVoucher] = useState<string>('')

  const { data: categories } = GetAllCategoryLeftAPI()
  const customCategories: AddTitleCategoriesLeftType[] | undefined =
    categories?.map((category) => ({
      id: category.id,
      name: category.name,
      image: category.image,
      title: `${category.name}(${category.id})`
    }))

  const { filteredProducts: products, mutation } = GetAllProductAPI()
  useEffect(() => {
    mutation.mutate({ sort_by: 'Mới nhất' })
  }, [])
  const customProducts: AddTitleProductsType[] | undefined = products?.map(
    (product) => ({
      id: product.id,
      name: product.name,
      image: product.image,
      title: `${product.name}(${product.id})`
    })
  )

  const { data: variants } = GetAllVariantAPI()
  const customVariants = variants?.map((variant) => ({
    id: variant.id,
    name: '',
    image: variant?.product?.product_image[0]?.image,
    title: `(${variant.size} - ${variant.color.name}) ${variant.product.name}`
  }))

  const { data: users } = GetAllUserAPI()
  const customUsers = users?.map((user) => ({
    id: user.id,
    name: user.name,
    email: user.email,
    image: user.image,
    phone_number: user.phone_number,
    title: `${user.name} (${user.id}) - ${user.email} - ${user.phone_number}`
  }))

  const [usersSelect, setUsersSelect] = useState<string[]>([])

  const [categoryVouchers, setCategoryVouchers] = useState<
    { id: ''; name: ''; image: '' }[]
  >([])
  const [productVouchers, setProductVouchers] = useState<
    { id: ''; name: '' }[]
  >([])
  const [variantVouchers, setVariantVouchers] = useState<
    { id: ''; productId: ''; productName: ''; size: ''; color: '' }[]
  >([])
  const [userVouchers, setUserVouchers] = useState<
    { id?: number; image?: string; name?: string; email?: string }[]
  >([])

  const [voucherInfo, setVoucherInfo] = useState<{
    id: number
    name: string
    created_at: string
    started_at: string
    expired_at: string
    remaining_quantity: string
    used_quantity: string
    max_price: number
    min_price_apply: number
    percent_discount: number
    type: string
  }>()

  useEffect(() => {
    loadData()
  }, [])

  const loadData = async () => {
    try {
      const res = await GetRangeVoucherAPI(id)
      if (res.status === 200) {
        setTypeVoucher(res.data.data.voucherInfo.type)
        setVoucherInfo(res.data.data.voucherInfo)
        const userVouchersResult = res.data.data.user_own_voucher.map(
          (user) => ({
            id: user.user.id,
            name: user.user.name,
            email: user.user.email,
            image: user.user.image
          })
        )
        setUserVouchers(userVouchersResult)

        if (res.data.data.voucherInfo.type === 'CATEGORY') {
          const categoriesResult = res.data.data.categoryLists.map(
            (category) => ({
              id: category.category.id,
              name: category.category.name,
              image: category.category.image
            })
          )

          setCategoryVouchers(categoriesResult)
          setProductVouchers([])
          setVariantVouchers([])
        } else if (res.data.data.voucherInfo.type === 'PRODUCT') {
          const productsResult = res.data.data.productLists.map((product) => ({
            id: product.product.id,
            name: product.product.name
          }))
          setProductVouchers(productsResult)
          setCategoryVouchers([])
          setVariantVouchers([])
        } else if (res.data.data.voucherInfo.type === 'VARIANT') {
          const variantsResult = res.data.data.variantLists.map((variant) => ({
            id: variant.variant.id,
            productId: variant.variant.product.id,
            productName: variant.variant.product.name,
            size: variant.variant.size,
            color: variant.variant.color.name
          }))
          setVariantVouchers(variantsResult)
          setCategoryVouchers([])
          setProductVouchers([])
        } else if (res.data.data.voucherInfo.type === 'ALL') {
          const productsResult = res.data.data.allLists.map((product) => ({
            id: product.id,
            name: product.name
          }))
          setProductVouchers(productsResult)
          setCategoryVouchers([])
          setVariantVouchers([])
        }
      }
    } catch (err) {
      toastErrorMessage(err)
    }
  }

  const [valueTab, setValueTab] = useState('1')
  const handleChangeValueTab = (
    event: React.SyntheticEvent,
    newValue: string
  ) => {
    setValueTab(newValue)
  }

  //react-query: updateRangeVoucher
  const {
    mutate: mutateUpdateRangeVoucher,
    isPending: isPendingUpdateRangeVoucher
  } = useMutation({
    mutationFn: UpdateRangeVoucherAPI,
    onSuccess: () => {
      toast.success('Cập nhật RangeVoucher thành công')
      loadData()
      handleCloseModalUpdateRangeVoucher()
    },
    onError: (err) => {
      toastErrorMessage(err)
    }
  })
  const handleUpdateRangeVoucher = async (data) => {
    if (typeVoucher === 'CATEGORY') {
      data.categories_id = convertArrayIds(customCategories, valuesSelected)
    } else if (typeVoucher === 'PRODUCT') {
      data.products_id = convertArrayIds(customProducts, valuesSelected)
    } else if (typeVoucher === 'VARIANT') {
      data.variants_id = convertArrayIds(customVariants, valuesSelected)
    }
    data.type = typeVoucher
    data.voucher_id = Number(id)
    mutateUpdateRangeVoucher(data)
  }

  //react-query: assignVoucherToUsers
  const {
    mutate: mutateAssignVoucherToUsers,
    isPending: isPendingAssignVoucherToUsers
  } = useMutation({
    mutationFn: UpdateUserOwnVoucherAPI,
    onSuccess: () => {
      toast.success('Assign voucher to user successfully')
      loadData()
      handleCloseModalAssignVoucherToUsers()
    },
    onError: (err) => {
      toastErrorMessage(err)
    }
  })
  const handelAssignVoucherToUsers = async (data) => {
    data.users_id =
      usersSelect.length === 0 ? [] : convertArrayIds(customUsers, usersSelect)
    data.voucher_id = Number(id)
    mutateAssignVoucherToUsers(data)
  }

  return (
    <Stack spacing={8}>
      <Typography>Voucher Info {id}</Typography>
      <Stack
        spacing={5}
        className="text-gray-500"
        sx={{
          padding: 5,
          borderRadius: 5,
          background: (theme) => theme.palette._white_212b36.main,
          boxShadow:
            'rgba(145, 158, 171, 0.2) 0px 0px 2px 0px, rgba(145, 158, 171, 0.12) 0px 12px 24px -4px'
        }}
      >
        <Stack>
          <Stack direction="row">
            <Typography flex={1}>Name</Typography>
            <Typography flex={3}>{voucherInfo?.name}</Typography>
          </Stack>
          <Stack direction="row">
            <Typography flex={1}>Create at</Typography>
            <Typography flex={3}>
              {formatDateTime(voucherInfo?.created_at)}
            </Typography>
          </Stack>
          <Stack direction="row">
            <Typography flex={1}>Started at</Typography>
            <Typography flex={3}>
              {formatDateTime(voucherInfo?.started_at)}
            </Typography>
          </Stack>
          <Stack direction="row">
            <Typography flex={1}>Expired at</Typography>
            <Typography flex={3}>
              {formatDateTime(voucherInfo?.expired_at)}
            </Typography>
          </Stack>
          <Stack direction="row">
            <Typography flex={1}>Remaining quantity</Typography>
            <Typography flex={3}>{voucherInfo?.remaining_quantity}</Typography>
          </Stack>
          <Stack direction="row">
            <Typography flex={1}>Used quantity</Typography>
            <Typography flex={3}>{voucherInfo?.used_quantity}</Typography>
          </Stack>
          <Stack direction="row">
            <Typography flex={1}>Max price</Typography>
            <Typography flex={3}>{voucherInfo?.max_price}</Typography>
          </Stack>
          <Stack direction="row">
            <Typography flex={1}>Min price apply</Typography>
            <Typography flex={3}>{voucherInfo?.min_price_apply}</Typography>
          </Stack>
          <Stack direction="row">
            <Typography flex={1}>Percent discount</Typography>
            <Typography flex={3}>{voucherInfo?.percent_discount}</Typography>
          </Stack>
          <Stack direction="row">
            <Typography flex={1}>Type voucher</Typography>
            <Typography flex={3}>{voucherInfo?.type}</Typography>
          </Stack>
        </Stack>
        <Stack direction="row" spacing={8}>
          <Button
            variant="contained"
            onClick={handleOpenModalUpdateRangeVoucher}
          >
            Update Voucher Range
          </Button>
          <Button
            variant="contained"
            onClick={handleOpenModalAssignVoucherToUsers}
          >
            Assign Voucher To Users
          </Button>

          <ModalMUI
            open={openModalUpdateRangeVoucher}
            onClose={handleCloseModalUpdateRangeVoucher}
          >
            <Stack spacing={5}>
              <ChooseTypeVoucher
                valuesSelected={valuesSelected}
                setValuesSelected={setValuesSelected}
                typeVoucher={typeVoucher}
                setTypeVoucher={setTypeVoucher}
                products={customProducts}
                variants={customVariants}
                categories={customCategories}
              />

              <Stack direction="row" gap={4}>
                <Button
                  variant="contained"
                  onClick={handleCloseModalUpdateRangeVoucher}
                  color="error"
                >
                  Hủy
                </Button>
                <LoadingButton
                  variant="contained"
                  onClick={handleSubmit(handleUpdateRangeVoucher)}
                  loading={isPendingUpdateRangeVoucher}
                >
                  Update voucher
                </LoadingButton>
              </Stack>
            </Stack>
          </ModalMUI>

          <ModalMUI
            open={openModalAssignVoucherToUsers}
            onClose={handleCloseModalAssignVoucherToUsers}
          >
            <Stack spacing={4}>
              <AssignUserVoucher
                usersSelect={usersSelect}
                setUsersSelect={setUsersSelect}
                users={customUsers}
              />
              <Stack direction="row" spacing={4}>
                <Button
                  variant="contained"
                  color="error"
                  onClick={handleCloseModalAssignVoucherToUsers}
                >
                  Cancel
                </Button>
                <LoadingButton
                  variant="contained"
                  onClick={handleSubmit(handelAssignVoucherToUsers)}
                  loading={isPendingAssignVoucherToUsers}
                >
                  Confirm
                </LoadingButton>
              </Stack>
            </Stack>
          </ModalMUI>
        </Stack>
      </Stack>

      <Box
        sx={{
          width: '100%',
          typography: 'body1',
          borderRadius: '6px',
          '& .MuiTabPanel-root': {
            padding: 0
          }
        }}
      >
        <TabContext value={valueTab}>
          <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
            <TabList
              onChange={handleChangeValueTab}
              aria-label="lab API tabs example"
            >
              <Tab label="Categories" value="1" />
              <Tab label="Products" value="2" />
              <Tab label="Variants" value="3" />
              <Tab label="Users" value="4" />
            </TabList>
          </Box>
          <TabPanel value="1">
            <Stack sx={styleTab} gap={3}>
              {categoryVouchers?.map((category) => (
                <Stack
                  gap={3}
                  direction="row"
                  alignItems="center"
                  key={category.id}
                >
                  <img
                    src={category.image}
                    alt=""
                    width={50}
                    height={50}
                    className="rounded-full bg-[#E5E5E5]"
                  />
                  <Typography>
                    {category.name} (category_id: {category.id})
                  </Typography>
                </Stack>
              ))}
            </Stack>
          </TabPanel>

          <TabPanel value="2">
            <Stack sx={styleTab}>
              {productVouchers.map((product) => (
                <Stack key={product.id}>
                  <Typography>
                    {product.id} - {product.name}
                  </Typography>
                </Stack>
              ))}
            </Stack>
          </TabPanel>
          <TabPanel value="3">
            <Stack sx={styleTab}>
              {variantVouchers.map((variant) => (
                <Stack key={variant.id}>
                  <Typography>
                    {variant.productName} - {variant.size} - {variant.color}
                  </Typography>
                </Stack>
              ))}
            </Stack>
          </TabPanel>
          <TabPanel value="4">
            <Stack sx={styleTab} gap={3}>
              {userVouchers.map((item) => (
                <Stack
                  key={item.id}
                  direction="row"
                  alignItems="center"
                  gap={3}
                >
                  <img src={item.image} alt="" width={30} height={30} />
                  <Typography>
                    ID: {item.id}, Name: {item.name}, Email: {item.email}
                  </Typography>
                </Stack>
              ))}
            </Stack>
          </TabPanel>
        </TabContext>
      </Box>
    </Stack>
  )
}

export default function VoucherDetailPage() {
  return <MainLayout content={<VoucherDetail />} back={true} />
}
