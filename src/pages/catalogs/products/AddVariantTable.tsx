import React from 'react'
import { Box } from '@mui/material'
import Button from '@mui/material/Button'
import AddIcon from '@mui/icons-material/Add'
import EditIcon from '@mui/icons-material/Edit'
import DeleteIcon from '@mui/icons-material/DeleteOutlined'
import SaveIcon from '@mui/icons-material/Save'
import CancelIcon from '@mui/icons-material/Close'
import {
  GridRowsProp,
  GridRowModesModel,
  GridRowModes,
  DataGrid,
  GridColDef,
  GridToolbarContainer,
  GridActionsCellItem,
  GridEventListener,
  GridRowId,
  GridRowModel,
  GridRowEditStopReasons
} from '@mui/x-data-grid'
import { randomId } from '@mui/x-data-grid-generator'
import { CreateVariantAPI } from '@/api/VariantColorAPI'

const STYLE_HEADER = {
  fontWeight: 'bold'
}

interface EditToolbarProps {
  setRows: (newRows: (oldRows: GridRowsProp) => GridRowsProp) => void
  setRowModesModel: (
    newModel: (oldModel: GridRowModesModel) => GridRowModesModel
  ) => void
}

function EditToolbar(props: EditToolbarProps) {
  const { setRows, setRowModesModel } = props

  const handleClick = () => {
    const id = randomId()
    setRows((oldRows) => [...oldRows, { id }])
    setRowModesModel((oldModel) => ({
      ...oldModel,
      [id]: { mode: GridRowModes.Edit, fieldToFocus: 'name' }
    }))
  }

  return (
    <GridToolbarContainer>
      <Button color="primary" startIcon={<AddIcon />} onClick={handleClick}>
        Add variant
      </Button>
    </GridToolbarContainer>
  )
}

export default function AddVariantTable({ productId }) {
  const [rows, setRows] = React.useState<GridRowsProp>([])
  const [rowModesModel, setRowModesModel] = React.useState<GridRowModesModel>(
    {}
  )

  const handleRowEditStop: GridEventListener<'rowEditStop'> = (
    params,
    event
  ) => {
    if (params.reason === GridRowEditStopReasons.rowFocusOut) {
      event.defaultMuiPrevented = true
    }
  }

  const handleEditClick = (id: GridRowId) => () => {
    setRowModesModel({ ...rowModesModel, [id]: { mode: GridRowModes.Edit } })
  }

  const handleSaveClick = (id: GridRowId) => () => {
    setRowModesModel({ ...rowModesModel, [id]: { mode: GridRowModes.View } })
  }

  const handleDeleteClick = (id: GridRowId) => () => {
    setRows(rows.filter((row) => row.id !== id))
  }

  const handleCancelClick = (id: GridRowId) => () => {
    setRowModesModel({
      ...rowModesModel,
      [id]: { mode: GridRowModes.View, ignoreModifications: true }
    })

    const editedRow = rows.find((row) => row.id === id)
    if (editedRow!.isNew) {
      setRows(rows.filter((row) => row.id !== id))
    }
  }

  const processRowUpdate = (newRow: GridRowModel) => {
    const updatedRow = { ...newRow }
    setRows(rows.map((row) => (row.id === newRow.id ? updatedRow : row)))
    return updatedRow
  }

  const handleRowModesModelChange = (newRowModesModel: GridRowModesModel) => {
    setRowModesModel(newRowModesModel)
  }

  const columns: GridColDef[] = [
    {
      field: 'color_id',
      //   width: 180,
      editable: true,
      type: 'singleSelect',
      valueOptions: ['Red', 'Green', 'Blue'],
      renderHeader: () => <Box sx={STYLE_HEADER}>Color</Box>
    },
    {
      field: 'size',
      align: 'left',
      headerAlign: 'left',
      editable: true,
      type: 'singleSelect',
      valueOptions: ['XS', 'XL', 'XXL', 'XXXL'],
      renderHeader: () => <Box sx={STYLE_HEADER}>Size</Box>
    },
    {
      field: 'price_import',
      editable: true,
      renderHeader: () => <Box sx={STYLE_HEADER}>Price Import</Box>
    },
    {
      field: 'price_export',
      editable: true,
      renderHeader: () => <Box sx={STYLE_HEADER}>Price Export</Box>
    },
    // {
    //   field: 'price_sale',
    //   editable: true,
    //   renderHeader: () => <Box sx={STYLE_HEADER}>Price Sale</Box>
    // },
    {
      field: 'remain_quantity',
      editable: true,
      renderHeader: () => <Box sx={STYLE_HEADER}>Quantity</Box>
    },
    {
      field: 'actions',
      type: 'actions',
      renderHeader: () => <Box sx={STYLE_HEADER}>Actions</Box>,
      width: 100,
      cellClassName: 'actions',
      getActions: ({ id }) => {
        const isInEditMode = rowModesModel[id]?.mode === GridRowModes.Edit

        if (isInEditMode) {
          return [
            <GridActionsCellItem
              key={id}
              icon={<SaveIcon />}
              label="Save"
              sx={{
                color: 'primary.main'
              }}
              onClick={handleSaveClick(id)}
            />,
            <GridActionsCellItem
              key={id}
              icon={<CancelIcon />}
              label="Cancel"
              className="textPrimary"
              onClick={handleCancelClick(id)}
              color="inherit"
            />
          ]
        }

        return [
          <GridActionsCellItem
            key={id}
            icon={<EditIcon />}
            label="Edit"
            className="textPrimary"
            onClick={handleEditClick(id)}
            color="inherit"
          />,
          <GridActionsCellItem
            key={id}
            icon={<DeleteIcon />}
            label="Delete"
            onClick={handleDeleteClick(id)}
            color="inherit"
          />
        ]
      }
    }
  ]

  const handleAddVariant = async () => {
    const rowsFormat = rows.map((row) => {
      return {
        ...row,
        product_id: productId
      }
    })
    try {
      const res = await CreateVariantAPI(rowsFormat[0])
      console.log(res)
    } catch (err) {
      console.log(err)
    }
  }

  return (
    <Box>
      <Box
        sx={{
          height: 500,
          width: '100%',
          '& .actions': {
            color: 'text.secondary'
          },
          '& .textPrimary': {
            color: 'text.primary'
          }
        }}
      >
        <DataGrid
          rows={rows}
          columns={columns}
          editMode="row"
          rowModesModel={rowModesModel}
          onRowModesModelChange={handleRowModesModelChange}
          onRowEditStop={handleRowEditStop}
          processRowUpdate={processRowUpdate}
          slots={{
            toolbar: EditToolbar
          }}
          slotProps={{
            toolbar: { setRows, setRowModesModel }
          }}
          sx={{
            '& .MuiDataGrid-menuIcon': {
              display: 'none'
            },
            '& .MuiDataGrid-iconButtonContainer': {
              display: 'none'
            },
            '& .MuiDataGrid-columnSeparator': {
              display: 'none'
            },
            '& .MuiDataGrid-columnHeader': { '&:focus': { outline: 'none' } },
            '& .MuiDataGrid-cell': {
              '&:focus': { outline: 'none' },
              '&:hover': {
                cursor: 'pointer'
              }
            },
            '& .MuiSelect-select': {
              fontSize: '12px'
            },
            '& .MuiInputBase-input': {
              fontSize: '12px'
            }
          }}
        />
      </Box>
      <Button variant="contained" color="success" onClick={handleAddVariant}>
        Xong
      </Button>
    </Box>
  )
}
