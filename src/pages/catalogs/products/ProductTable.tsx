import React, { useEffect, useState } from 'react'
import Box from '@mui/material/Box'
import TableMUI from '@/components/TableMUI'
import { useNavigate } from 'react-router-dom'
import { convertCategory } from '@/utils/convertData'
import { Button } from '@mui/material'
import { STYLE_HEADER_TABLE } from '@/assets/styles/style'
import { GetAllCategoryAPI } from '@/api/CategoryAPI'

type ProductTablePropsType = {
  productList
  isLoading?: boolean
  isError?: boolean
  errorLabel?: string
}

export default function ProductTable({
  productList,
  isLoading,
  isError,
  errorLabel
}: ProductTablePropsType) {
  const navigate = useNavigate()

  const columns = [
    {
      field: 'id',
      maxWidth: 30,
      sortable: false,
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>ID</Box>
    },
    {
      field: 'name',
      minWidth: 200,
      sortable: false,
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>PRODUCT NAME</Box>
    },
    {
      field: 'category_name',
      sortable: false,
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>CATEGORY</Box>
    },
    {
      field: 'created_at',
      minWidth: 180,
      sortable: false,
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>CREATE AT</Box>
    },
    {
      field: 'actions',
      minWidth: 120,
      sortable: false,
      renderCell: (params) => (
        <Box sx={{ display: 'flex', gap: 1 }}>
          <Button
            size="small"
            variant="outlined"
            onClick={() => navigate(`/catalogs/products/${params.row.id}`)}
          >
            Xem chi tiết
          </Button>
        </Box>
      ),
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>ACTIONS</Box>
    }
  ]

  const { data: categories } = GetAllCategoryAPI()
  const [rows, setRows] = useState([])

  useEffect(() => {
    if (categories) {
      const categoriesMap = {}
      const categoryConvert = convertCategory(categories)
      categoryConvert?.forEach((category) => {
        categoriesMap[category.id] = category.name
      })
      const rowsWithCategoryNames = productList?.map((product) => {
        const categoryName = categoriesMap[product.category_id]
        return { ...product, category_name: categoryName }
      })
      setRows(rowsWithCategoryNames)
    }
  }, [productList, categories])

  return (
    <>
      <TableMUI
        columns={columns}
        rows={rows}
        isLoading={isLoading}
        isError={isError}
        errorLabel={errorLabel}
        loadingLabel="get All Product"
        checkboxSelection={false}
      />
    </>
  )
}
