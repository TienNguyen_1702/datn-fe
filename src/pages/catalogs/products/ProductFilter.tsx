import React, { useEffect, useState } from 'react'
import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Button,
  Stack,
  FormControl,
  MenuItem,
  Typography,
  InputLabel,
  Select,
  TextField,
  IconButton,
  InputBase,
  Paper
} from '@mui/material'
import { FilterAlt, Search } from '@mui/icons-material'
import { GetAllCategoryLeftAPI } from '@/api/CategoryAPI'
import { FilterProductAPIBodyType } from '@/api/ProductAPI'
import { useTranslation } from 'react-i18next'

export default function ProductFilter({ mutation }) {
  const [filterPost, setFilterPost] = useState<FilterProductAPIBodyType>()
  const { t } = useTranslation('translation', { keyPrefix: 'page.Product' })

  const { data: categoryLeafList } = GetAllCategoryLeftAPI()

  const handleFilterField = (event) => {
    const { name, value } = event.target
    let filteredValue = value // Giữ giá trị người dùng nhập vào

    // Kiểm tra nếu giá trị là rỗng, thiết lập là undefined
    if (name === 'min_price' || name === 'max_price') {
      if (value === '') {
        filteredValue = undefined
      } else {
        // Chuyển đổi giá trị thành số nếu không rỗng
        filteredValue = parseFloat(value)
      }
    }

    setFilterPost({
      ...filterPost,
      [name]: filteredValue
    })
  }

  useEffect(() => {
    mutation.mutate(filterPost)
  }, [filterPost])

  return (
    <Stack>
      <Accordion sx={{ boxShadow: 'none' }}>
        <AccordionSummary
          aria-controls="panel3-content"
          id="panel3-header"
          sx={{
            '&.Mui-focusVisible': {
              bgcolor: (theme) => theme.palette._white_212b36.main
            }
          }}
        >
          <Stack direction="row" gap={20}>
            <Button variant="outlined" endIcon={<FilterAlt />}>
              {t('Filter')}
            </Button>
            <Paper
              component="form"
              sx={{
                p: '2px 4px',
                display: 'flex',
                alignItems: 'center',
                width: 400
              }}
              onClick={(e) => e.stopPropagation()}
            >
              <InputBase
                sx={{ ml: 2, flex: 1, fontSize: 13 }}
                placeholder={t('Search product')}
                inputProps={{ 'aria-label': 'search google maps' }}
                value={filterPost?.search}
                name="search"
                onChange={handleFilterField}
              />
              <IconButton type="button" sx={{ p: '10px' }} aria-label="search">
                <Search />
              </IconButton>
            </Paper>
          </Stack>
        </AccordionSummary>
        <AccordionDetails>
          <Stack direction="row" gap={2}>
            <TextField
              value={filterPost?.min_price}
              name="min_price"
              sx={{ width: 150 }}
              label={t('Min price')}
              type="number"
              onChange={handleFilterField}
            />

            <TextField
              sx={{ width: 150 }}
              name="max_price"
              value={filterPost?.max_price}
              label={t('Max price')}
              type="number"
              onChange={handleFilterField}
            />
            <FormControl>
              <InputLabel id="demo-simple-select-label">
                {t('Number of rating stars')}
              </InputLabel>
              <Select
                name="rating"
                value={filterPost?.rating}
                sx={{ width: 150 }}
                id="demo-simple-select"
                labelId="demo-simple-select-label"
                label={t('Number of rating stars')}
                onChange={handleFilterField}
              >
                <MenuItem value="" disabled>
                  <Typography>
                    {t('Choose the number of rating stars')}
                  </Typography>
                </MenuItem>

                <MenuItem value={undefined}>
                  <Typography>{t('Not selected')}</Typography>
                </MenuItem>
                <MenuItem value={1}>
                  <Typography>1</Typography>
                </MenuItem>
                <MenuItem value={2}>
                  <Typography>2</Typography>
                </MenuItem>
                <MenuItem value={3}>
                  <Typography>3</Typography>
                </MenuItem>
                <MenuItem value={4}>
                  <Typography>4</Typography>
                </MenuItem>
                <MenuItem value={5}>
                  <Typography>5 </Typography>
                </MenuItem>
              </Select>
            </FormControl>

            <FormControl>
              <InputLabel id="demo-simple-select-label">
                {t('Category')}
              </InputLabel>
              <Select
                id="demo-simple-select"
                labelId="demo-simple-select-label"
                label={t('Category')}
                name="category_id"
                value={filterPost?.category_id}
                sx={{ width: 150 }}
                onChange={handleFilterField}
              >
                <MenuItem value="" disabled>
                  <Typography>{t('Select a category')}</Typography>
                </MenuItem>
                <MenuItem value={undefined}>
                  <Stack direction="row" spacing={2} alignItems="center">
                    <img width="30" height="30" src="" />
                    <Typography>{t('Not selected')}</Typography>
                  </Stack>
                </MenuItem>

                {categoryLeafList?.map((category) => (
                  <MenuItem key={category.id} value={category.id}>
                    <Stack direction="row" spacing={2} alignItems="center">
                      <img width="30" height="30" src={category.image || ''} />
                      <Typography> {category.name}</Typography>
                    </Stack>
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <FormControl>
              <InputLabel id="demo-simple-select-label">
                {t('Sort by')}
              </InputLabel>
              <Select
                name="sort_by"
                value={filterPost?.sort_by ?? ''}
                sx={{ width: 150 }}
                id="demo-simple-select"
                labelId="demo-simple-select-label"
                label="Sort by"
                onChange={handleFilterField}
              >
                <MenuItem value="" disabled>
                  <Typography>{t('Select sort type')}</Typography>
                </MenuItem>

                <MenuItem value="Đánh giá cao">
                  <Typography>Đánh giá cao</Typography>
                </MenuItem>
                <MenuItem value="Giá tăng dần">
                  <Typography>Giá tăng dần</Typography>
                </MenuItem>
                <MenuItem value="Giá giảm dần">
                  <Typography>Giá giảm dần</Typography>
                </MenuItem>
                <MenuItem value="Mới nhất">
                  <Typography>Mới nhất</Typography>
                </MenuItem>
                <MenuItem value="Phổ biến nhất">
                  <Typography>Phổ biến nhất</Typography>
                </MenuItem>
                <MenuItem value="Giảm giá nhiều nhất">
                  <Typography>Giảm giá nhiều nhất</Typography>
                </MenuItem>
              </Select>
            </FormControl>
          </Stack>
        </AccordionDetails>
        {/* <AccordionActions>
          <Button variant="outlined">Reset</Button>
          <Button variant="outlined">Áp dụng</Button>
        </AccordionActions> */}
      </Accordion>
    </Stack>
  )
}
