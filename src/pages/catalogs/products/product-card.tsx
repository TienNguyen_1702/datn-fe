import Box from '@mui/material/Box'
import Link from '@mui/material/Link'
import Card from '@mui/material/Card'
import Stack from '@mui/material/Stack'
import Typography from '@mui/material/Typography'
import { useNavigate } from 'react-router-dom'

import Label from './Label'
import { Tooltip } from '@mui/material'
// import ColorPreview from './color-preview'

// ----------------------------------------------------------------------

export default function ShopProductCard({ product }) {
  const navigate = useNavigate()
  const renderStatus = (
    <Label
      variant="filled"
      color={(product.status === 'sale' && 'error') || 'info'}
      sx={{
        zIndex: 9,
        top: 16,
        right: 16,
        position: 'absolute',
        textTransform: 'uppercase'
      }}
    >
      {product?.status}
    </Label>
  )

  const renderImg = (
    <Box
      component="img"
      alt={product.name}
      src={product.image}
      sx={{
        top: 0,
        width: 1,
        height: 1,
        objectFit: 'cover',
        position: 'absolute'
      }}
    />
  )

  const renderPrice = (
    <Typography variant="subtitle1" className="flex flex-col">
      <Typography
        component="span"
        sx={{
          color: 'red',
          fontWeight: 'bold',
          fontSize: 16
        }}
      >
        {product.price_sale.toLocaleString('it-IT', {
          style: 'currency',
          currency: 'VND'
        })}
      </Typography>
      <Typography
        component="span"
        sx={{
          color: 'text.disabled',
          textDecoration: 'line-through',
          fontSize: 14
        }}
      >
        {product.price_export.toLocaleString('it-IT', {
          style: 'currency',
          currency: 'VND'
        })}
      </Typography>
    </Typography>
  )

  return (
    <Card>
      <Box
        sx={{ pt: '100%', position: 'relative' }}
        className="hover:cursor-pointer"
        onClick={() => navigate(`/catalogs/products/${product.id}`)}
      >
        {product.status && renderStatus}

        {renderImg}
      </Box>

      <Stack spacing={1} sx={{ p: 3 }}>
        <Tooltip title={product.name}>
          <Link
            color="inherit"
            underline="hover"
            variant="subtitle2"
            noWrap
            className="hover:cursor-pointer"
            onClick={() => navigate(`/catalogs/products/${product.id}`)}
          >
            {product.name}
          </Link>
        </Tooltip>

        <Stack
          direction="row"
          alignItems="center"
          justifyContent="space-between"
        >
          {/* <ColorPreview colors={product.colors} /> */}
          {renderPrice}
        </Stack>
      </Stack>
    </Card>
  )
}
