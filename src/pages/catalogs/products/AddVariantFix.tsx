import { useState } from 'react'
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import { Controller, useForm } from 'react-hook-form'
import { toast } from 'react-toastify'
import ImageUploader from './File'
import { toastErrorMessage } from '@/utils/errorMessage'
import { Difference } from '@mui/icons-material'
import {
  FormControl,
  FormHelperText,
  InputLabel,
  MenuItem,
  Select,
  Snackbar,
  Stack,
  TextField,
  Typography
} from '@mui/material'
import { useMutation } from '@tanstack/react-query'
import { LoadingButton } from '@mui/lab'
import { CreateVariantAPI, GetAllColorAPI } from '@/api/VariantColorAPI'
import { useTranslation } from 'react-i18next'

const VARIANT_SIZE = [
  'XS',
  'S',
  'M',
  'L',
  'XL',
  '2XL',
  '3XL',
  '35',
  '36',
  '37',
  '38',
  '39',
  '40',
  '41',
  '42',
  '43',
  '44',
  '45'
]

const AddVariantFix = ({
  productId,
  loadDataProductDetail
}: {
  productId
  loadDataProductDetail?
}) => {
  //react-hook-form: validate chỉ cho name và manufacturer
  const {
    handleSubmit,
    control,
    formState: { errors },
    reset
  } = useForm()

  const { t } = useTranslation('translation', { keyPrefix: 'page.Product' })
  const { t: v } = useTranslation('translation', { keyPrefix: 'validate' })

  const [images, setImages] = useState<File[]>([])
  const [isCopyForm, setIsCopyForm] = useState<boolean>(false)

  const { data: colors } = GetAllColorAPI()

  const { mutate: mutateCreate, isPending: isPendingCreate } = useMutation({
    mutationFn: CreateVariantAPI,
    onSuccess: () => {
      toast.success('Tạo Variant thành công')
      if (loadDataProductDetail) {
        loadDataProductDetail()
      }
      if (isCopyForm === false) {
        reset()
        setImages([])
      }
    },
    onError: (err) => {
      toastErrorMessage(err)
    }
  })

  const handleAddVariant = async (data) => {
    const formData = new FormData()
    formData.append('product_id', productId)
    formData.append('size', data.size)
    formData.append('color_id', data.color_id)
    formData.append('price_import', data.price_import)
    formData.append('price_export', data.price_export)
    if (data.price_sale) {
      formData.append('price_sale', data.price_sale)
    }
    formData.append('remain_quantity', data.remain_quantity)

    // Append danh sách ảnh vào formData
    images.forEach((image) => {
      formData.append('image', image)
    })

    mutateCreate(formData)
  }

  const handleCopyForm = (data) => {
    setOpen(true)

    setIsCopyForm(!isCopyForm)
    console.log(data)
  }

  const [open, setOpen] = useState(false)

  const handleClose = (
    event: React.SyntheticEvent | Event,
    reason?: string
  ) => {
    if (reason === 'clickaway') {
      return
    }

    setOpen(false)
  }

  return (
    <>
      <Box>
        <Stack maxWidth="70%">
          {/* Start Body Modal */}
          <Box className="border-t border-l-0 border-r-0 border-b-0 border-dashed border-gray-300">
            <Stack py={5}>
              <Typography variant="_title">{t('Add Variant')}</Typography>
            </Stack>
            <Stack spacing={5}>
              <Stack direction="row" spacing={5}>
                <Controller
                  defaultValue=""
                  name="size"
                  control={control}
                  rules={{
                    required: v('This field is required')
                  }}
                  render={({ field }) => (
                    <FormControl error={!!errors.size} sx={{ flex: 1 }}>
                      <InputLabel id="demo-simple-select-label">
                        Size
                      </InputLabel>
                      <Select
                        id="demo-simple-select"
                        labelId="demo-simple-select-label"
                        label="Size"
                        {...field}
                      >
                        <MenuItem value="" disabled>
                          <Typography>{t('Select a size')}</Typography>
                        </MenuItem>
                        {VARIANT_SIZE.map((size) => (
                          <MenuItem key={size} value={size}>
                            <Typography>{size}</Typography>
                          </MenuItem>
                        ))}
                      </Select>
                      <FormHelperText>
                        {errors.size?.message?.toString()}
                      </FormHelperText>
                    </FormControl>
                  )}
                />

                <Controller
                  defaultValue=""
                  name="color_id"
                  control={control}
                  rules={{
                    required: v('This field is required')
                  }}
                  render={({ field }) => (
                    <FormControl error={!!errors.color_id} sx={{ flex: 1 }}>
                      <InputLabel id="demo-simple-select-label">
                        {t('Color')}
                      </InputLabel>
                      <Select
                        id="demo-simple-select"
                        labelId="demo-simple-select-label"
                        label={t('Color')}
                        {...field}
                      >
                        <MenuItem value="" disabled>
                          {t('Select a color')}
                        </MenuItem>

                        {colors?.map((color) => (
                          <MenuItem key={color.id} value={color.id}>
                            <Stack direction="row" alignItems="center">
                              <Box
                                sx={{
                                  backgroundColor: color.color_code,
                                  width: 15,
                                  height: 15,
                                  marginRight: '10px'
                                }}
                              ></Box>
                              <Typography> {color.name}</Typography>
                            </Stack>
                          </MenuItem>
                        ))}
                      </Select>
                      <FormHelperText>
                        {errors.color_id?.message?.toString()}
                      </FormHelperText>
                    </FormControl>
                  )}
                />
              </Stack>
              <Stack direction="row" spacing={5}>
                <Controller
                  defaultValue=""
                  name="price_export"
                  control={control}
                  rules={{
                    required: v('This field is required'),
                    min: { value: 1, message: v('Invalid value') }
                  }}
                  render={({ field }) => (
                    <TextField
                      sx={{ flex: 1 }}
                      type="number"
                      label="Price export"
                      error={!!errors.price_export}
                      helperText={errors.price_export?.message?.toString()}
                      {...field}
                    />
                  )}
                />
                <Controller
                  defaultValue=""
                  name="price_import"
                  control={control}
                  rules={{
                    required: v('This field is required'),
                    min: { value: 1, message: v('Invalid value') }
                  }}
                  render={({ field }) => (
                    <TextField
                      sx={{ flex: 1 }}
                      type="number"
                      label="Price import"
                      error={!!errors.price_import}
                      helperText={errors.price_import?.message?.toString()}
                      {...field}
                    />
                  )}
                />
              </Stack>
              <Stack direction="row" spacing={5}>
                {' '}
                <Controller
                  defaultValue=""
                  name="price_sale"
                  control={control}
                  rules={{
                    required: v('This field is required'),
                    min: { value: 1, message: v('Invalid value') }
                  }}
                  render={({ field }) => (
                    <TextField
                      sx={{ flex: 1 }}
                      type="number"
                      label="Price sale"
                      error={!!errors.price_sale}
                      helperText={errors.price_sale?.message?.toString()}
                      {...field}
                    />
                  )}
                />
                <Controller
                  defaultValue=""
                  name="remain_quantity"
                  control={control}
                  rules={{
                    required: v('This field is required'),
                    min: { value: 1, message: v('Invalid value') }
                  }}
                  render={({ field }) => (
                    <TextField
                      sx={{ flex: 1 }}
                      type="number"
                      label="Remain quantity"
                      error={!!errors.remain_quantity}
                      helperText={errors.remain_quantity?.message?.toString()}
                      {...field}
                    />
                  )}
                />
              </Stack>

              <ImageUploader images={images} setImages={setImages} />
            </Stack>
          </Box>

          {/* End Body Modal */}
        </Stack>
        <Stack direction="row" spacing={4}>
          <LoadingButton
            variant="contained"
            color="success"
            onClick={handleSubmit(handleAddVariant)}
            loading={isPendingCreate}
          >
            {t('Add Variant')}
          </LoadingButton>
          <Button
            startIcon={<Difference />}
            variant="outlined"
            onClick={handleSubmit(handleCopyForm)}
            sx={{
              color: isCopyForm === false ? '#1976d2' : '#d32f2f'
            }}
          >
            {isCopyForm === false ? 'Copy Form' : 'Cancel Copy Form'}
          </Button>
          <Snackbar
            open={open}
            autoHideDuration={600}
            onClose={handleClose}
            message={isCopyForm === true ? 'Đã copy form' : 'Đã hủy copy form'}
          />
        </Stack>
      </Box>
    </>
  )
}

export default AddVariantFix
