import { toastErrorMessage } from '@/utils/errorMessage'
import {
  FormControl,
  FormHelperText,
  InputLabel,
  MenuItem,
  Select,
  Stack,
  TextField,
  Typography
} from '@mui/material'
import { useMutation } from '@tanstack/react-query'
import React, { useEffect, useState } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { toast } from 'react-toastify'
import { useParams, useNavigate } from 'react-router-dom'
import DoubleInputDynamic from '../DoubleInputDynamic'
import { LoadingButton } from '@mui/lab'
import MainLayout from '@/components/MainLayout'
import { GetAllCategoryLeftAPI } from '@/api/CategoryAPI'
import { GetProductDetailAPI, UpdateProductAPI } from '@/api/ProductAPI'
import { useTranslation } from 'react-i18next'

export default function UpdateProductPage() {
  const { t } = useTranslation('translation', { keyPrefix: 'page.Product' })

  const UpdateProduct = () => {
    const { id: product_id } = useParams()
    const navigate = useNavigate()
    const {
      handleSubmit,
      control,
      formState: { errors }
    } = useForm()

    const { data: productData, refetch: refetchProductData } =
      GetProductDetailAPI(Number(product_id))

    // State để lưu danh sách thuộc tính othersProduct
    const [othersProduct, setOthersProduct] = useState(productData?.others)

    const { data: categoryLeafList = [] } = GetAllCategoryLeftAPI()

    //react-query: updateProduct
    const { mutate: mutateUpdate, isPending: isPendingUpdate } = useMutation({
      mutationFn: UpdateProductAPI,
      onSuccess: () => {
        toast.success('Cập nhật sản phẩm thành công')
        navigate(`/catalogs/products/${product_id}`)
        refetchProductData()
      },
      onError: (err) => {
        toastErrorMessage(err)
      }
    })

    useEffect(() => {
      setOthersProduct(productData?.others)
    }, [productData?.others])

    const handleUpdateProduct = async (data) => {
      data.id = Number(product_id)
      data.category_id = Number(data.category_id)
      data.others = othersProduct
      mutateUpdate(data)
    }

    return (
      <Stack sx={{ background: (theme) => theme.palette._white_212b36.main }}>
        {productData && (
          <Stack spacing={5} p={5}>
            <Stack direction="row" spacing={4}>
              <Controller
                defaultValue={productData?.name}
                name="name"
                control={control}
                rules={{
                  required: 'This field is required'
                }}
                render={({ field }) => (
                  <TextField
                    sx={{ flex: 1 }}
                    label={t('Product Name')}
                    error={!!errors.name}
                    helperText={errors.name?.message?.toString()}
                    {...field}
                  />
                )}
              />
              <Controller
                defaultValue={productData?.manufacturer}
                name="manufacturer"
                control={control}
                rules={{
                  required: 'This field is required'
                }}
                render={({ field }) => (
                  <TextField
                    sx={{ flex: 1 }}
                    label={t('Manufacturer')}
                    error={!!errors.manufacturer}
                    helperText={errors.manufacturer?.message?.toString()}
                    {...field}
                  />
                )}
              />
            </Stack>

            <Stack direction="row" spacing={4}>
              <Controller
                defaultValue={productData?.pattern}
                name="pattern"
                control={control}
                rules={{
                  required: 'This field is required'
                }}
                render={({ field }) => (
                  <TextField
                    sx={{ flex: 1 }}
                    label={t('Pattern')}
                    error={!!errors.pattern}
                    helperText={errors.pattern?.message?.toString()}
                    {...field}
                  />
                )}
              />
              <Controller
                defaultValue={productData?.material}
                name="material"
                control={control}
                rules={{
                  required: 'This field is required'
                }}
                render={({ field }) => (
                  <TextField
                    sx={{ flex: 1 }}
                    label={t('Material')}
                    error={!!errors.material}
                    helperText={errors.material?.message?.toString()}
                    {...field}
                  />
                )}
              />
            </Stack>
            <Stack direction="row" spacing={4}>
              <Controller
                defaultValue={productData?.style}
                name="style"
                control={control}
                rules={{
                  required: 'This field is required'
                }}
                render={({ field }) => (
                  <TextField
                    label={t('Style')}
                    sx={{ flex: 1 }}
                    error={!!errors.style}
                    helperText={errors.style?.message?.toString()}
                    {...field}
                  />
                )}
              />
              <FormControl error={!!errors.category_id} sx={{ flex: 1 }}>
                <Controller
                  defaultValue={productData?.category_id}
                  name="category_id"
                  control={control}
                  rules={{
                    required: 'This field is required'
                  }}
                  render={({ field }) => (
                    <>
                      <InputLabel id="demo-simple-select-label">
                        {t('Category')}
                      </InputLabel>
                      <Select
                        id="demo-simple-select"
                        labelId="demo-simple-select-label"
                        label={t('Category')}
                        {...field}
                      >
                        <MenuItem value="" disabled>
                          <Typography> {t('Select a category')} </Typography>
                        </MenuItem>

                        {categoryLeafList?.map((category) => (
                          <MenuItem key={category.id} value={category.id}>
                            <Stack
                              direction="row"
                              spacing={2}
                              alignItems="center"
                            >
                              <img
                                width="30"
                                height="30"
                                src={category.image || ''}
                              />
                              <Typography> {category.name}</Typography>
                            </Stack>
                          </MenuItem>
                        ))}
                      </Select>
                    </>
                  )}
                />
                <FormHelperText>
                  {errors.category_id?.message?.toString()}
                </FormHelperText>
              </FormControl>
            </Stack>
            <Stack>
              <Typography variant="_emphasize" py={2} className="text-gray-500">
                {t('Edit attributes')}
              </Typography>
              {productData.others && (
                <DoubleInputDynamic
                  data={othersProduct}
                  setData={setOthersProduct}
                  buttonName={t('Add attribute')}
                />
              )}
            </Stack>

            <Stack direction="row" spacing={4}>
              <LoadingButton
                variant="contained"
                onClick={handleSubmit(handleUpdateProduct)}
                loading={isPendingUpdate}
              >
                {t('Update Product')}
              </LoadingButton>
            </Stack>
          </Stack>
        )}
      </Stack>
    )
  }

  return (
    <MainLayout
      back={true}
      title={t('Update Product')}
      content={<UpdateProduct />}
    />
  )
}
