import { AddPhotoAlternate } from '@mui/icons-material'
import { Box, Button } from '@mui/material'
import React from 'react'
import { useDropzone } from 'react-dropzone'

interface ImageUploaderProps {
  images: File[]
  setImages: React.Dispatch<React.SetStateAction<File[]>>
}

const ImageUploader: React.FC<ImageUploaderProps> = ({ images, setImages }) => {
  const onDrop = (acceptedFiles: File[]) => {
    setImages((prevImages) => [...prevImages, ...acceptedFiles])
  }

  const { getRootProps, getInputProps } = useDropzone({ onDrop })

  const removeImage = (index: number) => {
    const newImages = [...images]
    newImages.splice(index, 1)
    setImages(newImages)
  }

  return (
    <Box>
      <Box {...getRootProps()} style={{ cursor: 'pointer' }}>
        <input {...getInputProps()} accept="image/*" />

        <Button
          startIcon={<AddPhotoAlternate />}
          variant="outlined"
          size="small"
        >
          Choose images
        </Button>
      </Box>
      <Box style={{ marginTop: '20px', display: 'flex' }}>
        {images.map((file, index) => (
          <Box
            key={index}
            style={{ position: 'relative', marginRight: '10px' }}
          >
            <img
              src={URL.createObjectURL(file)}
              alt={`preview-${index}`}
              style={{
                width: '100px',
                height: '100px',
                objectFit: 'cover',
                borderRadius: '4px'
              }}
            />
            <span
              style={{
                position: 'absolute',
                top: '5px',
                right: '5px',
                cursor: 'pointer',
                color: 'red'
              }}
              onClick={() => removeImage(index)}
            >
              x
            </span>
          </Box>
        ))}
      </Box>
    </Box>
  )
}

export default ImageUploader
