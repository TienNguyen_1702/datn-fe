import React, { useState } from 'react'
import {
  AddPhotoAlternate,
  DeleteOutlineOutlined,
  DriveFileRenameOutlineOutlined
} from '@mui/icons-material'
import { Controller, useForm } from 'react-hook-form'
import TableMUI from '@/components/TableMUI'
import {
  Button,
  FormControl,
  FormHelperText,
  InputLabel,
  MenuItem,
  Select,
  Stack,
  TextField,
  Box,
  Typography
} from '@mui/material'
import { STYLE_HEADER_TABLE } from '@/assets/styles/style'
import { toast } from 'react-toastify'
import { ConvertIdToName } from '@/utils/convertData'
import ModalMUI from '@/components/ModalMUI'
import { useMutation } from '@tanstack/react-query'
import { toastErrorMessage } from '@/utils/errorMessage'
import { LoadingButton } from '@mui/lab'
import {
  DeleteVariantAPI,
  GetAllColorAPI,
  UpdateVariantAPI
} from '@/api/VariantColorAPI'
import { useTranslation } from 'react-i18next'
import { AddVariantImageAPI } from '@/api/ProductAPI'
import ImageUploader from '../File'
import { ConvertIdToColorCode } from './TabProductDetail'

interface VariantType {
  id: number
  size?: string
  price_import?: number
  price_export?: number
  price_sale?: number
  remain_quantity?: number
  color_id?: number
  product_id?: number
}

const VARIANT_SIZE = [
  'XS',
  'S',
  'M',
  'L',
  'XL',
  '2XL',
  '3XL',
  '35',
  '36',
  '37',
  '38',
  '39',
  '40',
  '41',
  '42',
  '43',
  '44',
  '45'
]

export default function VariantTable({
  refetch,
  variants,
  loadDataProductDetail
}) {
  const {
    handleSubmit,
    reset,
    control,
    formState: { errors }
  } = useForm()

  const [openModalAddVariantImg, setOpenModalAddVariantImg] =
    useState<boolean>(false)
  const handleOpenModalAddVariantImg = () => setOpenModalAddVariantImg(true)
  const handleCloseModalAddVariantImg = () => {
    setOpenModalAddVariantImg(false)
    setImages([])
  }

  const [images, setImages] = useState<File[]>([])

  const { mutate: mutateAdd, isPending: isPendingEdit } = useMutation({
    mutationFn: AddVariantImageAPI,
    onSuccess: () => {
      toast.success('Thêm ảnh variant thành công')
      refetch()
      handleCloseModalAddVariantImg()
    },
    onError: (err) => {
      toastErrorMessage(err)
      handleCloseModalAddVariantImg()
    }
  })

  const handleAddVariantImage = (variant_id) => {
    const formData = new FormData()
    images.forEach((image) => {
      formData.append('image', image)
    })
    formData.append('id', variant_id)
    mutateAdd(formData)
  }

  const columns = [
    {
      field: 'size',
      sortable: false,
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>SIZE</Box>
    },
    {
      field: 'price_export',

      sortable: false,
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>PRICE EXPORT</Box>,
      renderCell: (rowData) => (
        <Box>
          {rowData?.row?.price_export.toLocaleString('it-IT', {
            style: 'currency',
            currency: 'VND'
          })}
        </Box>
      )
    },
    {
      field: 'price_import',
      sortable: false,
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>PRICE IMPORT</Box>,
      renderCell: (rowData) => (
        <Box>
          {' '}
          {rowData?.row?.price_import.toLocaleString('it-IT', {
            style: 'currency',
            currency: 'VND'
          })}
        </Box>
      )
    },
    {
      field: 'color_id',
      sortable: false,
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>COLOR</Box>,
      renderCell: (rowData) => (
        <Stack direction="row" alignItems="center">
          <Box
            sx={{
              border: '1px solid #000',
              backgroundColor: ConvertIdToColorCode(
                colors,
                rowData?.row?.color_id
              ),
              width: 15,
              height: 15,
              marginRight: '10px'
            }}
          ></Box>
          {/* <Typography> {color.name}</Typography> */}
        </Stack>
      )
    },
    {
      field: 'price_sale',
      sortable: false,
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>PRICE SALE</Box>,
      renderCell: (rowData) => (
        <Box>
          {rowData?.row?.price_sale.toLocaleString('it-IT', {
            style: 'currency',
            currency: 'VND'
          })}
        </Box>
      )
    },
    {
      field: 'remain_quantity',
      minWidth: 180,
      sortable: false,
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>REMAIN QUANTITY</Box>
    },
    {
      field: 'actions',
      minWidth: 230,
      sortable: false,
      renderCell: (params) => (
        <Stack direction="row" gap={3}>
          <DriveFileRenameOutlineOutlined
            style={{ cursor: 'pointer' }}
            onClick={() => {
              handleOpenModalUpdateVariant()
              setSelectedRows(params.row)
            }}
          />
          <DeleteOutlineOutlined
            style={{ cursor: 'pointer' }}
            onClick={() => {
              handleOpenModalDeleteVariant()
              setSelectedRows(params.row)
            }}
          />
          <AddPhotoAlternate
            style={{ cursor: 'pointer' }}
            onClick={handleOpenModalAddVariantImg}
          />
          <ModalMUI
            open={openModalAddVariantImg}
            onClose={handleCloseModalAddVariantImg}
            minWidth={500}
          >
            <Stack minHeight={200} justifyContent="flex-end" gap={8}>
              <ImageUploader images={images} setImages={setImages} />
              <Stack direction="row" gap={4} justifyContent="flex-end">
                <Button
                  variant="contained"
                  color="error"
                  onClick={handleCloseModalAddVariantImg}
                >
                  {t('Cancel')}
                </Button>
                <LoadingButton
                  variant="contained"
                  onClick={() => handleAddVariantImage(params.row.id)}
                  loading={isPendingEdit}
                >
                  {t('Add Image')}
                </LoadingButton>
              </Stack>
            </Stack>
          </ModalMUI>
        </Stack>
      ),
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>ACTIONS</Box>
    }
  ]

  const { data: colors = [] } = GetAllColorAPI()
  const { t } = useTranslation('translation', { keyPrefix: 'page.Product' })
  const { t: v } = useTranslation('translation', { keyPrefix: 'validate' })

  const [selectedRows, setSelectedRows] = useState<VariantType>()

  const [openModalUpdateVariant, setOpenModalUpdateVariant] =
    useState<boolean>(false)
  const handleOpenModalUpdateVariant = () => {
    setOpenModalUpdateVariant(true)
  }
  const handleCloseModalUpdateVariant = () => {
    reset()
    setOpenModalUpdateVariant(false)
  }

  const [openModalDeleteVariant, setOpenModalDeleteVariant] =
    useState<boolean>(false)
  const handleOpenModalDeleteVariant = () => {
    setOpenModalDeleteVariant(true)
  }
  const handleCloseModalDeleteVariant = () => {
    setOpenModalDeleteVariant(false)
  }

  //react-query: updateVariant
  const { mutate: mutateUpdate, isPending: isPendingUpdate } = useMutation({
    mutationFn: UpdateVariantAPI,
    onSuccess: () => {
      toast.success('Cập nhật variant thành công')
      loadDataProductDetail()
      handleCloseModalUpdateVariant()
    },
    onError: (err) => {
      toastErrorMessage(err)
    }
  })
  const handleUpdateVariant = async (data) => {
    data.id = selectedRows?.id
    data.price_import = Number(data.price_import)
    data.price_export = Number(data.price_export)
    data.remain_quantity = Number(data.remain_quantity)
    data.color_id = Number(data.color_id)
    if (data.price_sale) {
      data.price_sale = Number(data.price_sale)
    } else {
      delete data.price_sale
    }
    mutateUpdate(data)
  }

  //react-query: deleteVariant
  const { mutate: mutateDelete, isPending: isPendingDelete } = useMutation({
    mutationFn: DeleteVariantAPI,
    onSuccess: () => {
      toast.success('Xóa Variant thành công')
      loadDataProductDetail()
      handleCloseModalDeleteVariant()
    },
    onError: (err) => {
      toastErrorMessage(err)
    }
  })
  const handleDeleteVariant = async () => {
    mutateDelete(selectedRows?.id)
  }

  return (
    <>
      <TableMUI columns={columns} rows={variants} />
      <ModalMUI
        open={openModalUpdateVariant}
        onClose={handleCloseModalUpdateVariant}
      >
        <Stack spacing={5}>
          <Stack direction="row" spacing={4}>
            <Controller
              defaultValue={selectedRows?.size}
              name="size"
              control={control}
              rules={{
                required: 'This field is required'
              }}
              render={({ field }) => (
                <FormControl error={!!errors.size} sx={{ flex: 1 }}>
                  <InputLabel id="demo-simple-select-label">Size</InputLabel>
                  <Select
                    id="demo-simple-select"
                    labelId="demo-simple-select-label"
                    label="Size"
                    {...field}
                  >
                    <MenuItem value="" disabled>
                      <Typography>{t('Select a size')}</Typography>
                    </MenuItem>
                    {VARIANT_SIZE.map((size) => (
                      <MenuItem key={size} value={size}>
                        <Typography>{size}</Typography>
                      </MenuItem>
                    ))}
                  </Select>
                  <FormHelperText>
                    {errors.size?.message?.toString()}
                  </FormHelperText>
                </FormControl>
              )}
            />
            <Controller
              defaultValue={selectedRows?.price_import}
              name="price_import"
              control={control}
              rules={{
                required: v('This field is required'),
                min: { value: 1, message: v('Minimum value is 1') }
              }}
              render={({ field }) => (
                <TextField
                  type="number"
                  label={t('Price import')}
                  sx={{ flex: 1 }}
                  error={!!errors.price_import}
                  helperText={errors.price_import?.message?.toString()}
                  {...field}
                />
              )}
            />
          </Stack>

          <Stack direction="row" spacing={4}>
            <Controller
              defaultValue={selectedRows?.price_export}
              name="price_export"
              control={control}
              rules={{
                required: v('This field is required'),
                min: { value: 1, message: v('Minimum value is 1') }
              }}
              render={({ field }) => (
                <TextField
                  sx={{ flex: 1 }}
                  type="number"
                  label={t('Price export')}
                  error={!!errors.price_export}
                  helperText={errors.price_export?.message?.toString()}
                  {...field}
                />
              )}
            />

            <Controller
              defaultValue={selectedRows?.price_sale}
              name="price_sale"
              control={control}
              rules={{
                min: { value: 1, message: v('Minimum value is 1') }
              }}
              render={({ field }) => (
                <TextField
                  sx={{ flex: 1 }}
                  label={t('Price sale')}
                  type="number"
                  error={!!errors.price_sale}
                  helperText={errors.price_sale?.message?.toString()}
                  {...field}
                />
              )}
            />
          </Stack>
          <Stack direction="row" spacing={4}>
            <Controller
              defaultValue={selectedRows?.remain_quantity}
              name="remain_quantity"
              control={control}
              rules={{
                required: v('This field is required'),
                min: { value: 0, message: v('Minimum value is 0') }
              }}
              render={({ field }) => (
                <TextField
                  label="Remain quantity"
                  type="number"
                  sx={{ flex: 1 }}
                  error={!!errors.remain_quantity}
                  helperText={errors.remain_quantity?.message?.toString()}
                  {...field}
                />
              )}
            />

            <Controller
              defaultValue={selectedRows?.color_id}
              name="color_id"
              control={control}
              rules={{
                required: v('This field is required')
              }}
              render={({ field }) => (
                <FormControl error={!!errors.color_id} sx={{ flex: 1 }}>
                  <InputLabel id="demo-simple-select-label">Color</InputLabel>
                  <Select
                    id="demo-simple-select"
                    labelId="demo-simple-select-label"
                    label="Color"
                    {...field}
                  >
                    <MenuItem value="" disabled>
                      {t('Select a color')}
                    </MenuItem>

                    {colors?.map((color) => (
                      <MenuItem key={color.id} value={color.id}>
                        <Stack direction="row" alignItems="center">
                          <Box
                            sx={{
                              backgroundColor: color.color_code,
                              width: 15,
                              height: 15,
                              marginRight: '10px'
                            }}
                          ></Box>
                          <Typography> {color.name}</Typography>
                        </Stack>
                      </MenuItem>
                    ))}
                  </Select>
                  <FormHelperText>
                    {errors.color_id?.message?.toString()}
                  </FormHelperText>
                </FormControl>
              )}
            />
          </Stack>

          <Box className="flex gap-[15px]">
            <Button
              variant="contained"
              onClick={handleCloseModalUpdateVariant}
              color="error"
            >
              {t('Cancel')}
            </Button>
            <LoadingButton
              variant="contained"
              onClick={handleSubmit(handleUpdateVariant)}
              loading={isPendingUpdate}
            >
              {t('Update Variant')}
            </LoadingButton>
          </Box>
        </Stack>
      </ModalMUI>

      <ModalMUI
        open={openModalDeleteVariant}
        onClose={handleCloseModalDeleteVariant}
        minWidth={400}
      >
        <Stack spacing={3} alignItems="center">
          <Stack alignItems="center">
            <Typography>
              {t('Are you sure you want to delete Variant')} (size-color):
            </Typography>
            <Typography variant="_emphasize">
              ({selectedRows?.size} -
              {ConvertIdToName(colors, selectedRows?.color_id)})
            </Typography>
          </Stack>
          <Box className="flex gap-[15px]">
            <Button
              variant="contained"
              color="error"
              onClick={handleCloseModalDeleteVariant}
            >
              {t('Cancel')}
            </Button>
            <LoadingButton
              variant="contained"
              onClick={handleDeleteVariant}
              loading={isPendingDelete}
            >
              {t('Confirm')}
            </LoadingButton>
          </Box>
        </Stack>
      </ModalMUI>
    </>
  )
}
