import * as React from 'react'
import Lightbox from 'yet-another-react-lightbox'
import Thumbnails from 'yet-another-react-lightbox/plugins/thumbnails'
import Inline from 'yet-another-react-lightbox/plugins/inline'
import Zoom from 'yet-another-react-lightbox/plugins/zoom'
import 'yet-another-react-lightbox/plugins/captions.css'
import 'yet-another-react-lightbox/plugins/thumbnails.css'
import NoImage from '@/assets/images/NoImage.png'

export default function LightBoxLibrary({ slides }) {
  return (
    <Lightbox
      toolbar={{ buttons: [''] }}
      open={true}
      slides={slides?.length === 0 ? [{ src: NoImage }] : slides}
      plugins={[
        // Captions,
        // Fullscreen,
        // Slideshow,
        Thumbnails,
        // Video,
        Zoom,
        Inline
      ]}
      inline={{
        style: {
          flex: 4,
          maxHeight: '90vh',
          aspectRatio: '1/ 1'
        }
      }}
    />
  )
}
