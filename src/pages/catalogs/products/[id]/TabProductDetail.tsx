import { useEffect, useState } from 'react'
import {
  Alert,
  Box,
  ImageList,
  ImageListItem,
  ImageListItemBar,
  LinearProgress,
  Modal,
  Stack,
  Typography
} from '@mui/material'
import Tab from '@mui/material/Tab'
import TabContext from '@mui/lab/TabContext'
import TabList from '@mui/lab/TabList'
import TabPanel from '@mui/lab/TabPanel'
import { Avatar, Button, Rating } from '@mui/material'
import { formatDateTime } from '@/utils/format'
import { SolarPenBold } from '@/assets/icons/SolarPenBold'
import { styleBoxModal } from '@/assets/styles/style'
import { DeleteOutlineOutlined } from '@mui/icons-material'
import { toast } from 'react-toastify'
import VariantTable from './VariantTable'
import AddVariantFix from '../AddVariantFix'
import NoAvatar from '@/assets/images/NoAvatar.jpg'
import { toastErrorMessage } from '@/utils/errorMessage'
import { DeleteReviewProductAPI, GetAllReviewAPI } from '@/api/ReviewAPI'
import { useTranslation } from 'react-i18next'
import { GetAllColorAPI } from '@/api/VariantColorAPI'
import ModalMUI from '@/components/ModalMUI'
import { LoadingButton } from '@mui/lab'
import { useMutation } from '@tanstack/react-query'
import { DeleteVariantImageAPI } from '@/api/ProductAPI'

export const UserReview = ({
  id,
  rating,
  created_at,
  content,
  image,
  is_disable,
  user,
  loadReviewData
}) => {
  const { t } = useTranslation('translation', { keyPrefix: 'page.Product' })
  const [openModalDeleteReview, setOpenModalDeleteReview] =
    useState<boolean>(false)
  const handleOpenModalDeleteReview = () => {
    setOpenModalDeleteReview(true)
  }
  const handleCloseModalDeleteReview = () => {
    setOpenModalDeleteReview(false)
  }

  const handleDeleteReview = async () => {
    try {
      const res = await DeleteReviewProductAPI(id)
      if (res.status === 200) {
        toast.success('Xóa review thành công')
        handleCloseModalDeleteReview()
        loadReviewData()
      }
    } catch (err) {
      toastErrorMessage(err)
    }
  }

  return (
    <Stack
      direction="row"
      p={20}
      className="border-[1px] border-b border-dashed border-gray-300"
    >
      <Stack flex={1} spacing={2} alignItems="center" className="min-w-[250px]">
        <Box>
          <Avatar
            sx={{ bgcolor: 'green', height: 60, width: 60 }}
            src={user?.image ? user?.image : NoAvatar}
          />
        </Box>
        <Stack alignItems="center">
          <Box className="text-[14px] font-bold">
            {user?.name ? user?.name : 'No Name'}
          </Box>
          <Box className="text-gray-500 text-[12px]">{user.email}</Box>
        </Stack>
        <Box className="text-[12px] text-gray-600">
          {formatDateTime(created_at)}
        </Box>
      </Stack>
      <Box className="flex-[8] flex flex-col gap-[5px] justify-center">
        <Box>
          <Rating
            size="small"
            name="half-rating-read"
            value={rating}
            precision={0.1}
            readOnly
          />
        </Box>
        <Box className="text-normal">{content}</Box>
        {image && (
          <Box>
            <img src={image} alt="" width={100} className="rounded-[15px]" />
          </Box>
        )}
      </Box>
      <Box className="flex items-center flex-[5]">
        {is_disable && (
          <Alert severity="error" className="!text-[13px]">
            {t('Product review is disabled')}
          </Alert>
        )}
      </Box>
      <Box className="flex items-center hover:cursor-pointer">
        <DeleteOutlineOutlined onClick={handleOpenModalDeleteReview} />
        <Modal
          open={openModalDeleteReview}
          onClose={handleCloseModalDeleteReview}
        >
          <Box sx={styleBoxModal}>
            <Box className="flex flex-col gap-[15px]">
              <Box>
                {t('Are you sure you want to delete Review')}
                <span className="font-bold px-[5px]">{content}</span>
              </Box>
              <Box className="flex gap-[10px]">
                <Button
                  variant="contained"
                  color="error"
                  onClick={handleCloseModalDeleteReview}
                >
                  {t('Cancel')}
                </Button>
                <Button variant="contained" onClick={handleDeleteReview}>
                  {t('Confirm')}
                </Button>
              </Box>
            </Box>
          </Box>
        </Modal>
      </Box>
    </Stack>
  )
}

const LinerBox = ({ name, value, valueName }) => {
  return (
    <Stack direction="row" alignItems="center" spacing={3} px={8}>
      <Typography flex={2}>{name}</Typography>

      <LinearProgress
        variant="determinate"
        value={value}
        sx={{
          minWidth: 160,
          backgroundColor: '#BCBFC2',
          borderRadius: '10px',
          '& .MuiLinearProgress-bar': {
            borderRadius: '10px',
            backgroundColor: '#212B36' // Đổi màu nền thành màu đỏ
          }
        }}
      />
      <Typography flex={1}>{valueName}</Typography>
    </Stack>
  )
}

const ReviewTab = ({ product_id, countReview }) => {
  const { t } = useTranslation('translation', { keyPrefix: 'page.Product' })

  const { data, refetch: refetchReviewData } = GetAllReviewAPI({
    product_id
  })

  const result = {
    1: 0,
    2: 0,
    3: 0,
    4: 0,
    5: 0
  }

  // Duyệt qua mảng ratingCounts và cập nhật giá trị tương ứng trong result
  data?.ratingCounts?.forEach((item) => {
    const rating = item.rating
    const count = item._count.rating
    result[rating] = count
  })

  return (
    <Box>
      <Box className="flex border-b border-[1px] border-dashed border-gray-300">
        <Box className="flex-[1] flex flex-col items-center  py-[30px]">
          <Box className="text-normal">{t('Average rating')}</Box>
          <Box className="text-[28px] font-bold py-[10px]">{countReview}/5</Box>
          <Box>
            <Rating
              name="half-rating-read"
              precision={0.1}
              readOnly
              value={Number(countReview?.avg_rating)}
            />
          </Box>
          <Box className="text-normal"></Box>
        </Box>
        <Box className="flex-[1] flex flex-col gap-[10px] border-l-[1px] border-r-[1px] border-dashed border-t-0 border-b-0 border-gray-300 py-[30px]">
          <LinerBox
            name="5 star"
            value={(Number(result[5]) / Number(countReview)) * 100}
            valueName={result[5]}
          />
          <LinerBox
            name="4 star"
            value={(Number(result[4]) / Number(countReview)) * 100}
            valueName={result[4]}
          />
          <LinerBox
            name="3 star"
            value={(Number(result[3]) / Number(countReview)) * 100}
            valueName={result[3]}
          />
          <LinerBox
            name="2 star"
            value={(Number(result[2]) / Number(countReview)) * 100}
            valueName={result[2]}
          />
          <LinerBox
            name="1 star"
            value={(Number(result[1]) / Number(countReview)) * 100}
            valueName={result[1]}
          />
        </Box>
        <Box className="flex-[1] flex items-center justify-center">
          <Button
            size="large"
            startIcon={<SolarPenBold />}
            className="!text-[#212121] !font-bold !bg-gray-100 hover:!bg-gray-200"
          >
            {t('Write your review')}
          </Button>
        </Box>
      </Box>
      <Box className="flex flex-col ">
        {data?.reviews?.map((item) => (
          <UserReview
            key={item?.id}
            id={item?.id}
            rating={item?.rating}
            created_at={item?.created_at}
            content={item?.content}
            image={item?.image}
            is_disable={item?.is_disable}
            user={item?.user}
            loadReviewData={refetchReviewData}
          />
        ))}
      </Box>
      <Box className="flex items-center justify-center py-[20px]">
        {/* <Pagination
          count={10}
          sx={{
            '& .MuiPaginationItem-root.Mui-selected': {
              backgroundColor: '#212121',
              color: '#fff'
            }
          }}
        /> */}
      </Box>
    </Box>
  )
}

export function ConvertIdToColorCode(array, id) {
  const result = array?.find((cat) => cat.id === id)
  if (result !== undefined) {
    return result.color_code
  }
  return ''
}

export default function TabProductDetail({
  variants,
  product_id,
  refetch,
  countReview,
  productImage
}) {
  const [value, setValue] = useState(() => {
    // Lấy giá trị từ localStorage, nếu không có thì sử dụng giá trị mặc định là '1'
    return localStorage.getItem('currentTabProductDetail') || '1'
  })

  const { t } = useTranslation('translation', { keyPrefix: 'page.Product' })

  const { data } = GetAllColorAPI()

  const [isAddVariant, setIsAddVariant] = useState(false)

  const handleChange = (event: React.SyntheticEvent, newValue: string) => {
    setValue(newValue)
  }

  useEffect(() => {
    // Lưu giá trị của Tab vào localStorage khi giá trị thay đổi
    localStorage.setItem('currentTabProductDetail', value)
  }, [value])

  const [openModalDeleteProductImg, setOpenModalDeleteProductImg] =
    useState<boolean>(false)
  const handleOpenModalDeleteProductImg = () =>
    setOpenModalDeleteProductImg(true)
  const handleCloseModalDeleteProductImg = () => {
    setOpenModalDeleteProductImg(false)
  }

  const { mutate: mutateDelete, isPending: isPendingDelete } = useMutation({
    mutationFn: DeleteVariantImageAPI,
    onSuccess: () => {
      toast.success('Xóa ảnh variant thành công')
      refetch()
      handleCloseModalDeleteProductImg()
    },
    onError: (err) => {
      toastErrorMessage(err)
      handleCloseModalDeleteProductImg()
    }
  })

  const handleDeleteProductImage = (image_id) => {
    console.log(image_id)
    mutateDelete(image_id)
  }

  return (
    <Box
      sx={{
        width: '100%',
        typography: 'body1',
        borderRadius: '6px',
        '& .MuiTabPanel-root': {
          padding: 0
        }
      }}
    >
      <TabContext value={value}>
        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
          <TabList onChange={handleChange} aria-label="lab API tabs example">
            <Tab label={t('Variant')} value="1" />

            <Tab label={t('Reviews')} value="2" />
            <Tab label={t('Update image')} value="3" />
          </TabList>
        </Box>
        <TabPanel value="1">
          <Box className="p-[20px] ">
            {isAddVariant === false && (
              <Button
                variant="contained"
                color="secondary"
                className=" !my-[10px]"
                onClick={() => setIsAddVariant(true)}
              >
                {t('Add Variant')}
              </Button>
            )}
            {isAddVariant === true && (
              <AddVariantFix
                productId={product_id}
                loadDataProductDetail={refetch}
              />
            )}
            <Box className="my-[20px]"></Box>
            <VariantTable
              refetch={refetch}
              variants={variants}
              loadDataProductDetail={refetch}
            />
          </Box>
        </TabPanel>

        <TabPanel value="2">
          <ReviewTab product_id={product_id} countReview={countReview} />
        </TabPanel>
        <TabPanel value="3">
          <ImageList sx={{ width: '100%' }} cols={4} gap={8}>
            {productImage.map((item) => (
              <ImageListItem key={item.image}>
                <img
                  srcSet={`${item.image}?w=248&fit=crop&auto=format&dpr=2 2x`}
                  src={`${item.image}?w=248&fit=crop&auto=format`}
                  loading="lazy"
                  alt=""
                />
                <ImageListItemBar
                  title={
                    <Stack direction="row" alignItems="center">
                      <Box
                        sx={{
                          border: '1px solid #fff',
                          backgroundColor: ConvertIdToColorCode(
                            data,
                            item.color_id
                          ),
                          width: 15,
                          height: 15,
                          marginRight: '10px'
                        }}
                      ></Box>
                      {/* <Typography> {color.name}</Typography> */}
                    </Stack>
                  }
                  actionIcon={
                    <Stack direction="row" gap={3} padding={2}>
                      {/* <Button
                        variant="contained"
                        onClick={handleOpenModalEditProductImg}
                      >
                        Edit
                      </Button>
                      <ModalMUI
                        open={openModalEditProductImg}
                        onClose={handleCloseModalEditProductImg}
                        minWidth={500}
                      >
                        <UploadImage
                          setSelectedImage={setSelectedImage}
                          selectedImage={selectedImage}
                        />
                        <LoadingButton
                          variant="contained"
                          onClick={() => handleEditProductImage(item.id)}
                          loading={isPendingEdit}
                        >
                          Update
                        </LoadingButton>
                      </ModalMUI> */}
                      <LoadingButton
                        size="small"
                        variant="contained"
                        color="error"
                        onClick={handleOpenModalDeleteProductImg}
                        loading={isPendingDelete}
                      >
                        {t('Delete')}
                      </LoadingButton>
                      <ModalMUI
                        open={openModalDeleteProductImg}
                        onClose={handleCloseModalDeleteProductImg}
                        minWidth={500}
                      >
                        <Stack spacing={4} alignItems="center">
                          <Typography>
                            {t('Are you sure you want to delete Product Image')}
                          </Typography>
                          <Stack
                            direction="row"
                            justifyContent="center"
                            spacing={4}
                          >
                            <Button
                              variant="contained"
                              color="error"
                              onClick={handleCloseModalDeleteProductImg}
                            >
                              {t('Cancel')}
                            </Button>
                            <LoadingButton
                              variant="contained"
                              onClick={() => handleDeleteProductImage(item.id)}
                              // loading={isPendingDelete}
                            >
                              {t('Confirm')}
                            </LoadingButton>
                          </Stack>
                        </Stack>
                      </ModalMUI>
                    </Stack>
                  }
                />
              </ImageListItem>
            ))}
          </ImageList>
        </TabPanel>
      </TabContext>
    </Box>
  )
}
