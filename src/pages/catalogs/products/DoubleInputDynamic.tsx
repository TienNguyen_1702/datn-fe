import { useState } from 'react'
import { Box, Button, Stack, TextField } from '@mui/material'
import DeleteOutlineOutlinedIcon from '@mui/icons-material/DeleteOutlineOutlined'

//Tham khảo component tại https://github.com/chaoocharles/add-remove-form-field/blob/main/src/App.js
function DoubleInputDynamic({ data, setData, buttonName }) {
  const [error, setError] = useState(false)

  const handleInputChange = (index, name, value) => {
    setError(false)
    setData((prevData) => {
      const newData = [...prevData]
      newData[index] = { ...newData[index], [name]: value }
      return newData
    })
  }

  const handleRemove = (index) => {
    setError(false)
    setData((prevData) => {
      const newData = [...prevData]
      newData.splice(index, 1)
      return newData
    })
  }

  const handleAdd = () => {
    const isAnyInputEmpty = data.some(
      (item) => item.name === '' || item.value === ''
    )
    if (!isAnyInputEmpty) {
      setData((prevData) => [...prevData, { name: '', value: '' }])
    } else {
      setError(true)
    }
  }

  return (
    <Stack spacing={2} pt={1}>
      {data?.map((item, index) => (
        <Stack direction="row" key={index} spacing={3} alignItems="center">
          <TextField
            sx={{ flex: 1 }}
            label="Name"
            variant="outlined"
            value={item.name}
            onChange={(e) => handleInputChange(index, 'name', e.target.value)}
            required
            error={error && index === data.length - 1}
            helperText={
              error && index === data.length - 1 ? 'This field is required' : ''
            }
          />
          <TextField
            sx={{ flex: 1 }}
            label="Value"
            variant="outlined"
            value={item.value}
            onChange={(e) => handleInputChange(index, 'value', e.target.value)}
            required
            error={error && index === data.length - 1}
            helperText={
              error && index === data.length - 1 ? 'This field is required' : ''
            }
          />
          {data.length !== 0 && (
            <DeleteOutlineOutlinedIcon
              onClick={() => handleRemove(index)}
              sx={{
                color: '#e53935',
                '&:hover': {
                  cursor: 'pointer'
                }
              }}
            />
          )}
        </Stack>
      ))}
      <Box>
        <Button variant="outlined" onClick={handleAdd}>
          {buttonName}
        </Button>
      </Box>
    </Stack>
  )
}

export default DoubleInputDynamic
