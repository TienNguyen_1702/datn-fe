import MainLayout from '@/components/MainLayout'
import { Box, Stack, Typography } from '@mui/material'
import { useParams } from 'react-router-dom'
import { selectBgColorStatus, selectColorStatus } from '@/utils/select'
import { formatDateTime } from '@/utils/format'
import { SolarPenBold } from '@/assets/icons/SolarPenBold'
import { STYLE_HEADER_TABLE } from '@/assets/styles/style'
import TableMUI from '@/components/TableMUI'
import { useState } from 'react'
import { toastErrorMessage } from '@/utils/errorMessage'
import { toast } from 'react-toastify'
import LoadingComponent from '@/components/loading'
import { useMutation } from '@tanstack/react-query'
import { LoadingButton } from '@mui/lab'
import { GetOrderDetailAPI, UpdateStateOrderAPI } from '@/api/OrderAPI'
import { useTranslation } from 'react-i18next'

const columns = [
  {
    field: 'product_name',
    minWidth: 280,
    sortable: false,
    renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>PRODUCT NAME</Box>,
    renderCell: (rowData) => <Box>{rowData?.row?.variant?.product?.name}</Box>
  },
  {
    field: 'product_image',
    sortable: false,
    renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>IMAGE</Box>,
    renderCell: (rowData) => (
      <Stack>
        <img
          width={30}
          height={30}
          src={rowData?.row?.variant?.product?.product_image[0]?.image}
        />
      </Stack>
    )
  },
  {
    field: 'color',
    minWidth: 100,
    sortable: false,
    renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>COLOR</Box>,
    renderCell: (rowData) => <Box>{rowData?.row?.variant?.color?.name}</Box>
  },
  {
    field: 'size',
    minWidth: 100,
    sortable: false,
    renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>SIZE</Box>,
    renderCell: (rowData) => <Box>{rowData?.row?.variant?.size}</Box>
  },
  {
    field: 'quantity',
    minWidth: 100,
    sortable: false,
    renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>QUANTITY</Box>,
    renderCell: (rowData) => <Box>{rowData?.row?.quantity}</Box>
  },
  {
    field: 'price',
    minWidth: 100,
    sortable: false,
    renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>PRICE</Box>,
    renderCell: (rowData) => (
      <Box>
        {' '}
        {rowData?.row?.price.toLocaleString('it-IT', {
          style: 'currency',
          currency: 'VND'
        })}
      </Box>
    )
  }
]

const OrderDetail = () => {
  const { id } = useParams()

  const { t } = useTranslation('translation', { keyPrefix: 'page.Order' })

  const {
    data: orderDetails,
    isLoading,
    isError,
    error,
    refetch
  } = GetOrderDetailAPI(Number(id))

  const [selectedStatus, setSelectedStatus] = useState('')

  const handleChangeStatus = (event) => {
    setSelectedStatus(event.target.value)
  }

  //react-query: updateStateOrder
  const {
    mutate: mutateUpdateStateOrder,
    isPending: isPendingUpdateStateOrder
  } = useMutation({
    mutationFn: UpdateStateOrderAPI,
    onSuccess: () => {
      toast.success('Cập nhật state order thành công')
      refetch()
    },
    onError: (err) => {
      toastErrorMessage(err)
    }
  })
  const handleUpdateStateOrder = async () => {
    mutateUpdateStateOrder({ order_id: Number(id), new_state: selectedStatus })
  }

  return (
    <>
      {isLoading || isError ? (
        <LoadingComponent
          errorLabel={error?.message}
          isError={isError}
          loadingLabel={t('Loading order information')}
        />
      ) : (
        <Box>
          <Box className="flex items-center justify-between">
            <Box className="flex gap-[10px] items-center">
              <Typography variant="_subtitle">
                {t('Order')} #{id}
              </Typography>
              <Box
                className="w-[80px] text-[12px] font-semibold flex items-center justify-center py-[3px] rounded-[4px]"
                sx={{
                  color: selectColorStatus(orderDetails?.state),
                  bgcolor: selectBgColorStatus(orderDetails?.state)
                }}
              >
                {orderDetails?.state}
              </Box>
            </Box>
            <Box className="flex items-center gap-[10px] ">
              <Box className="input_component">
                <select
                  className="select w-[120px]"
                  value={selectedStatus || orderDetails?.state}
                  onChange={handleChangeStatus}
                >
                  <option value="WAITING">{t('Waiting')}</option>
                  <option value="CONFIRM">{t('Confirm')}</option>
                  <option value="SHIPPING">{t('Shipping')}</option>
                  <option value="DONE">{t('Done')}</option>
                  <option value="CANCEL">{t('Cancel')}</option>
                </select>
              </Box>
              <LoadingButton
                variant="contained"
                size="medium"
                onClick={handleUpdateStateOrder}
                loading={isPendingUpdateStateOrder}
              >
                {t('Update Status')}
              </LoadingButton>
            </Box>
          </Box>
          <Box>
            <Box className="py-[10px] text-gray-500 text-[12px]">
              {formatDateTime(orderDetails?.created_at)}
            </Box>
            <Stack direction="column-reverse" spacing={5}>
              <Box
                sx={{
                  background: (theme) => theme.palette._white_212b36.main,
                  boxShadow:
                    'rgba(145, 158, 171, 0.2) 0px 0px 2px 0px, rgba(145, 158, 171, 0.12) 0px 12px 24px -4px'
                }}
                className=" flex-[2] rounded-[16px] min-h-[600px] px-[15px] py-[20px]"
              >
                <Box className="flex items-center justify-between pb-[20px]">
                  <Typography variant="_subtitle">{t('Details')}</Typography>
                  <Box>
                    <SolarPenBold />
                  </Box>
                </Box>
                <TableMUI
                  columns={columns}
                  rows={
                    orderDetails?.order_detail ? orderDetails.order_detail : []
                  }
                  checkboxSelection={false}
                />
                <Box className="pt-[30px] border-b border-t-0 border-l-0 border-r-0 border-[1px] border-dashed border-gray-300"></Box>
                <Stack>
                  <Stack
                    paddingY={5}
                    className="border-b border-t-0 border-l-0 border-r-0 border-[1px] border-dashed border-gray-300"
                  >
                    <Stack
                      pb={2}
                      direction="row"
                      alignItems="center"
                      justifyContent="space-between"
                    >
                      <Typography variant="_subtitle">
                        {t('Customer Info')}
                      </Typography>
                      <Box>
                        <SolarPenBold />
                      </Box>
                    </Stack>
                    <Stack spacing={1}>
                      <Stack direction="row" justifyContent="space-between ">
                        <Typography className=" text-gray-500">
                          {t('Recipient name')}
                        </Typography>
                        <Typography>
                          {orderDetails?.recipient_information.name}
                        </Typography>
                      </Stack>
                      <Stack direction="row" justifyContent="space-between ">
                        <Typography className=" text-gray-500">
                          {t('Phone number')}
                        </Typography>
                        <Typography>
                          {orderDetails?.recipient_information.phone_number}
                        </Typography>
                      </Stack>
                      <Stack direction="row" justifyContent="space-between ">
                        <Typography className=" text-gray-500">
                          {t('Address')}
                        </Typography>
                        <Typography>
                          {orderDetails?.recipient_information.address}
                        </Typography>
                      </Stack>
                    </Stack>
                  </Stack>
                  <Stack
                    paddingY={5}
                    className="border-b border-t-0 border-l-0 border-r-0 border-[1px] border-dashed border-gray-300"
                  >
                    <Stack
                      pb={2}
                      direction="row"
                      alignItems="center"
                      justifyContent="space-between"
                    >
                      <Typography variant="_subtitle">
                        {t('Shipping Method')}
                      </Typography>
                      <Box>
                        <SolarPenBold />
                      </Box>
                    </Stack>
                    <Stack spacing={1}>
                      <Stack direction="row" justifyContent="space-between ">
                        <Typography className=" text-gray-500">
                          {t('Ship by')}
                        </Typography>
                        <Typography>
                          {orderDetails?.shipping_method.name}
                        </Typography>
                      </Stack>
                      <Stack direction="row" justifyContent="space-between ">
                        <Typography className=" text-gray-500">
                          {t('Ship price')}
                        </Typography>
                        <Typography>
                          {orderDetails?.shipping_method.price.toLocaleString(
                            'it-IT',
                            {
                              style: 'currency',
                              currency: 'VND'
                            }
                          )}
                        </Typography>
                      </Stack>
                    </Stack>
                  </Stack>

                  <Stack
                    paddingY={5}
                    className="border-b border-t-0 border-l-0 border-r-0 border-[1px] border-dashed border-gray-300"
                  >
                    <Stack
                      pb={2}
                      direction="row"
                      alignItems="center"
                      justifyContent="space-between"
                    >
                      <Typography variant="_subtitle">
                        {t('Payment Method')}
                      </Typography>
                      <Box>
                        <SolarPenBold />
                      </Box>
                    </Stack>
                    <Stack spacing={1}>
                      <Stack direction="row" justifyContent="space-between ">
                        <Typography className=" text-gray-500">
                          {t('Payment by')}
                        </Typography>
                        <Typography>
                          {orderDetails?.payment_method.name}
                        </Typography>
                      </Stack>
                    </Stack>
                  </Stack>
                </Stack>
                <Box className="flex justify-end pt-[20px]">
                  <Stack spacing={2} className="min-w-[400px]">
                    <Stack direction="row" spacing={8}>
                      <Typography className=" text-gray-500" flex={1}>
                        {t('Total price of products')}
                      </Typography>
                      <Typography variant="_emphasize" flex={1}>
                        {orderDetails?.amount.toLocaleString('it-IT', {
                          style: 'currency',
                          currency: 'VND'
                        })}
                      </Typography>
                    </Stack>
                    <Stack direction="row" spacing={8}>
                      <Typography className=" text-gray-500" flex={1}>
                        {t('Ship price')}
                      </Typography>
                      <Typography variant="_emphasize" flex={1}>
                        +
                        {orderDetails?.shipping_method.price.toLocaleString(
                          'it-IT',
                          {
                            style: 'currency',
                            currency: 'VND'
                          }
                        )}
                      </Typography>
                    </Stack>

                    <Stack direction="row" spacing={8}>
                      <Typography
                        className=" text-gray-500"
                        flex={1}
                        variant="_subtitle"
                      >
                        {t('Total order amount')}
                      </Typography>
                      <Typography variant="_subtitle" flex={1}>
                        {(
                          Number(orderDetails?.amount) +
                          Number(orderDetails?.shipping_method.price)
                        ).toLocaleString('it-IT', {
                          style: 'currency',
                          currency: 'VND'
                        })}
                      </Typography>
                    </Stack>
                  </Stack>
                </Box>
              </Box>
            </Stack>
          </Box>
        </Box>
      )}
    </>
  )
}
export default function OrderDetailPage() {
  return <MainLayout content={<OrderDetail />} back={true} />
}
