'use client'
import React, { useState } from 'react'
import AuthenticationComponent from '@/components/Authentication/Authentication'
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import FacebookOutlined from '@mui/icons-material/FacebookOutlined'
import Google from '@mui/icons-material/Google'
import RemoveRedEyeOutlined from '@mui/icons-material/RemoveRedEyeOutlined'
import VisibilityOffOutlined from '@mui/icons-material/VisibilityOffOutlined'

import Bg from '@/assets/images/Bg_authentication.jpeg'
import { toast } from 'react-toastify'
import { useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'
import { toastErrorMessage } from '@/utils/errorMessage'
import { LoginStaffAPI } from '@/api/StaffAPI'
import { Typography } from '@mui/material'

interface IFormInput {
  email: string
  password: string
}

const Login = () => {
  const navigate = useNavigate()
  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm<IFormInput>()

  const handleLogin = async (data: IFormInput) => {
    try {
      const res = await LoginStaffAPI(data)
      if (res.status === 200) {
        toast.success('Đăng nhập thành công')
        localStorage.setItem('jwt', res.data.data.token)
        navigate('/dashboard')
      }
    } catch (err) {
      toastErrorMessage(err, 'Không thể đăng nhập')
    }
  }

  const [isHiddenPassword, setIsHiddenPassword] = useState(true)

  return (
    <Box className="flex flex-col gap-[20px]  md:gap-[26px]">
      <Box className="flex flex-col gap-[15px]  md:gap-[20px]">
        <Box className="text-[25px] font-bold">Login</Box>
        <Box className="input_component">
          <label className="label">Email</label>
          <input
            {...register('email', {
              required: true,
              pattern: /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/
            })}
            className="input"
          />
          {errors?.email?.type === 'required' && (
            <p className="error">This field is required</p>
          )}
          {errors?.email?.type === 'pattern' && (
            <p className="error">Invalid email</p>
          )}
        </Box>
        <Box className="input_component">
          <label className="label">Password</label>
          <Box className="input_eye">
            <input
              {...register('password', {
                required: true,
                pattern: /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/
              })}
              type={isHiddenPassword ? 'password' : 'text'}
              className="input"
            />
            {isHiddenPassword ? (
              <RemoveRedEyeOutlined
                className="eye_icon"
                onClick={() => setIsHiddenPassword(false)}
              />
            ) : (
              <VisibilityOffOutlined
                className="eye_icon"
                onClick={() => setIsHiddenPassword(true)}
              />
            )}
          </Box>

          {errors?.password?.type === 'required' && (
            <p className="error">This field is required</p>
          )}

          {errors?.password?.type === 'pattern' && (
            <p className="error">
              Password must be at least eight characters, at least one letter
              and one number
            </p>
          )}
        </Box>
        <Box className="">
          <Button
            variant="contained"
            className="!bg-primary !normal-case w-[100%] !py-[10px]"
            onClick={handleSubmit(handleLogin)}
          >
            Login
          </Button>
        </Box>
      </Box>
      <hr />
      <Box className="flex flex-col gap-[20px] md:gap-[30px]">
        <Box className="flex flex-col gap-[20px]">
          <Button
            variant="contained"
            startIcon={<FacebookOutlined />}
            className="w-[100%] !normal-case !bg-[#F3F4F6] !text-[#000] !py-[10px]"
          >
            Login With Facebook
          </Button>
          <Button
            variant="contained"
            startIcon={<Google />}
            className="w-[100%] !normal-case !bg-[#F3F4F6] !text-[#000] !py-[10px]"
          >
            Login With Google
          </Button>
        </Box>
        <Box className="flex flex-col gap-[10px]">
          <Typography
            variant="body2"
            className="hover:cursor-pointer underline"
            onClick={() => {
              navigate('/forgot')
            }}
          >
            Forgot your password
          </Typography>
        </Box>
      </Box>
    </Box>
  )
}
export default function LoginPage() {
  return (
    <Box>
      <AuthenticationComponent Bg={Bg}>
        <Login />
      </AuthenticationComponent>
    </Box>
  )
}
