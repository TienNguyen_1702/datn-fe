import React, { useState } from 'react'
import { BarChart } from '@mui/x-charts/BarChart'
import dayjs, { Dayjs } from 'dayjs'
import { axisClasses } from '@mui/x-charts/ChartsAxis'
import {
  Box,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
  Stack,
  Typography
} from '@mui/material'
import { DatePicker } from '@mui/x-date-pickers/DatePicker'
import { GetRevenueDashboardAPI } from '@/api/DashboardAPI'
import { useTranslation } from 'react-i18next'

export default function BarCharts() {
  const [mode, setMode] = useState<'THIS_MONTH' | 'LAST_MONTH' | 'ADVANCED'>(
    'THIS_MONTH'
  )

  const { t } = useTranslation('translation', {
    keyPrefix: 'page.Dashboard'
  })

  const handleModeChange = (e: SelectChangeEvent) => {
    const value = e.target.value as string
    if (
      value === 'THIS_MONTH' ||
      value === 'LAST_MONTH' ||
      value === 'ADVANCED'
    ) {
      setMode(value)
    }
  }

  //startDate
  const [value1, setValue1] = useState<Dayjs>(dayjs())

  //endDate
  const [value2, setValue2] = useState<Dayjs>(dayjs())

  const handleChangeValue1 = (newValue) => {
    if (value2 && newValue && newValue.isBefore(value2)) {
      setValue1(newValue)
    }
  }

  const handleChangeValue2 = (newValue) => {
    if (value1 && newValue && newValue.isAfter(value1)) {
      setValue2(newValue)
    }
  }

  const { data = [] } = GetRevenueDashboardAPI({
    mode: mode,
    startDateInput: mode === 'ADVANCED' ? value1.unix() : undefined,
    endDateInput: mode === 'ADVANCED' ? value2.unix() : undefined
  })

  //Tạo trục x khi mode là THIS_MONTH hoặc LAST_MONTH
  const generateDates = (month: number, year: number): string[] => {
    const startDate = dayjs(`${year}-${month}-01`)
    const endDate = startDate.endOf('month')
    const dates: string[] = []
    let currentDate = startDate
    while (
      currentDate.isBefore(endDate) ||
      currentDate.isSame(endDate, 'day')
    ) {
      dates.push(currentDate.format('D/M') as never)
      currentDate = currentDate.add(1, 'day')
    }
    return dates
  }

  //Tạo trục x khi mode là ADVANCED
  const generateDates2 = (startDate: Dayjs, endDate: Dayjs): string[] => {
    if (startDate.unix() < endDate.unix()) {
      const dates: string[] = []
      let currentDate = dayjs(startDate)
      while (
        currentDate.isBefore(endDate) ||
        currentDate.isSame(endDate, 'day')
      ) {
        dates.push(currentDate.format('D/M') as never)
        currentDate = currentDate.add(1, 'day')
      }
      return dates
    }
    return [t('The selected time period is invalid')]
  }

  const xAxisData =
    mode === 'ADVANCED'
      ? generateDates2(value1, value2)
      : mode === 'THIS_MONTH'
      ? generateDates(dayjs().month() + 1, dayjs().year())
      : generateDates(dayjs().month(), dayjs().year())

  // Initialize dataX with zeros
  const dataX = Array(xAxisData.length).fill(0)

  // Update dataX with values from data
  data.forEach((item) => {
    const formattedDate = dayjs(item.date).format('D/M')
    const index = xAxisData.indexOf(formattedDate)
    if (index !== -1) {
      dataX[index] = item.totalAmount
    }
  })

  return (
    <Stack pt={5}>
      <Box display="flex" justifyContent="space-between">
        <FormControl>
          <InputLabel id="demo-simple-select-label">{t('Mode')}</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={mode}
            label={t('Mode')}
            onChange={handleModeChange}
          >
            <MenuItem value="THIS_MONTH">
              <Typography>{t('This month')}</Typography>
            </MenuItem>
            <MenuItem value="LAST_MONTH">
              <Typography>{t('Last month')}</Typography>
            </MenuItem>
            <MenuItem value="ADVANCED">
              <Typography>{t('Customize the time period')}</Typography>
            </MenuItem>
          </Select>
        </FormControl>
        {mode === 'ADVANCED' && (
          <Stack direction="row" spacing={2}>
            <DatePicker
              sx={{ flex: 1 }}
              label={t('Start date')}
              value={value1}
              format="DD/MM/YYYY"
              onChange={handleChangeValue1}
            />
            <DatePicker
              sx={{ flex: 1 }}
              label={t('End date')}
              value={value2}
              format="DD/MM/YYYY"
              onChange={handleChangeValue2}
            />
          </Stack>
        )}
      </Box>
      <BarChart
        margin={{ left: 100 }}
        xAxis={[
          {
            id: 'barCategories',
            data: xAxisData,
            scaleType: 'band',
            tickPlacement: 'middle',
            label:
              mode === 'ADVANCED'
                ? `${t('Revenue from date')} ${value1.format('DD/MM/YYYY')} ${t(
                    'to'
                  )} ${value2.format('DD/MM/YYYY')}`
                : mode === 'THIS_MONTH'
                ? `${t('Revenue of the month')} ${
                    dayjs().month() + 1
                  } / ${dayjs().year()}`
                : `${t('Revenue of the month')} ${
                    dayjs().subtract(1, 'month').month() + 1
                  } / ${dayjs().subtract(1, 'month').year()}`
          }
        ]}
        series={[
          {
            data: dataX
          }
        ]}
        yAxis={[
          {
            label: t('total (VND)')
          }
        ]}
        height={300}
        sx={{
          [`& .${axisClasses.directionX} .${axisClasses.label}`]: {
            transform: 'translateY(8px)'
          },
          [`& .${axisClasses.directionY} .${axisClasses.label}`]: {
            transform: 'translateX(-50px)'
          }
        }}
      />
    </Stack>
  )
}
