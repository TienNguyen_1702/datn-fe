import TableMUI from '@/components/TableMUI'
import { STYLE_HEADER_TABLE } from '@/assets/styles/style'
import {
  Box,
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
  Stack,
  Typography
} from '@mui/material'
import { useNavigate } from 'react-router-dom'
import { useState } from 'react'
import { GetBestSellingDashboardAPI } from '@/api/DashboardAPI'
import { useTranslation } from 'react-i18next'

export const BestSellingTable = () => {
  const navigate = useNavigate()

  const { t } = useTranslation('translation', {
    keyPrefix: 'page.Dashboard'
  })

  const [mode, setMode] = useState<
    'THIS_MONTH' | 'LAST_MONTH' | 'THIS_WEEK' | 'LAST_WEEK'
  >('THIS_MONTH')

  const handleModeChange = (e: SelectChangeEvent) => {
    const value = e.target.value as string
    if (
      value === 'THIS_MONTH' ||
      value === 'LAST_MONTH' ||
      value === 'THIS_WEEK' ||
      value === 'LAST_WEEK'
    ) {
      setMode(value)
    }
  }
  const columns = [
    {
      field: 'id',
      maxWidth: 30,
      sortable: false,
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>ID</Box>
    },
    {
      field: 'name',
      minWidth: 200,
      sortable: false,
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>{t('Product Name')}</Box>
    },
    {
      field: 'totalQuantity',
      sortable: false,
      renderHeader: () => (
        <Box sx={STYLE_HEADER_TABLE}>{t('Total Quantity')}</Box>
      )
    },
    {
      field: 'actions',
      minWidth: 120,
      sortable: false,
      renderCell: (params) => (
        <Box sx={{ display: 'flex', gap: 1 }}>
          <Button
            size="small"
            variant="outlined"
            onClick={() => navigate(`/catalogs/products/${params.row.id}`)}
          >
            {t('See details')}
          </Button>
        </Box>
      ),
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}></Box>
    }
  ]

  const { data = [] } = GetBestSellingDashboardAPI({ mode })

  return (
    <Stack spacing={4}>
      <Box display="flex" justifyContent="flex-start">
        <FormControl sx={{ minWidth: 120 }}>
          <InputLabel id="demo-simple-select-label">{t('Mode')}</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={mode}
            label={t('Mode')}
            onChange={handleModeChange}
          >
            <MenuItem value="THIS_MONTH">
              <Typography>{t('This month')}</Typography>
            </MenuItem>
            <MenuItem value="LAST_MONTH">
              <Typography>{t('Last month')}</Typography>
            </MenuItem>
            <MenuItem value="THIS_WEEK">
              <Typography>{t('This week')}</Typography>
            </MenuItem>
            <MenuItem value="LAST_WEEK">
              <Typography>{t('Last week')}</Typography>
            </MenuItem>
          </Select>
        </FormControl>
      </Box>
      <TableMUI columns={columns} rows={data} />
    </Stack>
  )
}
