import { GetOverViewDashboardAPIType } from '@/api/DashboardAPI'
import { useTheme } from '@mui/material'
import ReactEcharts from 'echarts-for-react'
import { useTranslation } from 'react-i18next'

const DoughnutChart = ({
  height,
  result,
  color = []
}: {
  height: string
  color: string[]
  result?: GetOverViewDashboardAPIType
}) => {
  const theme = useTheme()
  const { t } = useTranslation('translation', { keyPrefix: 'page.Dashboard' })

  const option = {
    legend: {
      show: true,
      itemGap: 15,
      icon: 'circle',
      bottom: -5,
      textStyle: {
        fontSize: 13,
        color: theme.palette.text.secondary,
        fontFamily: theme.typography.fontFamily
      }
    },
    tooltip: {
      show: false,
      trigger: 'item',
      formatter: '{a} <br/>{b}: {c} ({d}%)'
    },
    xAxis: [{ axisLine: { show: false }, splitLine: { show: false } }],
    yAxis: [{ axisLine: { show: false }, splitLine: { show: false } }],

    label: {
      fontSize: 13,
      color: theme.palette.text.secondary,
      fontFamily: theme.typography.fontFamily
    },

    series: [
      {
        name: 'Traffic Rate',
        type: 'pie',
        radius: ['55%', '70%'],
        center: ['50%', '50%'],
        avoidLabelOverlap: false,
        hoverOffset: 5,
        stillShowZeroSum: false,
        label: {
          show: false,
          position: 'center'
        },

        data: [
          { value: Number(result?.confirm?.count), name: t('Confirm') },
          { value: Number(result?.waiting?.count), name: t('Waiting') },
          { value: Number(result?.shipping?.count), name: t('Shipping') },
          { value: Number(result?.done?.count), name: t('Done') },
          { value: Number(result?.cancel?.count), name: t('Cancel') }
        ],

        emphasis: {
          itemStyle: {
            shadowBlur: 10,
            shadowOffsetX: 0,
            shadowColor: 'rgba(0, 0, 0, 0.5)'
          },
          label: {
            show: true,
            formatter: '{b} \n{c} ({d}%)',
            fontFamily: theme.typography.fontFamily,
            color: theme.palette.text.primary,
            fontSize: 13
          }
        }
      }
    ]
  }

  return (
    <ReactEcharts
      style={{ height: height }}
      option={{ ...option, color: [...color] }}
    />
  )
}

export default DoughnutChart
