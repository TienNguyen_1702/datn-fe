import { useTheme } from '@mui/material'
import ReactEcharts from 'echarts-for-react'

const LineChart = ({
  height,
  color = []
}: {
  height: string
  color: string[]
}) => {
  const theme = useTheme()

  const option = {
    grid: { top: '10%', bottom: '10%', left: '5%', right: '5%' },
    legend: {
      itemGap: 20,
      icon: 'circle',
      textStyle: {
        fontSize: 13,
        color: theme.palette.text.secondary,
        fontFamily: theme.typography.fontFamily
      }
    },
    label: {
      fontSize: 13,
      color: theme.palette.text.secondary,
      fontFamily: theme.typography.fontFamily
    },
    xAxis: {
      type: 'category',
      data: [
        'Jan',
        'Feb',
        'Mar',
        'Apr',
        'May',
        'Jun',
        'Jul',
        'Aug',
        'Sep',
        'Oct',
        'Nov',
        'Dec'
      ],
      axisLine: { show: false },
      axisTick: { show: false },
      axisLabel: {
        fontSize: 14,
        fontFamily: 'roboto',
        color: theme.palette.text.secondary
      }
    },
    yAxis: {
      type: 'value',
      axisLine: { show: false },
      axisTick: { show: false },
      splitLine: {
        lineStyle: { color: theme.palette.text.secondary, opacity: 0.15 }
      },
      axisLabel: {
        color: theme.palette.text.secondary,
        fontSize: 13,
        fontFamily: 'roboto'
      }
    },
    series: [
      {
        data: [30, 34, 35, 51, 49, 62, 69, 91, 148, 35, 51, 49],
        type: 'line',
        stack: 'Total income',
        name: 'Total income',
        smooth: true,
        symbolSize: 4,
        lineStyle: { width: 3 }
      },
      {
        data: [20, 41, 13, 56, 77, 88, 99, 77, 45, 13, 56, 77],
        type: 'line',
        stack: 'Total expense',
        name: 'Total expense',
        smooth: true,
        symbolSize: 4,
        lineStyle: { width: 3 }
      }
    ]
  }

  return (
    <ReactEcharts
      style={{ height: height }}
      option={{ ...option, color: [...color] }}
    />
  )
}

export default LineChart
