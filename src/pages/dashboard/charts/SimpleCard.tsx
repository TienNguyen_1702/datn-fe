import Card from '@mui/material/Card'
import Box from '@mui/material/Box'
import { styled } from '@mui/material'

const CardRoot = styled(Card)({
  height: '100%',
  padding: '20px 24px'
})

const SimpleCard = ({ children, title }) => {
  return (
    <CardRoot
      elevation={6}
      className="shadow-md"
      sx={{ background: (theme) => theme.palette._white_212b36.main }}
    >
      <Box>{title}</Box>
      {children}
    </CardRoot>
  )
}

export default SimpleCard
