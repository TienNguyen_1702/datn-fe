import {
  FriendIcon,
  GalleryIcon,
  HeartIcon,
  LocationIcon,
  MailIcon,
  ProfileIcon,
  StudyIcon
} from '@/assets/icons/AboutUs'
import TabContext from '@mui/lab/TabContext'
import TabList from '@mui/lab/TabList'
import TabPanel from '@mui/lab/TabPanel'
import { Avatar, Box, Button, Tab } from '@mui/material'
import { useEffect, useState } from 'react'

export default function AboutUsPage() {
  const [value, setValue] = useState(() => {
    // Lấy giá trị từ localStorage, nếu không có thì sử dụng giá trị mặc định là '1'
    return localStorage.getItem('currentTabAboutUs') || '1'
  })

  const handleChange = (event: React.SyntheticEvent, newValue: string) => {
    setValue(newValue)
  }
  useEffect(() => {
    // Lưu giá trị của Tab vào localStorage khi giá trị thay đổi
    localStorage.setItem('currentTabAboutUs', value)
  }, [value])

  const [post, setPost] = useState('')

  return (
    <Box>
      <Box>
        <Box
          sx={{
            height: 300,
            width: 'auto',
            display: 'flex',
            alignItems: 'center',
            background: 'green',
            overflow: 'hidden'
          }}
        >
          <img
            src="https://scontent.fsgn2-9.fna.fbcdn.net/v/t1.6435-9/68661081_665375347306139_572668532404256768_n.jpg?_nc_cat=103&ccb=1-7&_nc_sid=5f2048&_nc_ohc=0mO5aZYkfMEAX-onxzb&_nc_ht=scontent.fsgn2-9.fna&oh=00_AfBrwG5ms_ZMtVXOL49jmiOSA_i2d2D5IlU8hCY1KrrHLA&oe=6627BB56"
            alt="Hình ảnh"
            className="w-[100%]"
          />
        </Box>
        <Box sx={{ position: 'relative' }}>
          <Avatar
            alt="Remy Sharp"
            src="https://scontent.fsgn2-4.fna.fbcdn.net/v/t39.30808-6/378119778_1691380301372300_8820761633006420688_n.jpg?_nc_cat=101&ccb=1-7&_nc_sid=5f2048&_nc_ohc=5exRQy3QKUYAX8J6vCh&_nc_ht=scontent.fsgn2-4.fna&oh=00_AfBPrE0GhE3soCzNelNHSM1YbVF953DA5hvzGJNlCqElzw&oe=66060C8F"
            sx={{
              width: 200,
              height: 200,
              position: 'absolute',
              left: 50,
              top: -100,
              border: '6px solid #fff'
            }}
          />
          <TabContext value={value}>
            <Box
              sx={{
                borderBottom: 1,
                borderColor: 'divider',
                display: 'flex',
                justifyContent: 'flex-end ',
                background: '#fff',
                paddingTop: 8
              }}
            >
              <TabList
                onChange={handleChange}
                aria-label="lab API tabs example"
              >
                <Tab
                  label={
                    <Box className="flex items-center gap-[5px]">
                      <ProfileIcon width={22} />
                      Profile
                    </Box>
                  }
                  value="1"
                />

                <Tab
                  label={
                    <Box className="flex items-center gap-[5px]">
                      <HeartIcon width={22} />
                      Followers
                    </Box>
                  }
                  value="2"
                />
                <Tab
                  label={
                    <Box className="flex items-center gap-[5px]">
                      <FriendIcon width={20} />
                      Friends
                    </Box>
                  }
                  value="3"
                />
                <Tab
                  label={
                    <Box className="flex items-center gap-[5px]">
                      <GalleryIcon width={20} />
                      Gallery
                    </Box>
                  }
                  value="4"
                />
              </TabList>
            </Box>
            <TabPanel value="1" sx={{ paddingX: 0 }}>
              <Box className="flex gap-[20px]">
                <Box className="flex-[1]">
                  <Box
                    sx={{
                      boxShadow:
                        'rgba(145, 158, 171, 0.2) 0px 0px 2px 0px, rgba(145, 158, 171, 0.12) 0px 12px 24px -4px'
                    }}
                    className=" bg-[#fff] p-[20px] flex flex-col gap-[15px]"
                  >
                    <Box className="font-bold">About</Box>
                    <Box className="text-[13px]">
                      Tart I love sugar plum I love oat cake. Sweet roll
                      caramels I love jujubes. Topping cake wafer..
                    </Box>
                    <Box className="flex items-center text-[13px] gap-[10px]">
                      <LocationIcon width={20} />
                      <Box>
                        Live at{' '}
                        <span className="font-bold">Cam Xuyen, Ha Tinh</span>
                      </Box>
                    </Box>
                    <Box className="flex items-center text-[13px] gap-[10px]">
                      <MailIcon width={20} />
                      <Box>tien.nguyenctcxht11@hcmut.edu.vn</Box>
                    </Box>
                    <Box className="flex text-[13px] gap-[10px]">
                      <StudyIcon width={20} />
                      <Box className="flex-[1]">
                        Computer Science major at{' '}
                        <span className="font-bold">
                          Ho Chi Minh University of Technology
                        </span>
                      </Box>
                    </Box>
                  </Box>
                </Box>
                <Box className="flex-[2] flex flex-col gap-[20px]">
                  <Box
                    sx={{
                      boxShadow:
                        'rgba(145, 158, 171, 0.2) 0px 0px 2px 0px, rgba(145, 158, 171, 0.12) 0px 12px 24px -4px'
                    }}
                    className=" bg-[#fff] p-[20px] flex flex-col gap-[20px]"
                  >
                    <input
                      value={post}
                      onChange={(e) => setPost(e.target.value)}
                      style={{ border: '1px solid rgba(145, 158, 171, 0.2)' }}
                      placeholder="Share what you are thinking here..."
                      className=" pb-[80px] pt-[20px] pl-[10px] rounded-[8px] focus:outline-none placeholder-gray-400"
                    />
                    <Box className="flex justify-between">
                      <Box className=" flex gap-[20px]">
                        <Button
                          startIcon={
                            <img
                              height="24"
                              width="24"
                              alt=""
                              className="xz74otr"
                              src="https://static.xx.fbcdn.net/rsrc.php/v3/yC/r/a6OjkIIE-R0.png"
                            />
                          }
                          sx={{
                            background: 'rgba(145, 158, 171, 0.08)',
                            color: '#6B7B88'
                          }}
                        >
                          Image/Video
                        </Button>
                        <Button
                          startIcon={
                            <img
                              height="24"
                              width="24"
                              alt=""
                              className="xz74otr"
                              src="https://static.xx.fbcdn.net/rsrc.php/v3/yF/r/v1iF2605Cb5.png"
                            />
                          }
                          sx={{
                            background: 'rgba(145, 158, 171, 0.08)',
                            color: '#6B7B88'
                          }}
                        >
                          Streaming
                        </Button>
                      </Box>
                      <Button variant="contained" onClick={() => alert(post)}>
                        Post
                      </Button>
                    </Box>
                  </Box>
                  <Box
                    sx={{
                      boxShadow:
                        'rgba(145, 158, 171, 0.2) 0px 0px 2px 0px, rgba(145, 158, 171, 0.12) 0px 12px 24px -4px'
                    }}
                    className=" bg-[#fff] p-[20px] flex flex-col gap-[20px]"
                  ></Box>
                </Box>
              </Box>
            </TabPanel>
            <TabPanel value="2"></TabPanel>
            <TabPanel value="3">
              <Box
                sx={{
                  boxShadow:
                    'rgba(145, 158, 171, 0.2) 0px 0px 2px 0px, rgba(145, 158, 171, 0.12) 0px 12px 24px -4px'
                }}
                className="flex-[1] bg-[#fff] p-[20px] flex flex-col gap-[20px]"
              ></Box>
            </TabPanel>
          </TabContext>
        </Box>
      </Box>
    </Box>
  )
}
