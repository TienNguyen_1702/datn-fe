import React, { useState } from 'react'
import { Box, Button, FormControlLabel, Checkbox } from '@mui/material'
import AuthenticationComponent from '@/components/Authentication/Authentication'
import Bg from '@/assets/images/Bg_register.jpeg'
import FacebookOutlined from '@mui/icons-material/FacebookOutlined'
import Google from '@mui/icons-material/Google'
import RemoveRedEyeOutlined from '@mui/icons-material/RemoveRedEyeOutlined'
import VisibilityOffOutlined from '@mui/icons-material/VisibilityOffOutlined'
import { useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'
import { toastErrorMessage } from '@/utils/errorMessage'
import { toast } from 'react-toastify'
import { CreateStaffAPI } from '@/api/StaffAPI'

const Register = () => {
  const navigate = useNavigate()
  const {
    register,
    watch,
    handleSubmit,
    formState: { errors }
  } = useForm()

  const [isHiddenPassword, setIsHiddenPassword] = useState({
    first: true,
    second: true
  })

  const handleRegister = async (data) => {
    delete data.confirm
    try {
      const res = await CreateStaffAPI(data)
      if (res.status === 200) {
        toast.success('Tạo tài khoản staff thành công')
        navigate('/login')
      }
    } catch (err) {
      toastErrorMessage(err, 'Không thể tạo tài khoản staff')
    }
  }
  return (
    <Box className="flex flex-col gap-[20px]  md:gap-[26px]">
      <Box className="flex flex-col gap-[15px]  md:gap-[20px]">
        <Box className="text-[25px] font-bold">Register</Box>
        <Box className="input_component">
          <label className="label">Name</label>
          <input
            {...register('name', {
              required: true
            })}
            className="input"
          />
          {errors?.name?.type === 'required' && (
            <p className="error">This field is required</p>
          )}
        </Box>
        <Box className="input_component">
          <label className="label">Email</label>
          <input
            {...register('email', {
              required: true,
              pattern: /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/
            })}
            className="input"
          />
          {errors?.email?.type === 'required' && (
            <p className="error">This field is required</p>
          )}
          {errors?.email?.type === 'pattern' && (
            <p className="error">Invalid email</p>
          )}
        </Box>
        <Box className="input_component">
          <label className="label">Phone number</label>
          <input
            {...register('phone_number', {
              required: true,
              pattern: /(03|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/
            })}
            className="input"
          />
          {errors?.phone_number?.type === 'required' && (
            <p className="error">This field is required</p>
          )}
          {errors?.phone_number?.type === 'pattern' && (
            <p className="error">Invalid phone number</p>
          )}
        </Box>
        <Box className="input_component">
          <label className="label">Password</label>
          <Box className="input_eye">
            <input
              {...register('password', {
                required: true,
                pattern: /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/
              })}
              type={isHiddenPassword.first ? 'password' : 'text'}
              className="input"
            />
            {isHiddenPassword.first ? (
              <RemoveRedEyeOutlined
                className="eye_icon"
                onClick={() =>
                  setIsHiddenPassword((prevState) => ({
                    ...prevState,
                    first: false
                  }))
                }
              />
            ) : (
              <VisibilityOffOutlined
                className="eye_icon"
                onClick={() =>
                  setIsHiddenPassword((prevState) => ({
                    ...prevState,
                    first: true
                  }))
                }
              />
            )}
          </Box>

          {errors?.password?.type === 'required' && (
            <p className="error">This field is required</p>
          )}

          {errors?.password?.type === 'pattern' && (
            <p className="error">
              Password must be at least eight characters, at least one letter
              and one number
            </p>
          )}
        </Box>
        <Box className="input_component">
          <label className="label">Confirm Password</label>
          <Box className="input_eye">
            <input
              {...register('confirm', {
                required: true,
                validate: (val: string) => {
                  if (watch('password') != val) {
                    return 'Your passwords do no match'
                  }
                }
              })}
              type={isHiddenPassword.second ? 'password' : 'text'}
              className="input"
            />
            {isHiddenPassword.second ? (
              <RemoveRedEyeOutlined
                className="eye_icon"
                onClick={() =>
                  setIsHiddenPassword((prevState) => ({
                    ...prevState,
                    second: false
                  }))
                }
              />
            ) : (
              <VisibilityOffOutlined
                className="eye_icon"
                onClick={() =>
                  setIsHiddenPassword((prevState) => ({
                    ...prevState,
                    second: true
                  }))
                }
              />
            )}
          </Box>

          {errors?.confirm?.type === 'required' && (
            <p className="error">This field is required</p>
          )}

          {errors?.confirm?.type === 'validate' && (
            <p className="error">Your passwords do no match</p>
          )}
        </Box>
        <Box className="input_component">
          <label className="label">Staff Role</label>
          <select {...register('role', { required: true })} className="select">
            <option value="" hidden>
              --- Select Role ---
            </option>
            <option value="admin">Admin</option>
            {/* <option value="CEO">CEO</option>
            <option value="Manage">Manage</option> */}
          </select>
          {errors?.role?.type === 'required' && (
            <p className="error">This field is required</p>
          )}
        </Box>

        <Box>
          <FormControlLabel control={<Checkbox size="small" />} label="" />
          <span className="text-normal">
            I agree to the
            <a
              target="_blank"
              href="#"
              className="underline pl-[5px] text-primary"
            >
              Privacy policy
            </a>
          </span>
        </Box>
        <Box>
          <Button
            variant="contained"
            className="!bg-primary !normal-case w-[100%] !py-[10px]"
            onClick={handleSubmit(handleRegister)}
          >
            Create Account
          </Button>
        </Box>
      </Box>
      <hr />
      <Box className="flex flex-col gap-[20px] md:gap-[30px]">
        <Box className="flex flex-col gap-[20px]">
          <Button
            variant="contained"
            startIcon={<FacebookOutlined />}
            className="w-[100%] !normal-case !bg-[#F3F4F6] !text-[#000] !py-[10px]"
          >
            Login With Facebook
          </Button>
          <Button
            variant="contained"
            startIcon={<Google />}
            className="w-[100%] !normal-case !bg-[#F3F4F6] !text-[#000] !py-[10px]"
          >
            Login With Google
          </Button>
        </Box>
        <Box className="flex flex-col gap-[5px] mg:gap-[10px]">
          <Box
            className="text-normal text-primary hover:cursor-pointer"
            onClick={() => {
              navigate('/login')
            }}
          >
            Already have an account? Login
          </Box>
        </Box>
      </Box>
    </Box>
  )
}

export default function RegisterPage() {
  return (
    <Box>
      <AuthenticationComponent Bg={Bg}>
        <Register />
      </AuthenticationComponent>
    </Box>
  )
}
