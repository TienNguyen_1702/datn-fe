import MainLayout from '@/components/MainLayout'
import { Box } from '@mui/material'
import { useParams } from 'react-router-dom'
import { UserGeneralIcon } from '@/assets/icons/UserGeneral'
import TabContext from '@mui/lab/TabContext'
import TabList from '@mui/lab/TabList'
import TabPanel from '@mui/lab/TabPanel'
import { Tab, TextField } from '@mui/material'
import { useEffect, useState } from 'react'
import NoAvatar from '@/assets/images/NoAvatar.jpg'
import { formatDateTime } from '@/utils/format'
import { GetStaffDetailAPI } from '@/api/StaffAPI'
import { useTranslation } from 'react-i18next'

const StaffDetail = () => {
  const { id } = useParams()

  const { t } = useTranslation('translation', { keyPrefix: 'page.Staff' })

  const [value, setValue] = useState(() => {
    // Lấy giá trị từ localStorage, nếu không có thì sử dụng giá trị mặc định là '1'
    return localStorage.getItem('currentTabDetailStaff') || '1'
  })

  const handleChange = (event: React.SyntheticEvent, newValue: string) => {
    setValue(newValue)
  }

  useEffect(() => {
    // Lưu giá trị của Tab vào localStorage khi giá trị thay đổi
    localStorage.setItem('currentTabDetailStaff', value)
  }, [value])

  const { data: staffDetail } = GetStaffDetailAPI(id)

  return (
    <Box>
      <TabContext value={value}>
        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
          <TabList onChange={handleChange} aria-label="lab API tabs example">
            <Tab
              label={
                <Box className="flex items-center gap-[5px]">
                  <UserGeneralIcon width={22} />
                  {t('Personal information')}
                </Box>
              }
              value="1"
            />
          </TabList>
        </Box>
        <TabPanel value="1">
          <Box className="flex gap-[20px]">
            <Box
              sx={{
                background: (theme) => theme.palette._white_212b36.main,
                boxShadow:
                  'rgba(145, 158, 171, 0.2) 0px 0px 2px 0px, rgba(145, 158, 171, 0.12) 0px 12px 24px -4px'
              }}
              className="flex-[1]"
            >
              <Box className="flex-[1] flex flex-col gap-[20px] items-center px-[20px] p-[80px] rounded-[15px] shadow-md">
                <label htmlFor="upload-photo">
                  <Box className="flex items-center justify-center border-[1px] bg-white border-gray-200 border-solid rounded-full w-[144px] h-[144px] hover:cursor-pointer">
                    <Box className="flex items-center justify-center border-[1px] border-gray-200 border-solid bg-gray-200 rounded-full w-[130px] h-[130px] hover:bg-gray-100">
                      <Box className="flex flex-col gap-[8px] justify-center items-center">
                        <img
                          src={
                            staffDetail?.image ? staffDetail?.image : NoAvatar
                          }
                          width={130}
                          height={130}
                          className="rounded-full max-w-[130px] max-h-[130px]"
                        />
                      </Box>
                    </Box>
                  </Box>
                </label>
              </Box>
            </Box>
            <Box
              sx={{
                background: (theme) => theme.palette._white_212b36.main,
                boxShadow:
                  'rgba(145, 158, 171, 0.2) 0px 0px 2px 0px, rgba(145, 158, 171, 0.12) 0px 12px 24px -4px'
              }}
              className="flex-[2] p-[20px] flex flex-col gap-[20px]"
            >
              <Box>
                {' '}
                <Box className="flex items-center justify-center text-[16px] font-bold">
                  {t("Staff's personal information")}
                </Box>
              </Box>
              <Box className="flex gap-[15px]">
                <TextField
                  label={t('Staff Name')}
                  value={staffDetail?.name || ''}
                  InputLabelProps={{ shrink: true }}
                  className="flex-[1]"
                />
                <TextField
                  label={t('Email')}
                  value={staffDetail?.email}
                  InputLabelProps={{ shrink: true }}
                  className="flex-[1]"
                />
              </Box>
              <Box className="flex gap-[15px]">
                <TextField
                  label={t('Phone number')}
                  value={staffDetail?.phone_number}
                  InputLabelProps={{ shrink: true }}
                  className="flex-[1]"
                />
                <TextField
                  label={t('Last login at')}
                  value={formatDateTime(staffDetail?.last_login_at)}
                  InputLabelProps={{ shrink: true }}
                  className="flex-[1]"
                />
              </Box>
              <Box className="flex gap-[15px]">
                <TextField
                  label={t('Password change at')}
                  value={formatDateTime(staffDetail?.password_change_at)}
                  InputLabelProps={{ shrink: true }}
                  className="flex-[1]"
                />
                <TextField
                  label={t('Registry at')}
                  value={formatDateTime(staffDetail?.registry_at)}
                  InputLabelProps={{ shrink: true }}
                  className="flex-[1]"
                />
              </Box>
            </Box>
          </Box>
        </TabPanel>
      </TabContext>
    </Box>
  )
}
export default function StaffDetailPage() {
  return <MainLayout content={<StaffDetail />} back={true} />
}
