import React, { useState } from 'react'
import {
  Box,
  Typography,
  Stack,
  TextField,
  IconButton,
  InputAdornment
} from '@mui/material'
import { Controller, useForm, useWatch } from 'react-hook-form'
import { toastErrorMessage } from '@/utils/errorMessage'
import { toast } from 'react-toastify'
import { CreateStaffAPI } from '@/api/StaffAPI'
import UploadImage from '@/components/Global/UploadImage'
import { useMutation } from '@tanstack/react-query'
import LoadingButton from '@mui/lab/LoadingButton'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
import { Visibility, VisibilityOff } from '@mui/icons-material'

export default function CreateStaffPage() {
  const {
    control,
    handleSubmit,
    formState: { errors }
  } = useForm()
  const password = useWatch({ control, name: 'password' })

  const navigate = useNavigate()

  const { t: v } = useTranslation('translation', { keyPrefix: 'validate' })
  const { t } = useTranslation('translation', { keyPrefix: 'page.Staff' })

  const [selectedImage, setSelectedImage] = useState<File | null>()
  const [isHiddenPassword, setIsHiddenPassword] = useState({
    first: true,
    second: true
  })

  const { mutate, isPending } = useMutation({
    mutationFn: CreateStaffAPI,
    onSuccess: () => {
      toast.success('Thêm nhân viên thành công')
      navigate('/staff/list')
    },
    onError: (err) => {
      toastErrorMessage(err)
    }
  })

  const handleCreateStaff = async (data) => {
    delete data.confirm
    data.role = 'admin'
    mutate(data)
  }

  return (
    <Stack gap={5}>
      <Typography variant="_title">Create Staff</Typography>
      <Stack gap={3}>
        <Stack
          direction={{ sx: 'column', lg: 'row' }}
          gap={3}
          p={{ xs: 2, lg: 5 }}
        >
          <UploadImage
            selectedImage={selectedImage}
            setSelectedImage={setSelectedImage}
          />
          <Stack
            spacing={4}
            p={5}
            flex={1}
            className="rounded-[15px] shadow-md relative"
            sx={{ background: (theme) => theme.palette._white_212b36.main }}
          >
            <Controller
              defaultValue={''}
              name="name"
              control={control}
              rules={{ required: v('This field is required') }}
              render={({ field }) => (
                <TextField
                  label={t('Staff Name')}
                  error={!!errors.name}
                  helperText={errors.name?.message?.toString()}
                  {...field}
                />
              )}
            />

            <Controller
              defaultValue={''}
              name="email"
              control={control}
              rules={{
                required: v('This field is required'),
                pattern: {
                  value: /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/,
                  message: v('Invalid email')
                }
              }}
              render={({ field }) => (
                <TextField
                  label={t('Email')}
                  error={!!errors.email}
                  helperText={errors.email?.message?.toString()}
                  {...field}
                />
              )}
            />

            <Controller
              defaultValue={''}
              name="phone_number"
              control={control}
              rules={{
                required: v('This field is required'),
                pattern: {
                  value: /(03|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/,
                  message: v('Invalid phone number')
                }
              }}
              render={({ field }) => (
                <TextField
                  label={t('Phone number')}
                  error={!!errors.phone_number}
                  helperText={errors.phone_number?.message?.toString()}
                  {...field}
                />
              )}
            />
            <Controller
              defaultValue={''}
              name="password"
              control={control}
              rules={{
                required: v('This field is required'),
                pattern: {
                  value: /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/,
                  message: v(
                    'Password must be at least eight characters, at least one letter and one number'
                  )
                }
              }}
              render={({ field }) => (
                <TextField
                  label={t('Password')}
                  error={!!errors.password}
                  helperText={errors.password?.message?.toString()}
                  type={isHiddenPassword.first ? 'password' : 'text'}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          onClick={() =>
                            setIsHiddenPassword((prevState) => ({
                              ...prevState,
                              first: !isHiddenPassword.first
                            }))
                          }
                        >
                          {isHiddenPassword.first ? (
                            <VisibilityOff fontSize="small" />
                          ) : (
                            <Visibility fontSize="small" />
                          )}
                        </IconButton>
                      </InputAdornment>
                    )
                  }}
                  {...field}
                />
              )}
            />
            <Controller
              defaultValue={''}
              name="confirm"
              control={control}
              rules={{
                required: v('This field is required'),
                validate: (value) =>
                  value === password || v('Confirm password does not match')
              }}
              render={({ field }) => (
                <TextField
                  label={t('Confirm Password')}
                  error={!!errors.confirm}
                  helperText={errors.confirm?.message?.toString()}
                  type={isHiddenPassword.second ? 'password' : 'text'}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          onClick={() =>
                            setIsHiddenPassword((prevState) => ({
                              ...prevState,
                              second: !isHiddenPassword.second
                            }))
                          }
                        >
                          {isHiddenPassword.second ? (
                            <VisibilityOff fontSize="small" />
                          ) : (
                            <Visibility fontSize="small" />
                          )}
                        </IconButton>
                      </InputAdornment>
                    )
                  }}
                  {...field}
                />
              )}
            />

            <Box className="flex justify-end">
              <LoadingButton
                variant="contained"
                onClick={handleSubmit(handleCreateStaff)}
                loading={isPending}
              >
                {t('Create Staff')}
              </LoadingButton>
            </Box>
          </Stack>
        </Stack>
      </Stack>
    </Stack>
  )
}
