import { UserGeneralIcon } from '@/assets/icons/UserGeneral'
import { UserRecipientIcon } from '@/assets/icons/UserRecipient'
import MainLayout from '@/components/MainLayout'
import TabContext from '@mui/lab/TabContext'
import TabList from '@mui/lab/TabList'
import TabPanel from '@mui/lab/TabPanel'
import { Stack, Tab, TextField, Typography } from '@mui/material'
import Box from '@mui/material/Box'
import { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import NoAvatar from '@/assets/images/NoAvatar.jpg'
import { formatDateTime } from '@/utils/format'
import { SolarPenBold } from '@/assets/icons/SolarPenBold'
import { UserReview } from '@/pages/catalogs/products/[id]/TabProductDetail'
import { OrderLayout } from '@/pages/orders/page'
import { GetUserDetailAPI } from '@/api/UserAPI'
import { GetAllReviewAPI } from '@/api/ReviewAPI'
import { useTranslation } from 'react-i18next'

const Child = () => {
  const { t } = useTranslation('translation', { keyPrefix: 'page.Customer' })
  const { id } = useParams()

  const [value, setValue] = useState(() => {
    // Lấy giá trị từ localStorage, nếu không có thì sử dụng giá trị mặc định là '1'
    return localStorage.getItem('currentTabDetailCustomer') || '1'
  })

  const handleChange = (event: React.SyntheticEvent, newValue: string) => {
    setValue(newValue)
  }

  useEffect(() => {
    // Lưu giá trị của Tab vào localStorage khi giá trị thay đổi
    localStorage.setItem('currentTabDetailCustomer', value)
  }, [value])

  const { data: userDetail } = GetUserDetailAPI(id)
  const { data: reviewOfUser, refetch: loadReviewOfUser } = GetAllReviewAPI({
    user_id: Number(id)
  })

  return (
    <Box>
      <TabContext value={value}>
        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
          <TabList onChange={handleChange} aria-label="lab API tabs example">
            <Tab
              label={
                <Box className="flex items-center gap-[5px]">
                  <UserGeneralIcon width={22} />
                  {t('General')}
                </Box>
              }
              value="1"
            />

            <Tab
              label={
                <Box className="flex items-center gap-[5px]">
                  <UserRecipientIcon width={22} />
                  {t('Recipient')}
                </Box>
              }
              value="2"
            />
            <Tab
              label={
                <Box className="flex items-center gap-[5px]">
                  <SolarPenBold width={20} />
                  {t('Product Review')}
                </Box>
              }
              value="3"
            />
          </TabList>
        </Box>
        <TabPanel value="1">
          <Box className="flex gap-[20px]">
            <Box
              sx={{
                background: (theme) => theme.palette._white_212b36.main,
                boxShadow:
                  'rgba(145, 158, 171, 0.2) 0px 0px 2px 0px, rgba(145, 158, 171, 0.12) 0px 12px 24px -4px'
              }}
              className="flex-[1]"
            >
              <Box className="flex-[1] flex flex-col gap-[20px] items-center px-[20px] p-[80px] rounded-[15px] shadow-md">
                <label htmlFor="upload-photo">
                  <Box className="flex items-center justify-center border-[1px] bg-white border-gray-200 border-solid rounded-full w-[144px] h-[144px] hover:cursor-pointer">
                    <Box className="flex items-center justify-center border-[1px] border-gray-200 border-solid bg-gray-200 rounded-full w-[130px] h-[130px] hover:bg-gray-100">
                      <Box className="flex flex-col gap-[8px] justify-center items-center">
                        <img
                          src={userDetail?.image ? userDetail?.image : NoAvatar}
                          width={130}
                          height={130}
                          className="rounded-full max-w-[130px] max-h-[130px]"
                        />
                      </Box>
                    </Box>
                  </Box>
                </label>
              </Box>
            </Box>
            <Box
              sx={{
                background: (theme) => theme.palette._white_212b36.main,
                boxShadow:
                  'rgba(145, 158, 171, 0.2) 0px 0px 2px 0px, rgba(145, 158, 171, 0.12) 0px 12px 24px -4px'
              }}
              className="flex-[2] p-[20px] flex flex-col gap-[20px]"
            >
              <Box>
                <Stack
                  direction="row"
                  alignItems="center"
                  justifyContent="center"
                >
                  <Typography variant="_subtitle">
                    {t("User's personal information")}
                  </Typography>
                </Stack>
              </Box>
              <Stack direction="row" spacing={4}>
                <TextField
                  label={t('Customer Name')}
                  value={userDetail?.name || ''}
                  InputLabelProps={{ shrink: true }}
                  className="flex-[1]"
                />
                <TextField
                  label={t('Email')}
                  value={userDetail?.email}
                  InputLabelProps={{ shrink: true }}
                  className="flex-[1]"
                />
              </Stack>
              <Stack direction="row" spacing={4}>
                <TextField
                  label={t('Phone Number')}
                  value={userDetail?.phone_number}
                  InputLabelProps={{ shrink: true }}
                  className="flex-[1]"
                />
                <TextField
                  label={t('Last login at')}
                  value={formatDateTime(userDetail?.last_login_at)}
                  InputLabelProps={{ shrink: true }}
                  className="flex-[1]"
                />
              </Stack>
              <Stack direction="row" spacing={4}>
                <TextField
                  label={t('Password change at')}
                  value={formatDateTime(userDetail?.password_change_at)}
                  InputLabelProps={{ shrink: true }}
                  className="flex-[1]"
                />
                <TextField
                  label={t('Registry at')}
                  value={formatDateTime(userDetail?.registry_at)}
                  InputLabelProps={{ shrink: true }}
                  className="flex-[1]"
                />
              </Stack>
            </Box>
          </Box>
        </TabPanel>
        <TabPanel value="2">
          <Box>
            <Stack spacing={5}>
              <Box
                sx={{
                  background: (theme) => theme.palette._white_212b36.main,
                  boxShadow:
                    'rgba(145, 158, 171, 0.2) 0px 0px 2px 0px, rgba(145, 158, 171, 0.12) 0px 12px 24px -4px'
                }}
                className="flex-[1] p-[20px] flex flex-col gap-[20px]"
              >
                {userDetail?.recipients.length !== 0 ? (
                  userDetail?.recipients.map((recipient) => (
                    <Stack spacing={2} key={recipient.id}>
                      <Stack direction="row">
                        <Typography className=" text-gray-500 flex-[1]">
                          {t('Recipient name')}
                        </Typography>
                        <Typography>{recipient.name}</Typography>
                      </Stack>
                      <Stack direction="row">
                        <Typography className="  text-gray-500 flex-[1]">
                          {t('Recipient address')}
                        </Typography>
                        <Typography>{recipient.address}</Typography>
                      </Stack>
                      <Stack direction="row">
                        <Typography className="  text-gray-500 flex-[1]">
                          {t('Recipient phone number')}
                        </Typography>
                        <Typography>{recipient.phone_number}</Typography>
                      </Stack>
                    </Stack>
                  ))
                ) : (
                  <Typography>
                    {t('The user has not created recipient information')}
                  </Typography>
                )}
              </Box>
              <Box
                sx={{
                  background: (theme) => theme.palette._white_212b36.main,
                  boxShadow:
                    'rgba(145, 158, 171, 0.2) 0px 0px 2px 0px, rgba(145, 158, 171, 0.12) 0px 12px 24px -4px'
                }}
                className="flex-[1] p-[20px] flex flex-col gap-[20px]"
              >
                <OrderLayout
                  name="Order of user"
                  tabNameStorage={'TabOrderOfUser'}
                  user_id={Number(id)}
                />
              </Box>
              <Box className="flex-[1]"></Box>
            </Stack>
          </Box>
        </TabPanel>
        <TabPanel value="3">
          <Box
            sx={{
              background: (theme) => theme.palette._white_212b36.main,
              boxShadow:
                'rgba(145, 158, 171, 0.2) 0px 0px 2px 0px, rgba(145, 158, 171, 0.12) 0px 12px 24px -4px'
            }}
            className="flex-[1] p-[20px] flex flex-col gap-[20px]"
          >
            {reviewOfUser?.reviews?.length !== 0 ? (
              reviewOfUser?.reviews?.map((review) => (
                <UserReview
                  key={review.id}
                  id={review.id}
                  rating={review.rating}
                  created_at={review.created_at}
                  content={review.content}
                  image={review.image}
                  is_disable={review.is_disable}
                  user={{
                    name: userDetail?.name,
                    email: userDetail?.email
                  }}
                  loadReviewData={loadReviewOfUser}
                />
              ))
            ) : (
              <Typography>{t('User has not rated the product yet')}</Typography>
            )}
          </Box>
        </TabPanel>
      </TabContext>
    </Box>
  )
}

export default function CustomerDetail() {
  return <MainLayout title="User Detail" content={<Child />} back={true} />
}
