import React from 'react'
import { Stack, Typography, Box } from '@mui/material'
import { useTranslation } from 'react-i18next'
import TableMUI from '@/components/TableMUI'
import {
  DeleteOutlineOutlined,
  DriveFileRenameOutlineOutlined,
  LoupeOutlined
} from '@mui/icons-material'
import { useNavigate } from 'react-router-dom'
import { formatDateTime } from '@/utils/format'
import { STYLE_HEADER_TABLE } from '@/assets/styles/style'
import { GetAllUserAPI } from '@/api/UserAPI'

const CustomerTable = () => {
  const navigate = useNavigate()
  const { t } = useTranslation('translation', { keyPrefix: 'page.Customer' })
  const columns = [
    {
      field: 'id',
      maxWidth: 30,
      sortable: false,
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>ID</Box>
    },
    {
      field: 'image',
      flex: 1,
      sortable: false,
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>{t('Image')}</Box>,
      renderCell: (params) => (
        <Box>
          <img src={params.row.image} width={50} height={50} alt="" />
        </Box>
      )
    },
    {
      field: 'name',
      flex: 1,
      sortable: false,
      renderHeader: () => (
        <Box sx={STYLE_HEADER_TABLE}>{t('Customer Name')}</Box>
      )
    },

    {
      field: 'email',
      flex: 1,
      sortable: false,
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>{t('Email')}</Box>
    },
    {
      field: 'phone_number',
      flex: 1,
      sortable: false,
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>{t('Phone Number')}</Box>
    },
    {
      field: 'password_change_at',
      flex: 1,
      sortable: false,
      renderHeader: () => (
        <Box sx={STYLE_HEADER_TABLE}>{t('Password change at')}</Box>
      ),
      renderCell: (rowData) => (
        <Box>{formatDateTime(rowData.row.password_change_at)}</Box>
      )
    },
    {
      field: 'last_login_at',
      flex: 1,
      sortable: false,
      renderHeader: () => (
        <Box sx={STYLE_HEADER_TABLE}>{t('Last login at')}</Box>
      ),
      renderCell: (rowData) => (
        <Box>{formatDateTime(rowData.row.last_login_at)}</Box>
      )
    },
    {
      field: 'registry_at',
      flex: 1,
      sortable: false,
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>{t('Registry at')}</Box>,
      renderCell: (rowData) => (
        <Box>{formatDateTime(rowData.row.registry_at)}</Box>
      )
    },

    {
      field: 'actions',
      flex: 1,
      sortable: false,
      minWidth: 200,
      renderCell: (params) => (
        <Box sx={{ display: 'flex', gap: 3 }}>
          <DriveFileRenameOutlineOutlined
            style={{ cursor: 'pointer' }}
            // onClick={() => handleEdit(params.row.id)}
            onClick={() => alert(`Edit: ${params.row.id}`)}
          />
          <DeleteOutlineOutlined
            style={{ cursor: 'pointer' }}
            // onClick={() => handleDelete(params.row.id, params.row.title)}
            onClick={() => alert(`Delete: ${params.row.id}`)}
          />
          <LoupeOutlined
            style={{ cursor: 'pointer' }}
            // onClick={() => handleDetail(params.row)}
            onClick={() => navigate(`/customers/${params.row.id}`)}
          />
        </Box>
      ),
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}></Box>
    }
  ]

  const { data: allUser, isLoading, isError, error } = GetAllUserAPI()

  return (
    <TableMUI
      columns={columns}
      rows={allUser}
      isLoading={isLoading}
      isError={isError}
      errorLabel={error?.message}
      loadingLabel="Get All User"
    />
  )
}

export default function CustomerPage() {
  const { t } = useTranslation('translation', { keyPrefix: 'page.Customer' })
  return (
    <Stack gap={5}>
      <Typography variant="_title">{t('Customer Page')}</Typography>
      <CustomerTable />
    </Stack>
  )
}
